
   
	function Cargando(){
		a=document.getElementById("cargando_contenedor");
		a.style.visibility="hidden";
	}
    
	function calendario()
	{
		 Calendar.setup({
        inputField  : "fecha_envio",
        ifFormat    : "%Y-%m-%d",
        daFormat    : "%A, %d de %B de %Y",
        range        : [2014,2017],
        button      : "launch_fecha",
        showsTime    : false,
        electric    : false,
        cache        : true
    });
	}
	function calendariof()
	{
		 Calendar.setup({
        inputField  : "fecha_termino",
        ifFormat    : "%Y-%m-%d",
        daFormat    : "%A, %d de %B de %Y",
        range        : [2010,2017],
        button      : "launch_fechaf",
        showsTime    : false,
        electric    : false,
        cache        : true
    });
	}
	function soloint()
	{
		$('movil').value = $('movil').value.replace(/[^0-9]/g,"");	                   
	} 
	function agregar_grupo()
    {
      var noCache = new Date().getTime();
      var idsesion=$('idsesion').value;
		if ($('grupo').value == "")
		{
			alert("Debe rellenar el nombre de Grupo"); 
			return 0;
		}
        new Ajax.Request('aplicacion.php?cid='+noCache+'&idsesion='+idsesion, {
            method: 'get',
            parameters:
			
			{
				idaplicacion: 'grupos',
				g_nombre:$('grupo').value,
				g_tipo:$('tipo_grupo').value,
				g_desc: $('grupo_desc').value
			},
            onSuccess: function(transport)
            {
                var response = transport.responseText;
				if (response == "ok")
				{
					resetear('grupos');
					ver_grupos();
				}
				else
				{
					alert("El nombre de Grupo ya existe.")
				}
            }
        });
    }
	
	function totalmsg()
    {
        var noCache = new Date().getTime();
        var idsesion=$('idsesion').value;
        new Ajax.Request('aplicacion.php?cid='+noCache+'&idsesion='+idsesion, {
            method: 'get',
            parameters: 
			{
				idaplicacion: 'totalmsg'
			},
            onSuccess: function(transport)
            {	
                var response = transport.responseText;
				$('totalmsg').innerHTML = response;
            }
        });
    }
    function eliminar_grupo(id)
    {
    	var del_id = id;
        var noCache = new Date().getTime();
        var idsesion=$('idsesion').value;
        new Ajax.Request('aplicacion.php?cid='+noCache+'&idsesion='+idsesion, {
            method: 'get',
            parameters: 
			{
				idaplicacion: 'eliminar_grupos',
				g_id: del_id
			},
            onSuccess: function(transport)
            {
                var response = transport.responseText;
				if (response == "ok")
				{
					ver_grupos();
				}
				else
				{
					alert("El grupo no se puede eliminar.")
				}
            }
        });
    }
	function eliminar_envio_prog(id)
    {
    		var del_id = id;
        var noCache = new Date().getTime();
        var idsesion=$('idsesion').value;
        new Ajax.Request('aplicacion.php?cid='+noCache+'&idsesion='+idsesion, {
            method: 'get',
            parameters: 
			{
				idaplicacion: 'eliminar_envio_prog',
				e_id: del_id
			},
            onSuccess: function(transport)
            {
                var response = transport.responseText;
				if (response == "ok")
				{
					ver_envios_prog();
				}
				else
				{
					alert("El envio no se puede cancelar.")
				}
            }
        });
    }
	function ver_grupos()
    {
        var noCache = new Date().getTime();
        var idsesion=$('idsesion').value;
        new Ajax.Request('aplicacion.php?cid='+noCache+'&idsesion='+idsesion, {
            method: 'get',
            parameters: 
			{
				idaplicacion: 'ver_grupos'
			},
            onSuccess: function(transport)
            {
                var response = transport.responseText;
                $('grupos').innerHTML = response;
            }
        });
    }
  	function ver_envios_prog()
    {
        var noCache = new Date().getTime();
        var idsesion=$('idsesion').value;
        limpiar_form();
        new Ajax.Request('aplicacion.php?cid='+noCache+'&idsesion='+idsesion, {
            method: 'get',
            parameters: 
			{
				idaplicacion: 'ver_envios_prog'
			},
            onSuccess: function(transport)
            {
                var response = transport.responseText;
                $('envios_prog').innerHTML = response;
            }
        });
    }

   	function enviar_mensajes()
    {  
		
    	var datetime = new Date(); 
			var hora=datetime.getHours();
 			var datetime=new Date();
			var month = datetime.getMonth()+1;
			if (month < 10)
			    month = "0"+month;
			var day = datetime.getDate();
			var year = datetime.getFullYear();
			var hour = datetime.getHours();
			if (hour < 10)
			    hour = "0"+hour;			
			var min = datetime.getMinutes();
			if (min < 10)
			    min = "0"+min;
			var dateTimeString = year+'-'+month+'-'+day+' '+hour+':'+min;
			var nombre_area = $('lista_area')[$('lista_area').selectedIndex].text;
			var nombre_grupo = $('lista_grupo')[$('lista_grupo').selectedIndex].text;
			var total=grupo_total($F('lista_grupo'));
			var mensaje="Por favor confirme el siguiente env"+'\u00ed'+"o.\n\n";
			mensaje=mensaje.concat("Area o Asunto: "+nombre_area+"\n");
			mensaje=mensaje.concat("Mensaje: "+$('mensaje').value+"\n");
			mensaje=mensaje.concat("Grupo: "+nombre_grupo+"\n");
			mensaje=mensaje.concat("Total de Usuarios: "+total+"\n");
			mensaje=mensaje.concat("Fecha: "+dateTimeString);
			var resConfirm=confirm(mensaje);
			if (resConfirm)
			{	 
				loading=document.getElementById("cargando_contenedor");
				loading.style.visibility="visible";
				$('enviar').disable();
    		$('res_lista').update('<img src="img/ajax-loader.gif" border="0" alt="Cargando...">');
      	  var noCache = new Date().getTime();
      	  var idsesion=$('idsesion').value;
      	  new Ajax.Request('aplicacion.php?cid='+noCache+'&idsesion='+idsesion, {
      	      method: 'get',
      	      parameters: 
				{
					idaplicacion: 'enviar_mensajes',
					areaid:$F('lista_area'),
					gid:$F('lista_grupo'),
					mensaje:$('mensaje').value
				},
      	      onSuccess: function(transport)
      	      {
      	          var response = transport.responseText;
      	          resetear('enviar');
      	          $('res_lista').update(response);
				  totalmsg();
				  $('enviar').enable();
				  loading.style.visibility="hidden";
      	      }
      	  });
		  
      }
    } 
  function buscamo()
    {
    		var fecha_ini = $('fecha_envio').value;
 				var fecha_fin = $('fecha_termino').value;
 				if (fecha_ini == '' || fecha_fin == ''){
  				alert("Ingrese una Fecha valida.");
  				return 0;
 				}
 				var anio=fecha_ini.substring(0,4);
  			var mes=fecha_ini.substring(5,7);
  			var dia=fecha_ini.substring(8,10);
  			var fechatimeini = new Date(anio,mes-1,dia,"00","00","00").getTime();
  			var aniof=fecha_fin.substring(0,4);
  			var mesf=fecha_fin.substring(5,7);
  			var diaf=fecha_fin.substring(8,10);
  			var fechatimefin = new Date(aniof,mesf-1,diaf,"00","00","00").getTime();      
				if (fechatimefin < fechatimeini)
				{
					alert("Ingrese una Fecha Inicio menor a Fecha Termino.");
  				return 0;
				}	
         var noCache = new Date().getTime();
      	  var idsesion=$('idsesion').value;
        $('res_lista').update('<img src="img/ajax-loader.gif" border="0" alt="Cargando...">');
        new Ajax.Request('aplicacion.php?cid='+noCache+'&idsesion='+idsesion, {
            method: 'get',
            parameters: 
			{
				idaplicacion: 'buscamo',
				fecha_envio:$('fecha_envio').value,
				fecha_termino: $('fecha_termino').value
			},
            onSuccess: function(transport)
            {
                var response = transport.responseText;
                $('res_lista').innerHTML = response;
            }
        });
    }    
    
  function enviar_mensajes_unoauno()
    {
    	var datetime=new Date();
			var month = datetime.getMonth()+1;
			if (month < 10)
			    month = "0"+month;
			var day = datetime.getDate();
			var year = datetime.getFullYear();
			var hour = datetime.getHours();
			if (hour < 10)
			    hour = "0"+hour;			
			var min = datetime.getMinutes();
			if (min < 10)
			    min = "0"+min;
			var dateTimeString = year+'-'+month+'-'+day+' '+hour+':'+min;		  	
    	var nombre_area = $('lista_areaunoauno')[$('lista_areaunoauno').selectedIndex].text;
    	var nombre_grupo = $('lista_grupouno')[$('lista_grupouno').selectedIndex].text;
			var info_grupo=grupo_total_uno($F('lista_grupouno'));
			var info_grup=info_grupo.split("|");
			var mensaje="Por favor confirme el siguiente env"+'\u00ed'+"o.\n\n";
			mensaje=mensaje.concat("Area o Asunto: "+nombre_area+"\n");
			mensaje=mensaje.concat("Mensaje: "+info_grup[1]+"\n");
			mensaje=mensaje.concat("Grupo: "+nombre_grupo+"\n");
			mensaje=mensaje.concat("Total de Usuarios: "+info_grup[0]+"\n");
			mensaje=mensaje.concat("Fecha: "+dateTimeString);
			var resConfirm=confirm(mensaje);
			if (resConfirm)
			{    loading=document.getElementById("cargando_contenedor");
				loading.style.visibility="visible";
				$('btn_enviar_unoauno').disable();
    		$('res_lista').update('<img src="img/ajax-loader.gif" border="0" alt="Cargando...">');
      	  var noCache = new Date().getTime();
      	  var idsesion=$('idsesion').value;
      	  new Ajax.Request('aplicacion.php?cid='+noCache+'&idsesion='+idsesion, {
      	      method: 'get',
      	      parameters: 
				{
					idaplicacion: 'enviar_mensajes_unoauno',
					areaid:$F('lista_areaunoauno'),
					gid:$F('lista_grupouno'),
				},
      	      onSuccess: function(transport)
      	      {
      	          var response = transport.responseText;
      	          $('res_lista').update(response);
				  totalmsg();
				  $('btn_enviar_unoauno').enable();
				  loading.style.visibility="hidden";
      	      }
      	  });
				
      }
    }
	function saber_tipo_grupo(id){
		var noCache = new Date().getTime(); 
		var tipo;
		var idsesion=$('idsesion').value; 
		new Ajax.Request('aplicacion.php?cid='+noCache+'&idsesion='+idsesion, {
				method: 'get',
				asynchronous: false,
        	    parameters: 
					{
						idaplicacion: 'saber_tipo_grupo',
						grupoid:id,
					},
        	    onSuccess: function(transport)
        	    {	
        	        var response = transport.responseText;
					tipo=response;
        	    }
        	});
	    return tipo;
	}
	function guardar_envio()
    {		
    		var fecha_form = $('fecha_envio').value;
    		if (fecha_form == ''){
    			alert("Ingrese una Fecha valida.");
    			return 0;
    		}
    		var anio=fecha_form.substring(0,4);
    		var mes=fecha_form.substring(5,7);
    		var dia=fecha_form.substring(8,10);
    		var ini_hor = $F('inicio_hora');
			var ini_min = $F('inicio_minuto');
    		var fechatime = new Date(anio,mes-1,dia,ini_hor,ini_min,"00").getTime();
			var noCache = new Date().getTime();     
			var idsesion=$('idsesion').value;   
				if (fechatime <= noCache)
				{
					alert("Ingrese una Fecha y Hora mayor a la actual.");
					return 0;
				}
				var nombre_grupo = $('lista_grupo')[$('lista_grupo').selectedIndex].text;
				var tipogrupo=saber_tipo_grupo($F('lista_grupo'));
				var msg;
				if(tipogrupo =='unoauno')
				{	var info_grupo=grupo_total_uno($F('lista_grupo'));
					var info_grup=info_grupo.split("|");
					var total=info_grup[0];
					var msg=info_grup[1];
				}else
				{	var msg=$('mensaje').value;
					var total=grupo_total($F('lista_grupo'));
				}
				var mensaje="Por favor confirme el siguiente env"+'\u00ed'+"o.\n\n";
				mensaje=mensaje.concat("Mensaje: "+msg+"\n");
				mensaje=mensaje.concat("Grupo: "+nombre_grupo+"\n");
				mensaje=mensaje.concat("Total de Usuarios: "+total+"\n");
				mensaje=mensaje.concat("Fecha: "+fecha_form+" "+ini_hor+":"+ini_min);
				var resConfirm=confirm(mensaje);
				if (resConfirm)
				{	
					loading=document.getElementById("cargando_contenedor");
					loading.style.visibility="visible";
					new Ajax.Request('aplicacion.php?cid='+noCache+'&idsesion='+idsesion, {
					method: 'get',
        	    parameters: 
					{
						idaplicacion: 'guardar_envio',
						areaid:$F('lista_area'),
						gid:$F('lista_grupo'),
						mensaje:msg,
						fecha:$('fecha_envio').value,
						inicio_hor:$F('inicio_hora'),
						inicio_min:$F('inicio_minuto')
					},
        	    onSuccess: function(transport)
        	    {	
        	        var response = transport.responseText;
        	        if (response == "ok")
        	        {
        	        	ver_envios_prog();
						totalmsg();
						loading.style.visibility="hidden";
        	        }
        	        else{
        	        	loading.style.visibility="hidden";
						alert(transport.responseText);
					}
        	    }
        	});  
       }    
    }
	function ver_lista_grupos()
    {
		//alert("PASO");
        var noCache = new Date().getTime();
        var idsesion=$('idsesion').value;
        new Ajax.Request('aplicacion.php?cid='+noCache+'&idsesion='+idsesion, {
            method: 'get',
            parameters: 
			{
				idaplicacion: 'ver_lista_grupos'
			},
            onSuccess: function(transport)
            {
                var response = transport.responseText;
                $('lista_grupos').innerHTML = '';
                $('lista_grupos').innerHTML = response;
            }
        });
    }
	
	function ver_lista_grupos_envio()
    {
        var noCache = new Date().getTime();
        var idsesion=$('idsesion').value;
        new Ajax.Request('aplicacion.php?cid='+noCache+'&idsesion='+idsesion, {
            method: 'get',
            parameters:
			{
				idaplicacion: 'ver_lista_grupos_envio'
			},
            onSuccess: function(transport)
            {
                var response = transport.responseText;
                $('lista_grupos').innerHTML = response;
            }
        });
    }
    	function ver_lista_grupos_agenda()
    {
        var noCache = new Date().getTime();
        var idsesion=$('idsesion').value;
        new Ajax.Request('aplicacion.php?cid='+noCache+'&idsesion='+idsesion, {
            method: 'get',
            parameters:
			{
				idaplicacion: 'ver_lista_grupos_agenda'
			},
            onSuccess: function(transport)
            {
                var response = transport.responseText;
                $('lista_grupos').innerHTML = response;
            }
        });
    }
	
		function ver_lista_grupos_uno()
    {
        var noCache = new Date().getTime();
        var idsesion=$('idsesion').value;
        new Ajax.Request('aplicacion.php?cid='+noCache+'&idsesion='+idsesion, {
            method: 'get',
            parameters:
			{
				idaplicacion: 'ver_lista_grupos_uno'
			},
            onSuccess: function(transport)
            {
                var response = transport.responseText;
                $('lista_grupos_uno').innerHTML = '';
                $('lista_grupos_uno').innerHTML = response;

            }
        });
    }
	function ver_lista_grupos_unoauno()
    {
        var noCache = new Date().getTime();
        var idsesion=$('idsesion').value;
        new Ajax.Request('aplicacion.php?cid='+noCache+'&idsesion='+idsesion, {
            method: 'get',
            parameters:
			{
				idaplicacion: 'ver_lista_grupos_unoauno'
			},
            onSuccess: function(transport)
            {
                var response = transport.responseText;
                $('lista_grupos_unoauno').innerHTML = response;
            }
        });
    }
	function ver_lista_grupos_m()
    {
        var noCache = new Date().getTime();
        var idsesion=$('idsesion').value;
        new Ajax.Request('aplicacion.php?cid='+noCache+'&idsesion='+idsesion, {
            method: 'get',
            parameters: 
			{
				idaplicacion: 'ver_lista_grupos_m'
			},
            onSuccess: function(transport)
            {
                var response = transport.responseText;
                $('lista_grupos_m').innerHTML = '';
                $('lista_grupos_m').innerHTML = response;
            }
        });
    }

	function agregar_lista()
	{
		var numero = $('numero').value;
		var regex = /^[0-9]+$/;
		var lista = $('lista_numero');
		
		if(numero == "" || !regex.test(numero) || numero.length < 10)
		{
			alert ('Se debe ingresar un numero valido.');
			return false;
		}

		for (i = lista.length - 1; i>=0; i--)
		{
			if (lista.options[i].value == numero)
			{
				alert ('El numero ya se encuentra dentro de esta lista.');
				return false;

			}
		}
		
		$('lista_numero').insert('<option>'+numero+'</option>');
		$('numero').value = "";
	}

	function eliminar_lista()		
	{
		var lista = $('lista_numero');
		var i;
		for (i = lista.length - 1; i>=0; i--)
		{
			if (lista.options[i].selected)
			{
		  		lista.remove(i);
			}
		}
	}
	function limpiarSelect_lista(idSelect) {
           var lista = $(idSelect);
			var i;
			for (i = lista.length - 1; i>=0; i--)
			{
			if (lista.options[i])
			{
		  		lista.remove(i);
			}
		}
	}
	function cambiar()
	{
		Effect.Appear('as_manual');
		Effect.Fade('as_mass_p');
		Effect.Fade('as_mass_uno');
		Effect.Fade('as_mass');
	}
	function cambiar2()
	{
		Effect.Appear('as_mass');
		Effect.Fade('as_mass_p');
		Effect.Fade('as_manual');
		Effect.Fade('as_mass_uno');
	}
		function cambiar3()
	{
		Effect.Appear('as_mass_p');
		Effect.Fade('as_mass');
		Effect.Fade('as_manual');
		Effect.Fade('as_mass_uno');
	}
		function cambiar4()
	{

		Effect.Appear('as_mass_uno');
		Effect.Fade('as_mass_p');
		Effect.Fade('as_manual');
		Effect.Fade('as_mass');
	}
	
	function cambiar_envio()
	{
		Effect.toggle('e_normal','slide');
		Effect.toggle('e_unoauno','slide');
	}
	
	   function ver_lista_grupos_p()
    {
        var noCache = new Date().getTime();
        var idsesion=$('idsesion').value;
        new Ajax.Request('aplicacion.php?cid='+noCache+'&idsesion='+idsesion, {
            method: 'get',
            parameters:
			{
				idaplicacion: 'ver_lista_grupos_p'
			},
            onSuccess: function(transport)
            {
                var response = transport.responseText;
                $('lista_grupos_p').innerHTML = '';
                $('lista_grupos_p').innerHTML = response;
            }
        });
    }
	
	
	function agregar_numeros()
    {
   	
		$('res_lista').update("");
		var lista = $('lista_numero').getElementsByTagName('option');
		var id_lista = $F('lista_grupo');
		var nodos = $A(lista);
		nodos.each(function(nodo)
		{
		   	var noCache = new Date().getTime();
		   	var idsesion=$('idsesion').value;
		    new Ajax.Request('aplicacion.php?cid='+noCache+'&idsesion='+idsesion,
				{
		            method: 'get',
		            parameters: 
					{
						numeros:nodo.innerHTML,
						id_lista:id_lista,
						idaplicacion: 'agregar_numeros'
					},
		            onSuccess: function(transport)
		            {
		                var response = transport.responseText;
		                $('res_lista').insert(response);
		            }
	        	}
			);
 		});
 	}
	
	function agregar_numeroslistanegra()
    {
		$('res_lista').update("");
		var lista = $('lista_numero').getElementsByTagName('option');
		var id_lista = 0;
		var user = $F('nuser');
		var nodos = $A(lista);
		nodos.each(function(nodo)
		{
		   	var noCache = new Date().getTime();
		   	var idsesion=$('idsesion').value;
		    new Ajax.Request('aplicacion.php?cid='+noCache+'&idsesion='+idsesion,
				{
		            method: 'get',
		            parameters: 
					{
						numeros:nodo.innerHTML,
						id_lista:id_lista,
						user:user,
						idaplicacion: 'agregar_numeros_negra'
					},
		            onSuccess: function(transport)
		            {
		                var response = transport.responseText;
		                $('res_lista').insert(response);
		            }
	        	}
			);
 		});
 	}
        
	function contador()
	{
		$('mensaje').value = $('mensaje').value.replace(/[^0-9a-zA-Z .)!:;,#@$+%#?&(/_=-]/g,"");			
		var largo = $('mensaje').value.length;
		if (largo > 160 )
		{
			$('enviar').enable();
			$('contador').innerHTML = "Este mensaje ya es concatenado tiene :<lebel style='color: red;'> "+largo+" Caracteres </label>";
		}
		else if (largo > 20 )
			
		{
			$('enviar').enable();
			$('contador').innerHTML = largo+" contador de caracteres ";			
		}
		else
		{					
			$('enviar').disable();
			$('contador').innerHTML = largo+" contador de caracteres ";
		}
	
	}

	/*
		* function confirmaMensaje()
		* comprueba si el grupo es unoauno, valido para realiza agendamiento de envio
		* desabilitando campo mensaje
	*/
	function confirmaMensaje()
	{  
  	var noCache = new Date().getTime();
  	var idsesion=$('idsesion').value;
        new Ajax.Request('aplicacion.php?cid='+noCache+'&idsesion='+idsesion,{
            method: 'get',
            parameters:
			{
				idaplicacion: 'confirmaMensaje',
				gid:$F('lista_grupo')
			},
            onSuccess: function(transport)
            {
                var response = transport.responseText;
                if (response=='ok')
                {	
                	$('mensaje').enable();
                }
                else
                {
                	$('mensaje').disable();
                	$('enviar').enable();
                }
            }
        });
	}
	function limpiar_form(){
		$('mensaje').value="";
		$('fecha_envio').value="";
		$('inicio_hora').value="08";
		$('inicio_minuto').value = "00";
		$('contador').innerHTML = "0";
}
	function resetear(pagina)
	{
		var pag=pagina;
		if (pag =='enviar')
		{
			$('mensaje').clear();
			$('contador').innerHTML = "0";
		}else if (pag=='busqueda')
				{
					$('movil').clear();
					$('fecha_envio').clear();  
					$('fecha_termino').clear();	
				}
		else if (pag=='asociar')
			{
				$('numero').clear();
				var lista=$('lista_numero');
				for (i = lista.length - 1; i>=0; i--)
				{
		  			lista.remove(i);					
				}
				if ($('filex'))
				{
					$('filex').clear();
				}
			}
			else if (pag=='grupos')
				{
					$('grupo').clear();
					$('grupo_desc').clear();
				}
	}

	function ver_lista_area()
    {
        var noCache = new Date().getTime();
        var idsesion=$('idsesion').value;
        new Ajax.Request('aplicacion.php?cid='+noCache+'&idsesion='+idsesion, {
            method: 'get',
            parameters:
			{
				idaplicacion: 'ver_lista_area'
			},
            onSuccess: function(transport)
            {
                var response = transport.responseText;
                $('lista_areas').innerHTML = '';
                $('lista_areas').innerHTML = response;
            }
        });
    }
	function ver_lista_areaunoauno()
    {
        var noCache = new Date().getTime();
        var idsesion=$('idsesion').value;
        new Ajax.Request('aplicacion.php?cid='+noCache+'&idsesion='+idsesion, {
            method: 'get',
            parameters:
			{
				idaplicacion: 'ver_lista_areaunoauno'
			},
            onSuccess: function(transport)
            {
                var response = transport.responseText;
                $('lista_areasunoauno').innerHTML = response;
            }
        });
    }
	function buscamovil()
	{
		var largo = $('movil').value.length;    
		if (largo < 10)
		{
			alert("Ingrese Movil Valido"); 
			return 0;
		}
		var fecha_ini = $('fecha_envio').value;
		var fecha_fin = $('fecha_termino').value;
		if (fecha_ini == '' || fecha_fin == ''){
			alert("Ingrese una Fecha valida.");
			return 0;
		}
		var anio=fecha_ini.substring(0,4);
		var mes=fecha_ini.substring(5,7);
		var dia=fecha_ini.substring(8,10);
		var fechatimeini = new Date(anio,mes-1,dia,"00","00","00").getTime();
		var aniof=fecha_fin.substring(0,4);
		var mesf=fecha_fin.substring(5,7);
		var diaf=fecha_fin.substring(8,10);
		var noCache = new Date().getTime(); 
		var idsesion=$('idsesion').value; 
		var fechatimefin = new Date(aniof,mesf-1,diaf,"00","00","00").getTime();      
		if (fechatimefin < fechatimeini)
		{
			alert("Ingrese una Fecha Inicio menor a Fecha Termino.");
			return 0;
		}	
		new Ajax.Request('aplicacion.php?cid='+noCache+'&idsesion='+idsesion, {
			method: 'get',
			parameters:			
					{
						idaplicacion: 'buscamovil',
						movil:$('movil').value,
						fecha_envio:$('fecha_envio').value,
						fecha_termino: $('fecha_termino').value
					},
				onSuccess: function(transport)
				{	
					var response = transport.responseText;
					//resetear('busqueda');
							$('res_lista').innerHTML = response;
				}
		});
	} 
	function grupo_total(id)
	{
		 var group_id = id;
	   var noCache = new Date().getTime();
	   var idsesion=$('idsesion').value;
	   var resultado;
	   new Ajax.Request('aplicacion.php?cid='+noCache+'&idsesion='+idsesion, {
	     method: 'get',
	     asynchronous: false,
	     parameters: 
			{
				idaplicacion: 'grupo_total',
				e_id: group_id
			},
	    onSuccess: function(transport)
	    {
	        var response = transport.responseText;
	        resultado=response;
	    }
	   });
	   return resultado;
	}
		function grupo_total_uno(id)
	{
		 var group_id = id;
	   var noCache = new Date().getTime();
	   var idsesion=$('idsesion').value;
	   var resultado;
	   new Ajax.Request('aplicacion.php?cid='+noCache+'&idsesion='+idsesion, {
	     method: 'get',
	     asynchronous: false,
	     parameters: 
			{
				idaplicacion: 'grupo_total_uno',
				e_id: group_id
			},
	    onSuccess: function(transport)
	    {
	        var response = transport.responseText;
	        resultado=response;
	    }
	   });
	   return resultado;
	}
	
	function contador2()
	{
		var largo = $('filex').value.length;
		var largo_p = $('filexp').value.length;
		var largo_uno = $('filexuno').value.length;
		if (largo > 10)
		{
			$('enviarx').enable();
		}
		else 
		{
			$('enviarx').disable();
		}
		if (largo_p > 10)
		{
			$('enviarp').enable();
		}
		else 
		{
			$('enviarp').disable();
		}
		if (largo_uno > 10)
		{
			$('enviaruno').enable();
		}
		else 
		{
			$('enviaruno').disable();
		}
	}
	
	function comprueba_nombre_file_black()
	{
		var nombre_grupo = $('lista_grupo_m')[$('lista_grupo_m').selectedIndex].text;
		var ruta_file = $('filex').value;
		var ruta_f=ruta_file.split("\\");
		var n=ruta_f.length;
		var nombre_f=ruta_f[n-1];
		var nn = nombre_f.lastIndexOf(".");
		var nombre_archivo = nombre_f.slice(0,nn);
		if (nombre_archivo=='')
		{
			$('enviarx').disable();
		}
		else
		{
			$('enviarx').enable();
		}
	}
	
	function comprueba_nombre_file()
	{
		var nombre_grupo = $('lista_grupo_m')[$('lista_grupo_m').selectedIndex].text;
		var ruta_file = $('filex').value;
		var ruta_f=ruta_file.split("\\");
		var n=ruta_f.length;
		var nombre_f=ruta_f[n-1];
		var nn = nombre_f.lastIndexOf(".");
		var nombre_archivo = nombre_f.slice(0,nn);
		if (nombre_archivo=='')
		{
			$('enviarx').disable();
		}
		else if (nombre_archivo != nombre_grupo)
		{
			$('enviarx').disable();
			alert ("Nombre de archivo no corresponde con el nombre del Grupo.");		
		}
		else
		{
			$('enviarx').enable();
		}
	}
	function comprueba_nombre_file_p()
	{
		var nombre_grupo = $('lista_grupo_p')[$('lista_grupo_p').selectedIndex].text;
		var ruta_file = $('filexp').value;
		var ruta_f=ruta_file.split("\\");
		var n=ruta_f.length;
		var nombre_f=ruta_f[n-1];
		var nn = nombre_f.lastIndexOf(".");
		var nombre_archivo = nombre_f.slice(0,nn);
		if (nombre_archivo=='')
		{
			$('enviarp').disable();
		}
		else if (nombre_archivo != nombre_grupo)
		{
			$('enviarp').disable();
			alert ("Nombre de archivo no corresponde con el nombre del Grupo.");		
		}
		else
		{
			$('enviarp').enable();
		}
	}
	function comprueba_nombre_file_uno()
	{
		var nombre_grupo = $('lista_grupo_uno')[$('lista_grupo_uno').selectedIndex].text;
		var ruta_file = $('filexuno').value;
		var ruta_f=ruta_file.split("\\");
		var n=ruta_f.length;
		var nombre_f=ruta_f[n-1];
		var nn = nombre_f.lastIndexOf(".");
		var nombre_archivo = nombre_f.slice(0,nn);
		if (nombre_archivo=='')
		{
			$('enviaruno').disable();
		}
		else if (nombre_archivo != nombre_grupo)
		{
			$('enviaruno').disable();
			alert ("Nombre de archivo no corresponde con el nombre del Grupo.");		
		}
		else
		{
			$('enviaruno').enable();
		}
	}
