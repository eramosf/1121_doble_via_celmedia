<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<meta name="author" content="celmedia" />
		<title>Celmedia :: Envios Masivos</title>
		<link rel="stylesheet" type="text/css" href="css/style.css" media="screen" />
		<link rel="shortcut icon" href="favicon.ico" />
		<script type="text/javascript" src="js/prototype.js"></script>
		<script type="text/javascript" src="js/textsizer.js"></script>
		<script type="text/javascript" src="js/rel.js"></script>	
	</head>

<body>
	<div id="wrap">
		<div id="top">
			<h2><a href="#" title="Back to main page">Celmedia</a></h2>
			<div id="menu">
				<?php include ('menu.php');?>
			</div>
		</div>
		<div id="content">
			<div style="float: right;">Usuario: @USER</div>
			<div id="left">
				<h2>Asociacion masiva de destinatarios</h2>
				<p>
					<table>
					<tr>
						<td class="td2">Nombre del Grupo</td>
						<td>
							<select>
								<option>Grupo Privado</option>
								<option>Grupo Publico</option>
							</select>
						</td>
					</tr>
					<tr><td class="td2">Selecciona el Archivo</td>
						<td>
						
							<input type="file" accept="Examinar..." size="20">
						</td>
					</tr>
					<tr><td class="td2" colspan=2><input type="submit" value="Subir"><input type="reset"></td></tr>
					</table>
				</p>		
			</div>
			<div id="right">
				<ul id="nav">
					<li><a href="#Home">Home</a></li>
					<li><a href="#Archive">Archive</a></li>
					<li><a href="#Link">Links</a></li>
					<li><a href="#download">Download</a></li>
					<li><a href="#support">Support</a></li>
					<li><a href="#contact">Contact</a></li>
				</ul>
				<div class="box">
					<h2 style="margin-top:17px">Recent Entries</h2>
					<ul>
						<li><a href="#">Recent Entries1</a> <i>01 Des 06</i></li>
						<li><a href="#">Recent Entries2</a> <i>01 Des 06</i></li>
						<li><a href="#">Recent Entries3</a> <i>01 Des 06</i></li>
						<li><a href="#">Recent Entries4</a> <i>01 Des 06</i></li>
						<li><a href="#">Recent Entries5</a> <i>01 Des 06</i></li>
					</ul>
				</div>
			</div>
			<div id="clear"></div>
		</div>
		<div id="footer">
			<?php include ('foot.php');?>
		</div>
	</div>
</body>
</html>