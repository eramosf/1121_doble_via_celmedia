<?php
	require ('login.head.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<meta name="author" content="celmedia" />
		<title>Celmedia :: Envios Masivos</title>
		<link rel="stylesheet" type="text/css" href="css/style.css" media="screen" />
		<link rel="shortcut icon" href="favicon.ico" />
		<script type="text/javascript" src="js/prototype.js"></script>
		<script type="text/javascript" src="js/textsizer.js"></script>
		<script type="text/javascript" src="js/rel.js"></script>
		<script type="text/javascript" src="js/aplicaciones.js"></script>
	</head>

<body onload="totalmsg();ver_grupos();">
	<div id="wrap">
		<div id="top">
			<h2><a href="#" title="Back to main page">Celmedia</a></h2>
			<div id="menu">
				<?php include ('menu.php');?>
			</div>
			<h3>Tel : 57 1 6421002</h3>
		</div>
		<div id="content">
			<div style="float: right;">Usuario: <?=$reg_nombre?> (<a href="logout.php">Salir</a>)<p id="totalmsg"><p/></div>
			<div id="left">
				<h2>Agregar nuevo grupo</h2>
				<p><form action="" method="post">
					<table>
					<tr><td class="td2">Nombre Grupo</td><td><input type="text" id="grupo" /></td></tr>
					<tr><td class="td2">Tipo de Grupo</td><td><select size="1" id="tipo_grupo">
							<option value="privado">Privado</option>
							<option value="publico">Publico</option>
							<option value="personalizado">Personalizado</option>
							<option value="unoauno">Personalizado 1 a 1</option>
							
						</select>
						<input name="idsesion" id="idsesion" value="<?php echo $_SESSION['idsess'.NBOLSA];?>" type="hidden"/></td></tr>
					<tr><td class="td2">Comentarios</td><td><textarea wrap="VIRTUAL" id="grupo_desc" maxlength="160"></textarea></td></tr>
					<tr><td class="td2" colspan=2><input type="button" value='Enviar' onclick="agregar_grupo();return false;"><input type="reset"></td></tr>
					</table>
					</form>
				</p>
				<br><br>
				<h2>Grupo <small style="float:right;" onclick="ver_grupos();return false;"><a href="#!">Actualizar</a></small></h2>
				<p>
					<div id="grupos">
						<img src="img/ajax-loader.gif" border="0" alt="Cargando...">						
					</div>
				</p>
				
			</div>
			<div id="clear"></div>
		</div>
		<div id="footer">
			<?php include ('foot.php');?>
		</div>
	</div>
</body>
</html>