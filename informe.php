<?php
	include ("config/config.inc.php");
	require ("config/dbal.class.php");
	set_time_limit(0);
	ini_set("max_input_time", "-1");
	ini_set("memory_limit", "2048M");
	$db = new dbal;
	$conectado = $db->setAll($kDbalDriver, $kDatabaseUsername, $kDatabasePassword, $kDatabaseHostname, $kDatabaseName);
	$id_envio = htmlspecialchars(addslashes(strip_tags($_GET['id'])), ENT_QUOTES, 'UTF-8');
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition:  filename=\"" . date("Ymd_His\G$id_envio") . ".xls\";");
	header("Pragma: no-cache");
	header("Expires: 0");
	echo "<table border=1>";
	echo "<tr><th> Numero Corto </th><th> Grupo  </th><th> Fecha </th><th> Numero </th><th> Operador </th> <th> Mensaje </th><th> Estado </th></tr>";
	$query = "select l.shortcode,g.nombre,l.fecha,l.sms_destino,l.operador,l.mensaje,l.estado from ".IDSERVICIO."_log l,".IDSERVICIO."_grupos g where g.id = l.grupo and l.id_envio=$id_envio order by operador,fecha";

	$db->executeQuery($query);
	while ($stInfo = $db->fetchArray())
	{	
		$nc = $stInfo[0];
		if ($stInfo[6] == "activo")
		{
			$estado = "Enviado";
		}
		else
		{
			$estado = "Fallido";
		}
		echo "<tr><td> $nc </td> <td> $stInfo[1] </td> <td> $stInfo[2] </td> <td> $stInfo[3] </td> <td> $stInfo[4] </td> <td> $stInfo[5] </td> <td> $estado </td></tr>";
	}
	echo "</table>";
?>