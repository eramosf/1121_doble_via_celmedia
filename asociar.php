<?php
	require ('login.head.php');
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<meta name="author" content="celmedia" />
		<title>Celmedia :: Envios Masivos</title>
		<link rel="stylesheet" type="text/css" href="css/style.css" media="screen" />
		<link rel="shortcut icon" href="favicon.ico" />
		<script type="text/javascript" src="js/prototype.js"></script>
		<script type="text/javascript" src="js/textsizer.js"></script>
		<script type="text/javascript" src="js/rel.js"></script>
		<script type="text/javascript" src="js/aplicaciones.js"></script>
		<script type="text/javascript" src="js/scriptaculous.js?load=effects"></script>

	</head>

<body onload="totalmsg();ver_lista_grupos();ver_lista_grupos_m();ver_lista_grupos_p();ver_lista_grupos_uno();contador2();">
	<div id="wrap">
		<div id="top">
			<h2><a href="#" title="Back to main page">Celmedia</a></h2>
			<div id="menu">
				<?php include ('menu.php');?>
			</div>
			<h3>Tel : 57 1 6421002</h3>
		</div>
		<div id="content">
			<div style="float: right;">Usuario: <?=$reg_nombre?> (<a href="logout.php">Salir</a>)<p id="totalmsg"><p/></div>
			<div id="left">
				<h2>Asociar destinatarios manual
					<small style="float:right" onclick="ver_lista_grupos();return false;">
						<a href="#!">Actualizar</a>
					</small>
				</h2>
				<div style="text-align:right">(<a href="#" onclick="cambiar();">Ver</a>)</div>
				<div id="as_manual">
				<p>
					<form action="" method="post">
					<table>
					<tr>
						<td class="td2">Nombre del Grupo</td>
						<td>
							<div id="lista_grupos">
							<select>
								<option disabled selected>No tiene grupos habilitados.</option>								
							</select>
							</div>
						</td>
					</tr>
					<tr><td class="td2">Numero Celular</td>
						<td>
							<input type="text" id="numero" style="width:10em;"><input type="button" value="+" onclick="agregar_lista();" ><input type="button" value="-" onclick="eliminar_lista();" ><br>
							<select size="10" id="lista_numero" style="width:10em;">
							</select>
							<input name="idsesion" id="idsesion" value="<?php echo $_SESSION['idsess'.NBOLSA];?>" type="hidden"/>								
						</td>
					</tr>
					<tr><td class="td2" colspan=2><input type="button" value='Enviar' onclick="agregar_numeros();">
						<input type="reset" onclick="limpiarSelect_lista('lista_numero');"></td></tr>
					</table>
					</form>
				</p>		
				</div>
				<br><br>
				<h2>Asociar destinatarios masivo
					<small style="float:right" onclick="ver_lista_grupos_m();return false;">
						<a href="#!">Actualizar</a>
					</small>
				</h2>
				<div style="text-align:right">(<a href="#" onclick="cambiar2();">Ver</a>)</div>
				<div id="as_mass" style="display:none;">
				<p>
				<form id="mass" action="upload.php" method="post" enctype="multipart/form-data">

					<table>
					<tr>
						<td class="td2">Nombre del Grupo</td>
						<td>
							<div id="lista_grupos_m">
							<select>
								<option disabled selected>No tiene grupos habilitados.</option>								
							</select>
							</div>
						</td>
					</tr>
					<tr><td class="td2">Selecciona el Archivo</td>
						<td>
							<input name="filex" type="file" size="60" maxlength="80" id="filex" onchange="comprueba_nombre_file();">
							<input name="idsesion" id="idsesion" value="<?php echo $_SESSION['idsess'.NBOLSA];?>" type="hidden"/>	
							</td>
					</tr>
					<tr><td class="td2" colspan=2><input type="submit" id="enviarx"><input type="reset"></td></tr>
					</table>
					</form>
				</p>
				</div>		
				<br /><br />
					<h2>Asociar destinatarios masivo Personalizados
						<small style="float:right" onclick="ver_lista_grupos_p();return false;">
							<a href="#!">Actualizar</a>
						</small></h2>
				<div style="text-align:right">(<a href="#" onclick="cambiar3();">Ver</a>)</div>
				<div id="as_mass_p" style="display:none;">
				<p>
				<form id="as_mass_p" action="upload_2.php" method="post" enctype="multipart/form-data">

					<table>
					<tr>
						<td class="td2">Nombre del Grupo</td>
						<td>
							<div id="lista_grupos_p">
							<select>
								<option disabled selected>No tiene grupos habilitados.</option>
							</select>
							</div>
						</td>
					</tr>
					<tr><td class="td2">Selecciona el Archivo</td>
						<td>
							<input name="filexp" type="file" size="60" maxlength="80" id="filexp" onchange="comprueba_nombre_file_p();">
							<input name="idsesion" id="idsesion" value="<?php echo $_SESSION['idsess'.NBOLSA];?>" type="hidden"/>	
							</td>
					</tr>
					<tr><td class="td2" colspan=2><input type="submit" id="enviarp"><input type="reset"></td></tr>
					</table>
					</form>
				</p>
				</div>
							<br /><br />
					<h2>Asociar destinatarios masivo Personalizados 1 a 1
						<small style="float:right" onclick="ver_lista_grupos_uno();return false;">
							<a href="#!">Actualizar</a>
						</small></h2>
				<div style="text-align:right">(<a href="#" onclick="cambiar4();">Ver</a>)</div>
				<div id="as_mass_uno" style="display:none;">
				<p>
				<form id="as_mass_unof" action="upload_unoauno.php" method="post" enctype="multipart/form-data">

					<table>
					<tr>
						<td class="td2">Nombre del Grupo</td>
						<td>
							<div id="lista_grupos_uno">
							<select>
								<option disabled selected>No tiene grupos habilitados.</option>
							</select>
							</div>
						</td>
					</tr>
					<tr><td class="td2">Selecciona el Archivo</td>
						<td>
							<input name="filexuno" type="file" size="60" maxlength="80" id="filexuno" onchange="comprueba_nombre_file_uno();">
							<input name="idsesion" id="idsesion" value="<?php echo $_SESSION['idsess'.NBOLSA];?>" type="hidden"/>	
							</td>
					</tr>
					<tr><td class="td2" colspan=2><input type="submit" id="enviaruno"><input type="reset"></td></tr>
					
					</table>
					<p>Archivo plano con el formato ( Movil | Mensaje )</p>
					</form>
				</p>
				</div>
				<br /><br />
				<h2>Resultado de Asociacion de destinatarios</h2>
				<p>
				<div id="res_lista">
				</div>
				</p>
			</div>
			<div id="clear"></div>
		</div>
		<div id="footer">
			<?php include ('foot.php');?>
		</div>
	</div>
</body>
</html>