<?php
	require ('login.head.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<meta name="author" content="celmedia" />
		<title>Celmedia :: Envios Masivos</title>
		<link rel="stylesheet" type="text/css" href="css/style.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="css/calendar-blue2.css" />
		<link rel="shortcut icon" href="favicon.ico" />
		<script type="text/javascript" src="js/prototype.js"></script>
		<script type="text/javascript" src="js/textsizer.js"></script>
		<script type="text/javascript" src="js/rel.js"></script>
		<script type="text/javascript" src="js/scriptaculous.js?load=effects"></script>
		<script type="text/javascript" src="js/aplicaciones.js"></script>
		<script type="text/javascript" src="js/jscalendar/calendar.js"></script>
		<script type="text/javascript" src="js/jscalendar/lang/calendar-es.js"></script>
		<script type="text/javascript" src="js/jscalendar/calendar-setup.js"></script>
	</head>
<body onload="totalmsg();calendario();calendariof();">
	<div id="wrap">
			<div id="top">
			<h2><a href="#" title="Back to main page">Celmedia</a></h2>
			<div id="menu">
				<?php include ('menu.php');?>
			</div>
			<h3>Tel : 57 1 6421002</h3>
		</div>
		<div id="content">
			<div style="float: right;">Usuario: <?=$reg_nombre?> (<a href="logout.php">Salir</a>)<p id="totalmsg"><p/></div>
			<div id="left">
				
							
				<h2>Servicio al Cliente</h2>
				<p>
					<form action="descargarBusqueda.php" method="post">
					<table>
					<tr><td class="td2">M&oacute;vil</td><td><input type="text" id="movil" name="movil" maxlength="10" size="20" autocomplete="off" onkeyup="soloint();"/></td></tr>
					<tr><td class="td2">Fecha Inicio</td>
						<td><input type="text" id="fecha_envio"  value="" name="fecha_envio" autocomplete="off" maxlength="10"  size="20"/>&nbsp;<img src="img/calendario.gif" width="17" height="16" border="0" align="absmiddle" title="CALENDARIO" onclick="calendario();return false;" id="launch_fecha" style="cursor:pointer;">&nbsp;&nbsp;</td></tr>
					<tr><td class="td2">Fecha Termino</td>
						<input name="idsesion" id="idsesion" value="<?php echo $_SESSION['idsess'.NBOLSA];?>" type="hidden"/>
						<td><input type="text" id="fecha_termino"  value="" name="fecha_termino"  autocomplete="off" maxlength="10" size="20" />&nbsp;<img src="img/calendario.gif" width="17" height="16" border="0" align="absmiddle" title="CALENDARIO" onclick="calendariof();return false;" id="launch_fechaf" style="cursor:pointer;">&nbsp;&nbsp;</td></tr>
					<tr><td class="td2" colspan=2><input id="enviar" type="button" onclick="buscamovil();" value='Enviar'><input type="reset" onclick="resetear('sac');"></td></tr>
					<tr><td class="td2" colspan=2>Descargar Detalle </br><input type='image' src='img/descargar.png' style='width:70 px; height:30px;'></td></tr>
					</table>
					</form>
				</p>
				
				<h2>Resultado de B&uacute;squeda</h2>
				<p>
				<div id="res_lista">
				</div>
				</p>
			</div>
			<div id="clear"></div>
		</div>
		<div id="footer">
			<?php include ('foot.php');?>
		</div>
	</div>
</body>
</html>