<?
include ("config.inc.mo.php");

//error_reporting(0);
$ComandoInscripcion = array ("%");

/* SERVICIO OPCION HAY QUE AGREGAR UN CAMPO PARA USALIMITE CHAR(1) usado para preguntas y respuestas */
/*
curl "http://localhost/celmedia_colombia/1113_Bolsa_Dane/1113_processmessage.php?from=57111111111&to=87922&message=test&systemId=&operator=mispColombia&operatorId=mispColombia&messageId=xx123&now=1";
*/
/********************************
			FCN_ESCRIBEMOMT: Funcion encargada de escribir todo lo que entra y sali de la dinamica
			RECIBE:
						   $db:                     Objeto de conexion
						   $operador:         		Operador
						   $numerocorto: 			Numero corto de la dinamica
						   $idservicioopcion:       ID servicio opcion (si existe)
						   $fechahorarecibido:      Fecha y hora del msg recibido
						   $movil:                  Movil que realizo el envio.
						   $msg:                    Msg procesado
						   $response:               Msg que se regreso.
						   $Valido:					Si el mensaje se proceso como valido o no (S o N)
			REGRESA CUERPO:
						   Nada
			REGRESA ENCABEZADO:
						   Nada
			***********************************/
			FUNCTION Fcn_escribemomt ($db, $operador, $numerocorto, $idservicioopcion, $fechahorarecibido, $movil, $msg, $response, $Valido)
				{
					IF ($numerocorto == '') $numerocorto = 'NULL';
					IF ($idservicioopcion == '') $idservicioopcion = 'NULL';
					$fechahoraresponse=date("Y-m-d H:i:s");
					$SQL = "INSERT INTO cel_momt (idservicio, operador, numerocorto, idservicioopcion, fechahorarecibido, movil, mensaje, fechahoraresponse, fechahorarequest, response, SentidoCobro, Valido) ".
							" VALUES (".IDSERVICIO.", '$operador', $numerocorto, $idservicioopcion, '$fechahorarecibido', '$movil', '$msg', '$fechahoraresponse', '$fechahorarecibido', '$response', 'O', '$Valido');";
					$db -> Query ($SQL);
				}
			FUNCTION Fcn_procesarMO ($db, $operator, $to,$time_recive, $from, $message){
					$fechahoraresponse=date("Y-m-d H:i:s");
					$SQL = "INSERT INTO ".IDSERVICIO."_momt (idservicio, operador, numerocorto, idservicioopcion, fechahorarecibido, movil, mensaje, fechahoraresponse, response, SentidoCobro, Valido) ".
							" VALUES (".IDSERVICIO.", '$operator', $to, 0, '$time_recive', '$from', '$message', '$fechahoraresponse','message accepted for process.', 'O', 'S');";
					$db -> Query ($SQL);
			}
			/*************************************************
			Fcn_ProcessPublicacion: Funcion encargada de procesar las campa�as TIPO PUBLICACIONES en TV o Menciones en RADIO
			RECIBE:
						   $CELMEDIA_COLOMBIA:		Objeto de la base de datos.
						   $SERVICIO:           Arreglo con todas las configuraciones del servicio
						   $message:          	Mensaje a procesar
						   $from:               Movil 
						   $operator:          	Nombre del operador
						   $idservicioopcion:   Opcion a la cual entro a la trivia.
						   $subopcion:          Comando con el que entro a la trivia
			REGRESA ENCABEZADO:
						   $pol_puntaje:        Puntaje acumulado por el participante (Si aplica).
						   $pol_error:          Codigo de error (0 ok)
						   $response:           Mensaje de terminacion del proceso
			REGRESA CUERPO:
						   BOOLEAN (False si hubo fallo)
			****************************************************/
			FUNCTION Fcn_ProcessPublicacion ($db, $SERVICIO, $message, $from, $operator, $idservicioopcion, &$pol_error, &$response)
				{
					$pol_puntaje = 0;
					$SQL = "SELECT idinscripcion, idpreguntaactual, puntaje, inscrito ".
								  "              FROM ".IDSERVICIO."_inscripcion ".
								  "              WHERE movil = '".$from."';";
					$RES = $db -> Query ($SQL);
					IF (!($RES))
					{
						$pol_puntaje = 0;
						$pol_error = -10;
						$response = '-10 Fallo al leer la inscripcion.';
						RETURN FALSE;
					}

					$INSCRIPCION = $db -> fetchAssoc ($RES);
					$ll_idinscripcion = $INSCRIPCION['idinscripcion'];
					$ll_idpreguntaactual = $INSCRIPCION['idpreguntaactual'];
					$pol_puntaje = $INSCRIPCION['puntaje'];
					$response = $SERVICIO['msgcorrecto'];
					//var_dump ($INSCRIPCION);
					switch ($INSCRIPCION['inscrito'])
					{
						/****************************************************************************
						SI ESTA INSCRITO (YA HABIA MANDADO MENSAJES PREVIOS)
						*****************************************************************************/
						case 'S':
						case 'N':
						/* si esta inscrito */
						$pol_puntaje = $pol_puntaje + 1;
						$SQL = "UPDATE ".IDSERVICIO."_inscripcion".
													 " SET puntaje = $pol_puntaje ".
													 " WHERE idinscripcion = ".$ll_idinscripcion.";";
						$RES = $db -> Query ($SQL);
						IF (!($RES))
							{
										 $response = '-20 Fallo al actualizar la inscripcion '.$SQL;
										 $pol_error = -20;
										 RETURN FALSE;
							}
									 
						$SQL = "INSERT INTO ".IDSERVICIO."_votacion (idinscripcion, fechahora, msg, puntosrecibidos, movil, operador, valido, respuesta)".
													 " VALUES ($ll_idinscripcion, now(), '$message', 1, $from, '$operator', 'S', '$response');";
						$RES = $db -> Query ($SQL);
						IF (!($RES))
							{
										 $response = '-30 Fallo al insertar la votacion (insc S)';
										 $pol_error = -30;
										 RETURN FALSE;
							}
						RETURN TRUE;

						/************************/
						/* NO HAY VOTOS PREVIOS */
						/************************/
						DEFAULT : 
							$SQL = "INSERT INTO ".IDSERVICIO."_inscripcion (movil, operador, puntaje, inscrito)".
														" VALUES ($from, '$operator', 1, 'S');";
							$RES = $db -> Query ($SQL);
							IF (!($RES))
								{
									$response = '-40 Fallo al registrar la inscripcion ' . $SQL;
									$pol_error = -40;
									RETURN FALSE;
								}
							$VAL = $db -> fetchAssoc ($RES);
							$ll_idinscripcion = $db -> insertId();

							$SQL = " INSERT INTO ".IDSERVICIO."_votacion (idinscripcion, fechahora, msg, puntosrecibidos, movil, operador, valido, respuesta)".
														"              VALUES ($ll_idinscripcion, now(), '$message', 1, $from, '$operator', 'S', '$response');";
							$RES = $db -> Query ($SQL);
							IF (!($RES))
								{
									$pol_error = -50;
									$response = '-50 Fallo al insertar en la votacion.'.$SQL;
									RETURN FALSE;
								}
										
										
							$pol_error = 0;
							RETURN TRUE;
					}
				}

				
			FUNCTION Fcn_LeeServicio ($db, &$SERVICIO, &$pol_error, &$pos_response)
				{
					$SQL = "SELECT nombre, descripcion, activo, php, desinscribir, urlmo, logmo, ".
							"      msgerror, logmt, tablamo, tablamt, fechainicio, fechatermino, logmt, lognoprocesado, logprocesado, ".
							"      tablanoprocesado, msgopcionincorrecta, msgcorrecto, usaestadistica, enviaestadistica, tablavotacion, ".
							"      tablaprocesado, now() as ahora ".
							"      FROM cel_servicio ".
							"      where (idservicio = ".IDSERVICIO.");";
					$RES = $db -> Query ($SQL);
					IF (!($RES))
						{
							$pol_error = -10;
							$pos_response = 'Fallo al leer la campa�a. '.$SQL. $db -> getlasterror ();
							RETURN FALSE;
						}
					IF ($db -> countRows ($RES) > 0)
						{
							$SERVICIO = $db -> fetchAssoc ($RES);
							IF ($SERVICIO['activo'] == 'S')
								{	
									// El servicio esta activo
									IF ($SERVICIO ['fechainicio'] == '')
									{
										IF ($SERVICIO ['fechatermino'] == '')
										{
											$pos_response = 'ok';
										}
										else
										{
											if (strtotime($SERVICIO ['fechatermino']) > strtotime($SERVICIO['ahora'])) 
												{
													$pol_error = 0;
													$pos_response = 'ok';
												}
											else
												{
													$pol_error = 0;
                                                    $SERVICIO['activo'] = 'N';
													$pos_response = 'Fecha expirada '.$SERVICIO['msgerror'];
												}
										}
									}
									ELSE
									{
										IF ((strtotime($SERVICIO ['fechainicio']) < strtotime($SERVICIO['ahora'])) && (strtotime($SERVICIO ['fechatermino']) > strtotime($SERVICIO['ahora'])) )
											{
												$pol_error = 0;
												$pos_response = 'ok';
											}
										else
											{
												$pol_error = 0;
                                                $SERVICIO['activo'] = 'N';
												$pos_response = $SERVICIO['msgerror'];
											}
									}
									IF ($pol_error <> 0)
										RETURN FALSE;
									ELSE
										RETURN TRUE;
								}
							ELSE
								{
									$pol_error = 0;
									IF ($SERVICIO['msgerror'] == '')
										$SERVICIO['msgerror'] = 'El concurso ha finalizado. Gracias por participar.';
									$pos_response = $SERVICIO['msgerror'];
									RETURN TRUE;
								}
						}
					ELSE
						{
							$pol_error = -40;
							$pos_response = 'La campa�a no existe ';
							RETURN FALSE;
						}
				}
			/********************************
			Fcn_LeeServicioOpcion: Funcion encargada de leer las opciones del servicio
			RECIBE:
				$db: 		Objeto de conexion
				$msg:		Msg a procesar
			REGRESA CUERPO:
				BOOLEAN:
					False si el servicio no existe o tuvo un fallo en la lectura
			REGRESA ENCABEZADO:
					$SERVICIOOPCION: Configuracion del servicioopcion
					POL_Error: Codigo de error (0: ok)
					POS_RESPONSE: mensaje de terminacion (ok si pol_error = 0)
			***********************************/
			FUNCTION Fcn_LeeServicioOpcion ($db, $msg, $SERVICIO, &$SERVICIOOPCION, &$pol_error, &$response)
				{
					$op = strtolower(substr($msg,0,3));
					// ahora se validan las opciones
					$SQL = "SELECT cel_servicioopcion.*, cel_serviciosubopcion.opcion as subopcion ".
							"	FROM cel_servicioopcion, cel_serviciosubopcion".
							"	WHERE cel_servicioopcion.idservicioopcion = cel_serviciosubopcion.idservicioopcion AND".
							"         cel_servicioopcion.idservicio = ".IDSERVICIO." AND ".
							"      	cel_servicioopcion.activo = 'S' and " .
							"         lower(cel_serviciosubopcion.opcion) = '".$op."';";
					$RES = $db -> Query ($SQL);
					IF (!($RES))
						{
							// Fallo al leer la campa�a
							$pol_error = -50;
							$response = "Fallo al leer la opcion de la campana";
							RETURN FALSE;
						}
					//echo $SQL;
					$SERVICIOOPCION = $db -> fetchAssoc ($RES);
					switch ($db -> countRows ($RES))
						{
							case 1:
								// SI HAY UN REGISTRO ENTRO EN LA OPCION CORRECTA
								$pol_error = 0;
								$response = "Ok";
								RETURN TRUE;break;
							CASE 0:
								// Si no hay opciones
								$SQL = "SELECT * ".
										"	FROM cel_servicioopcion, cel_serviciosubopcion".
										"	WHERE cel_servicioopcion.idservicioopcion = cel_serviciosubopcion.idservicioopcion AND".
										"         cel_servicioopcion.idservicio = ".IDSERVICIO." AND ".
										"      	cel_servicioopcion.activo = 'S' and " .
										"         lower(cel_serviciosubopcion.opcion) = '%';";
								$RES = $db -> Query ($SQL);
								IF (!($RES))
									{
										$pol_error = -30;
										$response = "No se pudo leer %";
										RETURN  FALSE;
									}
								switch ($db -> countRows ($RES))
									{
										case 1:
											// Se leyo un registro con la opcion '%'
											$SERVICIOOPCION = $db -> fetchAssoc ($RES);
											$pol_error = 0;
											$response = "Ok";
											RETURN TRUE;break;
										case 0:
											// No hay registro con %
											$pol_error = -10;
											IF ($SERVICIO['msgopcionincorrecta'] == '')
												$response = 'No hemos entendido tu mensaje.';
											ELSE
												$response = $SERVICIO['msgopcionincorrecta'];
											RETURN FALSE;break;
										default:
											// Hay mas de un registro para el %
											$pol_error = -20;
											$response = "El comando % esta configurado mas de una vez";
											RETURN FALSE;
									}
								break;
							DEFAULT:
								// Si hay mas de una opcion
								$pol_error = -40;
								$response = "El comando ".$msg." esta configurado mas de una vez";
								RETURN FALSE;
						}
				}
				
			/********************************
			Fcn_EscribeEstadistica: Funcion encargada de escribir las estadisticas
			RECIBE:
				$db: 		Objeto de conexion
				$msg:		Msg a procesar
			REGRESA CUERPO:
				BOOLEAN:
					False si el servicio no existe o tuvo un fallo en la lectura
			REGRESA ENCABEZADO:
					$SERVICIOOPCION: Configuracion del servicioopcion
					POL_Error: Codigo de error (0: ok)
					POS_RESPONSE: mensaje de terminacion (ok si pol_error = 0)
			***********************************/
			FUNCTION Fcn_EscribeEstadistica ($db, $Activo, $IdOperador, $NumeroCorto, $Movil, $FechaRecepcion, $Msg, &$IdEstadistica)
				{
					IF ($Activo != 'S')
						$Activo = 'N';
					$SQL = "INSERT INTO cel_estadisticatmp (idoperador, idnumerocorto, idservicio, movil, activo, fechahora, msg)".
							"	values (".$IdOperador.", ".$NumeroCorto.", ".IDSERVICIO.", '".$Movil."', '".$Activo."', '".$FechaRecepcion."', '".$Msg."');";
					$db -> Query ($SQL);
					$IdEstadistica = $db -> insertId ();
				}
				
			FUNCTION FCN_EnviaEstadistica ($IdEstadistica)
				{
					$Command = 'curl "'.CURL_ESTDISTICA.$IdEstadistica.'" & ';
					//echo $Command;
					exec ($Command);
					$response = "Actualizacion correcta ";
					RETURN TRUE;
				}
			
			/*************************************************
			Fcn_ProcessPregunta: Funcion encargada de procesar las campa�as TIPO pregunta o trivia
			RECIBE:
				$db:		Objeto de la base de datos.
				$message: 	Mensaje a procesar
				$from:		Movil 
				$operator:	Nombre del operador
				$preguntaaleatoria:	Si la campa�a tiene preguntas aleatorias
				$preguntadoble:		Si la campa�a tiene pregunta sencilla o doble
				$puntajeinicial:	Cual es el puntaje inicial de la campa�a si esta en preguntas sencillas.
				$puntajeinicialdoble:	Cual es el puntaje si la campa�a esta en dobles.
				$idservicioopcion:		Opcion a la cual entro a la trivia.
				$subopcion:				Comando con el que entro a la trivia
			REGRESA ENCABEZADO:
				$pol_puntaje:			Puntaje acumulado por el participante.
				$pol_error:				Codigo de error (0 ok)
				$response:				Mensaje de terminacion del proceso
			REGRESA CUERPO:
				BOOLEAN (False si hubo fallo)
			****************************************************/
			FUNCTION Fcn_ProcessPregunta ($db, $message, $from, $operator, $preguntaaleatoria, 
										$preguntadoble, $puntajeinicial, 
										$puntajeinicialdoble,
										$idservicioopcion, 
										$subopcion, $msgopcionincorrecta, 
										&$pol_puntaje, &$pol_error, &$response)
				{
					global $ComandoInscripcion;
					$pol_puntaje = 0;
					$SQL = "SELECT idinscripcion, idpreguntaactual, puntaje, inscrito ".
							"	FROM ".IDSERVICIO."_inscripcion ".
							"	WHERE movil = '".$from."';";
					$RES = $db -> Query ($SQL);
					IF (!($RES))
						{
							$pol_puntaje = 0;
							$pol_error = -10;
							$response = '-10 Fallo al leer la inscripcion.';
							RETURN FALSE;
						}
						
					$INSCRIPCION = $db -> fetchAssoc ($RES);
					$ll_idinscripcion = $INSCRIPCION['idinscripcion'];
					$ll_idpreguntaactual = $INSCRIPCION['idpreguntaactual'];
					$pol_puntaje = $INSCRIPCION['puntaje'];
					
					/* obtiene el id pregunta salir */
					$SQL = "SELECT idpregunta, msgok ".
							"	FROM ".IDSERVICIO."_pregunta".
							"	WHERE lower(respuestacorrecta) = 'salir';";
					$RES = $db -> Query ($SQL);
					IF (!($RES))
						{
							$pol_error = -20;
							$response = '-20 Fallo al leer la pregunta "salir".';
							RETURN FALSE;
						}
					$PREGUNTASALIR = $db -> fetchAssoc ($RES);
					IF ($PREGUNTASALIR['idpregunta'] <= 0 || $PREGUNTASALIR['idpregunta'] == '')
						{
							$pol_error = -30;
							$response = '-30 No esta definida la pregunta "salir".';
							RETURN FALSE;
						}
					
					/* obtiene el id pregunta help */
					/* obtiene el id pregunta salir */
					$SQL = "SELECT idpregunta, msgok ".
							"	FROM ".IDSERVICIO."_pregunta".
							"	WHERE lower(respuestacorrecta) = 'help';";
					$RES = $db -> Query ($SQL);
					IF (!($RES))
						{
							$pol_error = -40;
							$response = '-40 Fallo al leer la pregunta "help".';
							RETURN FALSE;
						}
					$PREGUNTAHELP = $db ->  fetchAssoc ($RES);
					IF ($PREGUNTAHELP['idpregunta'] <= 0 || $PREGUNTAHELP['idpregunta'] == '')
						{
							$pol_error = -50;
							$response = '-50 No esta definida la pregunta "help".';
							RETURN FALSE;
						}
					//var_dump ($INSCRIPCION);
					switch ($INSCRIPCION['inscrito'])
						{
							case 'S':
								/* si esta inscrito */
								if (strtolower ($message) == 'help' || strtolower ($message) == 'ayuda' || strtolower ($message) == 'salir' || strtolower ($message) ==  'out' || strtolower ($message) == 'baja' || strtolower ($message) == 'parar' || strtolower ($message) == 'stop')
									{
										IF ($preguntadoble == 'S')
											{
												$pol_puntaje = $pol_puntaje + $puntajeinicialdoble;
												$ll_puntajeinicial = $puntajeinicialdoble;
											}
										ELSE
											{
												$pol_puntaje = $pol_puntaje + $puntajeinicial;
												$ll_puntajeinicial = $puntajeinicial;
											}
										IF (strtolower ($message) == 'help' || strtolower ($message) == 'ayuda')
											$response = $PREGUNTAHELP['msgok'];
										ELSE
											$response = $PREGUNTASALIR['msgok'];
										$response = str_replace ('_PUNTOS_', $pol_puntaje, $response);
										
										$SQL = "INSERT INTO ".IDSERVICIO."_votacion (idinscripcion, fechahora, msg, puntosrecibidos, movil, operador, valido, respuesta) ".
											" VALUES (".$ll_idinscripcion.",  now(), '".$message."', ".$ll_puntajeinicial.", ".$from.", '".$operator."', 'N', '".$response."');";
										$RES = $db -> Query ($SQL);
										IF (!($RES))
											{
												$pol_error = -60;
												$response = '-60 Fallo al insertar la votacion. '.$SQL;
												RETURN FALSE;
											}
										$SQL = "UPDATE ".IDSERVICIO."_inscripcion ".
												" SET puntaje = ".$pol_puntaje.
												" WHERE idinscripcion = ".$ll_idinscripcion.";";
										$RES = $db -> Query ($SQL);
										IF (!($RES))
											{
												$response = '-70 Fallo al actualizar el puntaje. '.$SQL;
												$pol_error = -70;
												RETURN FALSE;
											}
										$pol_error = 0;
										RETURN TRUE;
									}
								// No fue ni help, ni salir.
								// Valida el comando
								/* obtiene los datos de la pregunta actual */
								$SQL = " SELECT respuestacorrecta, msgerror, msgok, puntosok, puntoserror ".
										" FROM ".IDSERVICIO."_pregunta".
										" WHERE idpregunta = ".$ll_idpreguntaactual.";";
								$RES = $db -> Query ($SQL);
								IF (!($RES))
									{
										$response = '-100 Fallo al obtener la pregunta actual (insc S)';
										$pol_error = -100;
										RETURN FALSE;
									}
								$ACTUAL = $db ->  fetchAssoc ($RES);
								$ls_respuestacorrecta = $ACTUAL['respuestacorrecta'];
								$ls_msgerror =			$ACTUAL['msgerror'];
								$ls_msgok = 			$ACTUAL['msgok'];
								$ll_puntosok = 			$ACTUAL['puntosok'];
								$ll_puntoserror = 		$ACTUAL['puntoserror'];
								
								if (strtolower($message) == strtolower($ls_respuestacorrecta))
									{
										$pol_puntaje = $pol_puntaje + $ll_puntosok;
										$ls_msg = $ls_msgok;
										$ll_puntajeinicial = $ll_puntosok;
										$ls_valido = 'S';
									}
								ELSE
									{
										$pol_puntaje = $pol_puntaje + $ll_puntoserror;
										$ls_msg = $ls_msgerror;
										$ll_puntajeinicial = $ll_puntoserror;
										$ls_valido = 'N';
									}
								
								if ($preguntaaleatoria == 'S' and $preguntadoble == 'S')
									$SQL = "SELECT idpregunta ".
											"	FROM ".IDSERVICIO."_pregunta ".
											"	WHERE doble = 'S' AND ".
											"		idpregunta <> ".$ll_idpreguntaactual.
											" 	ORDER BY rand() ".
											"	LIMIT 1;";
								ELSEIF ($preguntaaleatoria == 'S' and $preguntadoble == 'N')
									$SQL = "SELECT idpregunta ".
											" FROM ".IDSERVICIO."_pregunta ".
											" WHERE doble <> 'S' AND ".
											"		idpregunta <> ".$ll_idpreguntaactual.
											" ORDER BY RAND() ".
											" LIMIT 1;";
								ELSEIF ($preguntaaleatoria == 'N' and $preguntadoble == 'S')
									$SQL = "SELECT idpreguntasigdoble as idpregunta ".
											" FROM ".IDSERVICIO."_pregunta ".
											" WHERE idpregunta = ".$ll_idpreguntaactual;
								ELSE //($preguntaaleatoria = 'N' and $preguntadoble = 'N') then
									$SQL = "SELECT idpreguntasiguiente as idpregunta ".
											" FROM ".IDSERVICIO."_pregunta ".
											" WHERE idpregunta = ".$ll_idpreguntaactual;
								$RES = $db -> Query ($SQL);
								IF (!($RES))
									{
										$response = '-110 Fallo al obtener la pregunta siguiente (insc S)';
										$pol_error = -110;
										RETURN FALSE;
									}
								$VAL = $db -> fetchAssoc ($RES);
								$ll_idpreguntasiguiente = $VAL['idpregunta'];
								
								IF ($ll_idpreguntasiguiente <= 0 or $ll_idpreguntasiguiente == '')
									// si no hay pregunta siguiente lo deja en la pregunta actual
									$ll_idpreguntasiguiente = $ll_idpreguntaactual;
								
								$SQL = "UPDATE ".IDSERVICIO."_inscripcion".
										" SET puntaje = ".$pol_puntaje.", ".
										" 	idpreguntaactual = ".$ll_idpreguntasiguiente.
										" WHERE idinscripcion = ".$ll_idinscripcion.";";
								$RES = $db -> Query ($SQL);
								IF (!($RES))
									{
										$response = '-120 Fallo al actualizar la inscripcion (insc S)';
										$pol_error = -120;
										RETURN FALSE;
									}
									
								$SQL = "SELECT pregunta".
										" FROM ".IDSERVICIO."_pregunta".
										" WHERE idpregunta = ".$ll_idpreguntasiguiente.";";
								$RES = $db -> Query ($SQL);
								IF (!($RES))
									{
										$response = '-130 Fallo al leer la pregunta siguiente(insc S)';
										$pol_error = -130;
										RETURN FALSE;
									}
								$PREGUNTA = $db -> fetchAssoc ($RES);
								$ls_msg = $ls_msg.' '.$PREGUNTA['pregunta'];
								$ls_msg = str_replace ('_PUNTOS_', $pol_puntaje, $ls_msg);
								$pol_error = 0;
								$response = $ls_msg;
								$SQL = "INSERT INTO ".IDSERVICIO."_votacion (idpregunta, idinscripcion, fechahora, msg, puntosrecibidos, movil, operador, valido, respuesta)".
										" VALUES (".$ll_idpreguntasiguiente.", ".$ll_idinscripcion.", now(), '".$message."', ".$ll_puntajeinicial.", ".$from.", '".$operator."', '".$ls_valido."', '".$response."');";
								$RES = $db -> Query ($SQL);
								IF (!($RES))
									{
										$response = '-140 Fallo al insertar la votacion (insc S)';
										$pol_error = -140;
										RETURN FALSE;
									}
								RETURN TRUE;break;
							/****************************************************************************
							NO ESTA INSCRITO. PERO YA HABIA MANDADO MENSAJES PREVIOS 
							*****************************************************************************/
							CASE 'N':
								/* si no esta inscrito */
								if (strtolower ($message) == 'help' || strtolower ($message) == 'ayuda' ||
									strtolower ($message) == 'salir' || strtolower ($message) ==  'out' || 
									strtolower ($message) == 'baja' || strtolower ($message) == 'parar' || 
									strtolower ($message) == 'stop')
									{
										IF ($preguntadoble == 'S')
											{
												$pol_puntaje = $pol_puntaje + $puntajeinicialdoble;
												$ll_puntajeinicial = $puntajeinicialdoble;
											}
										ELSE
											{
												$pol_puntaje = $pol_puntaje + $puntajeinicial;
												$ll_puntajeinicial = $puntajeinicial;
											}
										IF (strtolower ($message) == 'help' || strtolower ($message) == 'ayuda')
											$response = $PREGUNTAHELP['msgok'];
										ELSE
											$response = $PREGUNTASALIR['msgok'];
										$response = str_replace ('_PUNTOS_', $pol_puntaje, $response);
										
										$SQL = "UPDATE ".IDSERVICIO."_inscripcion ".
												" SET puntaje = ".$pol_puntaje.
												" WHERE idinscripcion = ".$ll_idinscripcion.";";
										$RES = $db -> Query ($SQL);
										IF (!($RES))
											{
												$response = '-160 Fallo al actualizar el puntaje '.$SQL;
												$pol_error = -160;
												RETURN FALSE;
											}
										$SQL = "INSERT INTO ".IDSERVICIO."_votacion (idinscripcion, fechahora, msg, puntosrecibidos, movil, operador, valido, respuesta) ".
											" VALUES (".$ll_idinscripcion.",  now(), '".$message."', ".$ll_puntajeinicial.", ".$from.", '".$operator."', 'N', '".$response."');";
										$RES = $db -> Query ($SQL);
										IF (!($RES))
											{
												$pol_error = -150;
												$response = '-150 Fallo al insertar la votacion (inscripcion S).'.$SQL;
												RETURN FALSE;
											}
										$pol_error = 0;
										RETURN TRUE;
									}
									
								IF (in_array (strtolower($message), $ComandoInscripcion) || in_array ('%', $ComandoInscripcion))
									{
										if ($preguntaaleatoria == 'S' and $preguntadoble == 'S')
											{
												$pol_puntaje = $pol_puntaje + $puntajeinicialdoble;
												$ll_puntajeinicial = $puntajeinicialdoble;
												$SQL = "SELECT idpregunta ".
														"	FROM ".IDSERVICIO."_pregunta ".
														"	WHERE doble = 'S' AND ".
														"		numeropregunta >= (SELECT min(numeropregunta)".
														"								FROM ".IDSERVICIO."_pregunta".
														"								WHERE numeropregunta > 0 AND".
														"									doble = 'S' ".
														"								ORDER BY numeropregunta".
														"								LIMIT 1)".
														"	ORDER BY rand() ".
														"	LIMIT 1;";
											}
										ELSEIF ($preguntaaleatoria == 'S' and $preguntadoble == 'N')
											{
												$pol_puntaje = $pol_puntaje + $puntajeinicial;
												$ll_puntajeinicial = $puntajeinicial;
												$SQL = "SELECT idpregunta ".
														"	FROM ".IDSERVICIO."_pregunta ".
														"	WHERE doble <> 'S' AND ".
														"		numeropregunta >= (SELECT min(numeropregunta)".
														"								FROM ".IDSERVICIO."_pregunta".
														"								WHERE numeropregunta > 0 AND".
														"									doble <> 'S' ".
														"								ORDER BY numeropregunta".
														"								LIMIT 1)".
														"	ORDER BY rand() ".
														"	LIMIT 1;";
											}
										ELSEIF ($preguntaaleatoria == 'N' and $preguntadoble == 'S')
											{
												$pol_puntaje = $pol_puntaje + $puntajeinicialdoble;
												$ll_puntajeinicial = $puntajeinicialdoble;
												$SQL = "SELECT idpregunta ".
														"	FROM ".IDSERVICIO."_pregunta ".
														"	WHERE doble = 'S' AND ".
														"		numeropregunta >= (SELECT min(numeropregunta)".
														"								FROM ".IDSERVICIO."_pregunta".
														"								WHERE numeropregunta > 0 AND".
														"									doble = 'S' ".
														"								ORDER BY numeropregunta".
														"								LIMIT 1)".
														"	ORDER BY numeropregunta".
														"	LIMIT 1;";
											}
										ELSE //($preguntaaleatoria = 'N' and $preguntadoble = 'N') then
											{
												$pol_puntaje = $pol_puntaje + $puntajeinicial;
												$ll_puntajeinicial = $puntajeinicial;
												 $SQL = "SELECT idpregunta ".
														"	FROM ".IDSERVICIO."_pregunta ".
														"	WHERE doble <> 'S' AND ".
														"		numeropregunta >= (SELECT min(numeropregunta)".
														"								FROM ".IDSERVICIO."_pregunta".
														"								WHERE numeropregunta > 0 AND".
														"									doble <> 'S' ".
														"								ORDER BY numeropregunta".
														"								LIMIT 1)".
														"	ORDER BY numeropregunta".
														"	LIMIT 1;";
											}
										$RES = $db -> Query ($SQL);
										IF (!($RES))
											{
												$response = '-190 Fallo al obtener la pregunta siguiente '.$SQL;
												$pol_error = -190;
												RETURN FALSE;
											}
										$VAL = $db -> fetchAssoc ($RES);
										$ll_idpreguntaactual = $VAL['idpregunta'];
										if ($ll_idpreguntaactual <= 0 || IS_NULL ($ll_idpreguntaactual))
											{
												$pol_error = -200;
												$response = '-200 No hay preguntas definidas.';
												RETURN FALSE;
											}
										
										
										$SQL = "SELECT pregunta".
												"	FROM ".IDSERVICIO."_pregunta".
												"	WHERE idpregunta = ".$ll_idpreguntaactual.";";
										$RES = $db -> Query ($SQL);
										IF (!($RES))
											{
												$pol_error = -230;
												$response = '-230 Fallo al obtener la pregunta actual.';
												RETURN FALSE;
											}
										$PREGUNTA = $db -> fetchAssoc ($RES);
										$ls_msg = $PREGUNTA['pregunta'];
										$ls_msg = str_replace ('_PUNTOS_', $pol_puntaje, $ls_msg);
										$response = $ls_msg;
										
										
										$SQL = "UPDATE ".IDSERVICIO."_inscripcion ".
												"	SET idpreguntaactual = ".$ll_idpreguntaactual.", ".
												"		puntaje = ".$pol_puntaje.", ".
												"		inscrito = 'S' ".
												"	WHERE idinscripcion = ".$ll_idinscripcion.";";
										$RES = $db -> Query ($SQL);
										IF (!($RES))
											{
												$pol_error = -210;
												$response = '-210 Fallo al actualizar la inscripcion.'.$SQL;
												RETURN FALSE;
											}
										$SQL = " INSERT INTO ".IDSERVICIO."_votacion (idpregunta, idinscripcion, fechahora, msg, puntosrecibidos, movil, operador, valido, respuesta)".
												"	VALUES (".$ll_idpreguntaactual.", ".$ll_idinscripcion.", now(), '".$message."', ".$ll_puntajeinicial.", ".$from.", '".$operator."', 'S', '".$response."');";
										$RES = $db -> Query ($SQL);
										IF (!($RES))
											{
												$pol_error = -220;
												$response = '-220 Fallo al insertar la votacion. '.$SQL;
												RETURN FALSE;
											}
										$pol_error = 0;
										RETURN TRUE;
									}
								ELSE
									{
										IF ($preguntadoble == 'S')
											{
												$ll_puntajeinicial = $puntajeinicialdoble;
												$pol_puntaje = $pol_puntaje + $puntajeinicialdoble;
											}
										ELSE
											{
												$pol_puntaje = $pol_puntaje + $puntajeinicial;
												$ll_puntajeinicial = $puntajeinicial;
											}
											
										$SQL = 'UPDATE '.IDSERVICIO.'_inscripcion '.
												' SET puntaje = '.$pol_puntaje.
												' WHERE idinscripcion = '.$ll_idinscripcion.';';
										$RES = $db -> Query ($SQL);
										IF (!($RES))
											{
												$pol_error = -235;
												$response = '-235 Fallo al actualizar el puntaje.';
												RETURN FALSE;
											}
											
										$response = $msgopcionincorrecta;
										$response = str_replace ('_PUNTOS_', $pol_puntaje, $response);
										$SQL = " INSERT INTO ".IDSERVICIO."_votacion (idinscripcion, fechahora, msg, puntosrecibidos, movil, operador, valido, respuesta)".
												"	VALUES (".$ll_idinscripcion.", now(), '".$message."', ".$ll_puntajeinicial.", ".$from.", '".$operator."', 'N', '".$response."');";
										$RES = $db -> Query ($SQL);
										IF (!($RES))
											{
												$pol_error = -240;
												$response = '-240 Fallo al insertar en la votacion.'.$SQL;
												RETURN FALSE;
											}
										
										$pol_error = 0;
										RETURN TRUE;
									}
							DEFAULT : 
								/* LA INSCRIPCION TIENE NULO, SIGNIFICA QUE NUNCA HABIA VOTADO */
								if (strtolower ($message) == 'help' || strtolower ($message) == 'ayuda' || 
									strtolower ($message) == 'salir' || strtolower ($message) ==  'out' || 
									strtolower ($message) == 'baja' || strtolower ($message) == 'parar' || 
									strtolower ($message) == 'stop')
									{
										
										IF ($preguntadoble == 'S')
											{
												$ll_puntajeinicial = $puntajeinicialdoble;
												$pol_puntaje = $puntajeinicialdoble;
											}
										ELSE
											{
												$pol_puntaje = $puntajeinicial;
												$ll_puntajeinicial = $puntajeinicial;
											}
										
										$SQL = "INSERT INTO ".IDSERVICIO."_inscripcion (movil, operador, puntaje, inscrito)".
												" VALUES ('".$from."', '".$operator."', ".$pol_puntaje.", 'N');";
										$RES = $db -> Query ($SQL);
										//var_dump($RES);
										IF (!($RES))
											{
												$response = '-250 Fallo al registrar la inscripcion.'.$SQL;
												$pol_error = -250;
												RETURN FALSE;
											}
										
										//$VAL = $db -> fetcharray ();
										$ll_idinscripcion = $db -> insertId();
										
										
										IF (strtolower ($message) == 'help' || strtolower ($message) == 'ayuda')
										{
												$response = $PREGUNTAHELP['msgok'];
										}
										ELSE
										{
											
											$response = $PREGUNTASALIR['msgok'];
											$response = str_replace ('_PUNTOS_', $pol_puntaje, $response);
										}
										
										$SQL = " INSERT INTO ".IDSERVICIO."_votacion (idinscripcion, fechahora, msg, puntosrecibidos, movil, operador, valido, respuesta)".
												"	VALUES (".$ll_idinscripcion.", now(), '".$message."', ".$ll_puntajeinicial.", ".$from.", '".$operator."', 'N', '".$response."');";
										$RES = $db -> Query ($SQL);
										IF (!($RES))
											{
												$pol_error = -260;
												$response = '-260 Fallo al insertar en la votacion.'.$SQL;
												RETURN FALSE;
											}
										$pol_error = 0;
										RETURN TRUE;
									}
								/* Si llego aqui no fue comando HELP ni SALIR, se evalua la votacion. */
								IF (in_array (strtolower($message), $ComandoInscripcion) || in_array ('%', $ComandoInscripcion))
									{
										if ($preguntaaleatoria == 'S' and $preguntadoble == 'S')
											{
												$pol_puntaje = $puntajeinicialdoble;
												$ll_puntajeinicial = $puntajeinicialdoble;
												$SQL = "SELECT idpregunta ".
														"	FROM ".IDSERVICIO."_pregunta ".
														"	WHERE doble = 'S' AND ".
														"		numeropregunta >= (SELECT min(numeropregunta)".
														"								FROM ".IDSERVICIO."_pregunta".
														"								WHERE numeropregunta > 0 AND".
														"									doble = 'S' ".
														"								ORDER BY numeropregunta".
														"								LIMIT 1)".
														"	ORDER BY rand() ".
														"	LIMIT 1;";
											}
										ELSEIF ($preguntaaleatoria == 'S' and $preguntadoble == 'N')
											{
												$pol_puntaje = $puntajeinicial;
												$ll_puntajeinicial = $puntajeinicial;
												$SQL = "SELECT idpregunta ".
														"	FROM ".IDSERVICIO."_pregunta ".
														"	WHERE doble <> 'S' AND ".
														"		numeropregunta >= (SELECT min(numeropregunta)".
														"								FROM ".IDSERVICIO."_pregunta".
														"								WHERE numeropregunta > 0 AND".
														"									doble <> 'S' ".
														"								ORDER BY numeropregunta".
														"								LIMIT 1)".
														"	ORDER BY rand() ".
														"	LIMIT 1;";
											}
										ELSEIF ($preguntaaleatoria == 'N' and $preguntadoble == 'S')
											{
												$pol_puntaje = $puntajeinicialdoble;
												$ll_puntajeinicial = $puntajeinicialdoble;
												$SQL = "SELECT idpregunta ".
														"	FROM ".IDSERVICIO."_pregunta ".
														"	WHERE doble = 'S' AND ".
														"		numeropregunta >= (SELECT min(numeropregunta)".
														"								FROM ".IDSERVICIO."_pregunta".
														"								WHERE numeropregunta > 0 AND".
														"									doble = 'S' ".
														"								ORDER BY numeropregunta".
														"								LIMIT 1)".
														"	ORDER BY numeropregunta".
														"	LIMIT 1;";
											}
										ELSE //($preguntaaleatoria = 'N' and $preguntadoble = 'N') then
											{
												 $pol_puntaje =  $puntajeinicial;
												 $ll_puntajeinicial = $puntajeinicial;
												 $SQL = "SELECT idpregunta ".
														"	FROM ".IDSERVICIO."_pregunta ".
														"	WHERE doble <> 'S' AND ".
														"		numeropregunta >= (SELECT min(numeropregunta)".
														"								FROM ".IDSERVICIO."_pregunta".
														"								WHERE numeropregunta > 0 AND".
														"									doble <> 'S' ".
														"								ORDER BY numeropregunta".
														"								LIMIT 1)".
														"	ORDER BY numeropregunta".
														"	LIMIT 1;";
											}
										$RES = $db -> Query ($SQL);
										IF (!($RES))
											{
												$response = '-290 Fallo al obtener la pregunta siguiente (insc NUL)';
												$pol_error = -290;
												RETURN FALSE;
											}
										$VAL = $db -> fetchAssoc ($RES);
										$ll_idpreguntaactual = $VAL['idpregunta'];
										if ($ll_idpreguntaactual <= 0 || IS_NULL( $ll_idpreguntaactual))
											{
												$pol_error = -300;
												$response = '-300 No hay preguntas definidas.';
												RETURN FALSE;
											}
										
										$SQL = "INSERT INTO ".IDSERVICIO."_inscripcion (idpreguntaactual, movil, operador, puntaje, inscrito)".
												" VALUES (".$ll_idpreguntaactual.", '".$from."', '".$operator."', ".$pol_puntaje.", 'S');";
										$RES = $db -> Query ($SQL);
										IF (!($RES))
											{
												$response = '-310 Fallo al registrar la inscripcion (otro insc NULL) ' . $SQL;
												$pol_error = -310;
												RETURN FALSE;
											}
										$VAL = $db -> fetchAssoc ($RES);
										$ll_idinscripcion = $db -> insertId();
										
										
										$SQL = "SELECT pregunta".
												"	FROM ".IDSERVICIO."_pregunta".
												"	WHERE idpregunta = ".$ll_idpreguntaactual.";";
										$RES = $db -> Query ($SQL);
										IF (!($RES))
											{
												$pol_error = -330;
												$response = '-330 Fallo al obtener la pregunta actual.';
												RETURN FALSE;
											}
										$PREGUNTA = $db -> fetchAssoc ($RES);
										$ls_msg = $PREGUNTA['pregunta'];
										$ls_msg = str_replace ('_PUNTOS_', $pol_puntaje, $ls_msg);
										$response = $ls_msg;
										$SQL = " INSERT INTO ".IDSERVICIO."_votacion (idinscripcion, fechahora, msg, puntosrecibidos, movil, operador, valido, respuesta)".
												"	VALUES (".$ll_idinscripcion.", now(), '".$message."', ".$ll_puntajeinicial.", ".$from.", '".$operator."', 'S', '".$response."');";
										$RES = $db -> Query ($SQL);
										IF (!($RES))
											{
												$pol_error = -260;
												$response = '-265 Fallo al insertar en la votacion.'.$SQL;
												RETURN FALSE;
											}
											
											
										$pol_error = 0;
										RETURN TRUE;
									}
								ELSE
									{
										IF ($preguntadoble == 'S')
											$pol_puntaje = $puntajeinicialdoble;
										ELSE
											$pol_puntaje = $puntajeinicial;
										$SQL = "INSERT INTO ".IDSERVICIO."_inscripcion (movil, operador, puntaje, inscrito)".
												" VALUES ('".$from."', '".$operator."', ".$pol_puntaje.", 'N');";
										$RES = $db -> Query ($SQL);
										IF (!($RES))
											{
												$response = '-340 Fallo al registrar la inscripcion (help insc NULL) '.$SQL;
												$pol_error = -340;
												RETURN FALSE;
											}
										
										$VAL = $db -> fetchAssoc ($RES);
										$ll_idinscripcion = $db -> insertId();
										$response = $msgopcionincorrecta;
										$response = str_replace ('_PUNTOS_', $pol_puntaje, $response);
										$SQL = " INSERT INTO ".IDSERVICIO."_votacion (idinscripcion, fechahora, msg, puntosrecibidos, movil, operador, valido, respuesta)".
												"	VALUES (".$ll_idinscripcion.", now(), '".$message."', ".$pol_puntaje.", ".$from.", '".$operator."', 'N', '".$response."');";
										$RES = $db -> Query ($SQL);
										IF (!($RES))
											{
												$pol_error = -260;
												$response = '-268 Fallo al insertar en la votacion.'.$SQL;
												RETURN FALSE;
											}
										$pol_error = 0;
										RETURN TRUE;
									}
						}
				}
				
			FUNCTION Fcn_ProcessAcumulativa ($db, $message, $from, $operator, $time_recive, $preguntaaleatoria ,
								$idservicioopcion, &$pol_error, &$response)
				{
                    $SQL = "SELECT idinscripcion, idpreguntaactual, puntaje, inscrito ".
							"	FROM ".IDSERVICIO."_inscripcion ".
							"	WHERE movil = '".$from."';";
					$RES = $db -> Query ($SQL);
					IF (!($RES))
						{
							$pol_puntaje = 0;
							$pol_error = -10;
							$response = '-10 Fallo al leer la inscripcion.';
							RETURN FALSE;
						}
						
					$INSCRIPCION = $db -> fetchAssoc ($RES);
					$ll_idinscripcion = $INSCRIPCION['idinscripcion'];
                    $ls_inscrito = $INSCRIPCION['inscrito'];
                    
                    IF ($preguntaaleatoria == 'S')
						$SQL = "SELECT msgok FROM ".IDSERVICIO."_acumulativarespuesta where idservicioopcion = ".$idservicioopcion." ORDER BY RAND() LIMIT 1;";
					ELSE
						$SQL = "SELECT msgok FROM ".IDSERVICIO."_acumulativarespuesta where idservicioopcion = ".$idservicioopcion." ORDER BY idacumulativarespuesta  LIMIT 1;";
					$RES = $db -> Query ($SQL);
					IF (!($RES))
						{
							$pol_error = -10;
							$response = 'Fallo al ejecutar la busqueda del mensaje';
							RETURN FALSE;
						}
					$VAL = $db -> fetchAssoc ($RES);
					$Salida = $VAL['msgok'];
					
					$SQL = "INSERT INTO ".IDSERVICIO."_acumulativa (idservicioopcion, movil, fechahora, msg, ".
							"				respuesta, operador,comando)".
							" VALUES (".$idservicioopcion.", '".$from."', '".$time_recive."', '".$message."', ".
							" '".$Salida."', '".$operator."','".$message."');";
					$RES = $db -> Query ($SQL);
					IF (!($RES))
						{
							$pol_error = -20;
							$response = ' -20 Fallo al ejecutar el insert de la votacion.' . " " . $SQL;
							RETURN FALSE;
						}
					
					$pol_error = 0;
					$response = $Salida;
                    
                    SWITCH ($ls_inscrito)
                        {
							case 'S':
                            case 'N':
                                    $SQL = " INSERT INTO ".IDSERVICIO."_votacion (idinscripcion, fechahora, msg, puntosrecibidos, movil, operador, valido, respuesta)".
											"	VALUES (".$ll_idinscripcion.", now(), '".$message."', 1, ".$from.", '".$operator."', 'S', '".$response."');";
                                    $RES = $db -> Query ($SQL);
                                    IF (!($RES))
                                        {
                                            $pol_error = -30;
                                            $response = '-30 Fallo al insertar en la votacion.'.$SQL;
                                            RETURN FALSE;
                                        }
										break;
                            DEFAULT :
                                IF ($ll_idinscripcion > 0)
                                    {
                                        $SQL = " INSERT INTO ".IDSERVICIO."_votacion (idinscripcion, fechahora, msg, puntosrecibidos, movil, operador, valido, respuesta)".
											"	VALUES (".$ll_idinscripcion.", now(), '".$message."', 1, ".$from.", '".$operator."', 'S', '".$response."');";
                                        $RES = $db -> Query ($SQL);
                                        IF (!($RES))
                                            {
                                                $pol_error = -40;
                                                $response = '-40 Fallo al insertar en la votacion.'.$SQL;
                                                RETURN FALSE;
                                            }
                                    }
                                ELSE
                                    {
                                        $SQL = "INSERT INTO ".IDSERVICIO."_inscripcion (movil, operador, puntaje, inscrito)".
												" VALUES ('".$from."', '".$operator."', 1, 'S');";
										$RES = $db -> Query ($SQL);
										IF (!($RES))
											{
												$response = '-50 Fallo al registrar la inscripcion '.$SQL;
												$pol_error = -50;
												RETURN FALSE;
											}
										
										$VAL = $db -> fetchAssoc ($RES);
										$ll_idinscripcion = $db -> insertId();
										
										$SQL = " INSERT INTO ".IDSERVICIO."_votacion (idinscripcion, fechahora, msg, puntosrecibidos, movil, operador, valido, respuesta)".
												"	VALUES (".$ll_idinscripcion.", now(), '".$message."', 1, ".$from.", '".$operator."', 'S', '".$response."');";
										$RES = $db -> Query ($SQL);
										IF (!($RES))
											{
												$pol_error = -60;
												$response = '-60 Fallo al insertar en la votacion. '.$SQL;
												RETURN FALSE;
											}
                                    }
                        }
                    $pol_error = 0;
					RETURN TRUE;
				}
				
				FUNCTION Fcn_LimpiaMT ($message)
				{
					$message = str_replace ( "�", "a", $message );
					$message = str_replace ( "�", "e", $message );
					$message = str_replace ( "�", "i", $message );
					$message = str_replace ( "�", "o", $message );
					$message = str_replace ( "�", "u", $message );
					$message = str_replace ( "�", "a", $message );
					$message = str_replace ( "�", "e", $message );
					$message = str_replace ( "�", "i", $message );
					$message = str_replace ( "�", "o", $message );
					$message = str_replace ( "�", "u", $message );

					$busca =array('�', '�', '�', 'Ñ' , '�?' , 'É' , '�?' , 'Ó' , 'Ú' , 'á' , 'é' , 'í' , 'ó' , 'ú' , 'ñ', '\"' , '\'' , '[' , '\\' , ']' , '^' , '´', '`' , '{' , '|' , '}' , '~' , '¼'   , '½'   , '¾'   , '¿' , 'Æ' , '²' , '®' , '©' , '°' , '¬');
					$cambia=array(' ', 'n', 'N', 'N' , 'A' , 'E' , 'I' , 'O' , 'U' , 'a' , 'e' , 'i' , 'o' , 'u' , 'n', ''   , ''   , ''  , ''   , ''  , ''  , ''  , '' , ''  , ''  , ''  , ''  , '1/4' , '1/2' , '3/4' , ''  , ''  , ''  , ''  , ''  , ''   , '');
					$message = str_replace($busca, $cambia, $message);

					$message = str_replace ( "\n", " ", trim ($message));
					$message = str_replace ( "�", "ni", $message );
					$message = str_replace ( "\r", "", $message );
					$message = str_replace ( "\\", "", $message );
					$message = str_replace ( "^[\n", "", $message );
					$message = str_replace ( "  "," ",$message);

				return $message;
			}
$now = 1;

$IP = FCN_OBTIENEIP ();

if (isset ( $_GET ['from'] ))		$from 		= $_GET ['from'];
if (isset ( $_GET ['to'] ))			$to 		= $_GET ['to'];
if (isset ( $_GET ['message'] ))	$message 	= $_GET ['message'];
if (isset ( $_GET ['systemId'] ))	$systemId 	= $_GET ['systemId'];
if (isset ( $_GET ['operator']))	$operator 	= $_GET ['operator'];
if (isset ( $_GET ['operatorId'] ))	$operatorId = $_GET ['operatorId'];
if (isset ( $_GET ['messageId'] ))	$messageId 	= $_GET ['messageId'];
if (isset ( $_GET ['now'] )) 		$now 		= $_GET ['now'];

$from = '573'.substr($from, -9);

//$message = trim($message);
$message = Fcn_Limpiamsg ($message);

$original = $_SERVER["REQUEST_URI"];
$time_recive = date ( "Y-m-d H:i:s" );

/***Validacion para que no entren delivery al proceso****/
$status = $message;
preg_match_all ( "/stat\:(.*?)err\:/",$status ,$resp);
$resp_f = trim($resp [1][0]);
if($resp_f != '')
{
	Fcn_escribeLog (LOGGENERAL, IDSERVICIO.' '.$time_recive.' '.$original);
	PRINT("<?xml version='1.0'?>\n".
				"	<kobe-data>\n".
				"		<response-code value='".$response."'/>\n".
				"	</kobe-data>\n");
	EXIT;
}

$CELMEDIA_COLOMBIA 		= new EyeMySQLAdap (kDatabaseHostname, kDatabaseUsername, kDatabasePassword, kDatabaseName);
//echo 'XXX: '.$CELMEDIA_COLOMBIA -> error ();
$CELMEDIA_COLOMBIA -> query("SET wait_timeout = 60;");
//echo 'XXX: '.$CELMEDIA_COLOMBIA -> error ();
$CELMEDIA_COLOMBIA -> query("SET interactive_timeout = 60;");
//echo 'YYY: '.$CELMEDIA_COLOMBIA -> error ();


Fcn_escribeLog (LOGGENERAL, IDSERVICIO.' '.$time_recive.' '.$original);


$SQL = "SELECT now() as fecha, idoperador, cobrarmt ".
		"	FROM cel_operador ".
		"	WHERE (lower(nombre) = lower('".$operator."') OR (lower(alias) = lower('".$operator."'))) AND idpais = " . IDPAIS . " ;";

$RES = $CELMEDIA_COLOMBIA -> Query ($SQL);
IF (!($RES))
	{
		$response = 'No se pudo leer el operador: '.$operator;
		Fcn_escribeLog (LOGNOPROCESADO, IDSERVICIO.' '.$time_recive.' '.$response.' '.$original);
		Fcn_escribemomt ($CELMEDIA_COLOMBIA, $operator, $to, '', $time_recive, $from, $message, $response,'N');
		print ("<?xml version='1.0'?>\n".
				"	<kobe-data>\n".
				"		<response-code value='".$response."qqqq'/>\n".
				"	</kobe-data>\n");
		$CELMEDIA_COLOMBIA -> close();
		RETURN;
	}

$r = $CELMEDIA_COLOMBIA -> fetchAssoc ($RES);
$IdOperador = $r['idoperador'];
$CobrarMt   = $r['cobrarmt'];
$time_recive = $r['fecha'];

IF ($IdOperador == '' || $IdOperador <= 0)
	{
		$response = 'No existe el operador: '.$operator.'; ';
		Fcn_escribeLog (LOGNOPROCESADO, IDSERVICIO.' '.$time_recive.' '.$response.' '.$original);
		Fcn_escribemomt ($CELMEDIA_COLOMBIA, $operator, $to, '', $time_recive, $from, $message, $response,'N');
		print ("<?xml version='1.0'?>\n".
				"	<kobe-data>\n".
				"		<response-code value='".$response."ttt'/>\n".
				"	</kobe-data>\n");
		$CELMEDIA_COLOMBIA -> close();
		RETURN;
	}

IF (!(FCN_VALIDAIP ($IP, $response)))
	{
		// Si hay fallo en la validacion de la IP
		Fcn_escribeLog (LOGNOPROCESADO, IDSERVICIO.' '.$time_recive.' '.$response.' '.$original);
		Fcn_escribemomt ($CELMEDIA_COLOMBIA, $operator, $to, '', $time_recive, $from, $message, $response,'N');
		print ("<?xml version='1.0'?>\n".
				"	<kobe-data>\n".
				"		<response-code value='".$response."iii'/>\n".
				"	</kobe-data>\n");
		$CELMEDIA_COLOMBIA -> close();
		RETURN;
	}

// Si no hay fallo en la validacion de la IP
// Valida que las variables vengan completas
$response = '';
if (! isset ( $from )) 
	$response = 'El campo FROM no puede venir vacio.';
ELSEIF (! isset ( $to )) 
	$response = 'El campo TO no puede venir vacio.';
ELSEIF (! isset ( $message )) 
	$response = 'El campo MESSAGE no puede venir vacio.';
/*ELSEIF (! isset ( $systemId ))
	$response = 'El campo SYSTEMID no puede venir vacio.';
*/ 
ELSEIF (! isset ( $operatorId ))
	$response = 'El campo OPERATORID no puede venir vacio.';
IF ($response <> '')
	{
		// Si hay fallo en la validacion de las variables
		Fcn_escribeLog (LOGNOPROCESADO, IDSERVICIO.' '.$time_recive.' '.$response.' '.$original);
		Fcn_escribemomt ($CELMEDIA_COLOMBIA, $operator, $to, '', $time_recive, $from, $message, $response,'N');
		print ("<?xml version='1.0'?>\n".
				"	<kobe-data>\n".
				"		<response-code value='".$response."ooo'/>\n".
				"	</kobe-data>\n");
		$CELMEDIA_COLOMBIA -> close();
		RETURN;
	}
	
	
IF (!(Fcn_LeeServicio ($CELMEDIA_COLOMBIA, $SERVICIO, $pol_error, $pos_response)))
	{
		// el servicio tuvo un fallo en la lectura
		$response = $pos_response;
		Fcn_escribeLog (LOGNOPROCESADO, IDSERVICIO.' '.$time_recive.' '.$response.' '.$original);
		Fcn_escribemomt ($CELMEDIA_COLOMBIA, $operator, $to, '', $time_recive, $from, $message, $response,'N');
		print ("<?xml version='1.0'?>\n".
			"	<kobe-data>\n".
			"		<response-code value='".$response."'/>\n".
			"	</kobe-data>\n");
		$CELMEDIA_COLOMBIA -> close();
		RETURN;
	}
	

IF ($SERVICIO ['activo'] <> 'S')
    {
        IF ($pos_response == '') 
            IF ($SERVICIO['msgerror'] == '')
                $pos_response = 'El concurso ha finalizado. Gracias por participar.';
            ELSE
                $pos_response = $SERVICIO['msgerror'];
         
                             
        $response = $pos_response;
		Fcn_escribeLog (LOGPROCESADO, IDSERVICIO.' '.$time_recive.' '.$response.' '.$original);
		Fcn_escribemomt ($CELMEDIA_COLOMBIA, $operator, $to, '', $time_recive, $from, $message, $response,'N');
		
			IF ($SERVICIO ['usaestadistica'] == 'S')
			{
				Fcn_EscribeEstadistica ($CELMEDIA_COLOMBIA,  'N', $IdOperador, $to, $from, $time_recive, $message, $IdEstadistica);
				IF ($SERVICIO['enviaestadistica'] == 'S')
					FCN_EnviaEstadistica ($IdEstadistica);
			}
        print ("<?xml version=\"1.0\"?>\n".
				"	<kobe-data>\n".
				"		<response-code value=\"ok\"/>\n".
				"		<kobe-message>\n".
				"			<source-address      value=\"".$to ."\" />\n".
				"			<destination-address value=\"".$_GET ['from']."\" />\n".
				"			<data-message>".$response."</data-message>\n".
				"			<message-id value=\"".$messageId."\" />\n".
				"			<cobrar value=\"".$CobrarMt."\" />\n".
				"			<SMPP-dispatch>\n".
				"				<automatic method=\"operator\" value=\"".$operatorId."\" />\n".
				"			</SMPP-dispatch>\n".
				"		</kobe-message>\n".
				"	</kobe-data>") ;
       $CELMEDIA_COLOMBIA -> close();
		RETURN;
    }
	
	// No existen respuestas a MO.
		$response='ok';
		Fcn_procesarMO ($CELMEDIA_COLOMBIA, $operator, $to,$time_recive, $from, $message); 
		Fcn_escribemomt ($CELMEDIA_COLOMBIA, $operator, $to, '0', $time_recive, $from, $message, 'message accepted for process.','S'); 
		Fcn_escribeLog (LOGPROCESADO, IDSERVICIO.' '.$time_recive.' '.$response.' '.$original);
		print ("<?xml version='1.0'?>\n".
		"	<kobe-data>\n".
		"		<response-code value='".$response."'/>\n".
		"	</kobe-data>\n");
		exit();
$CELMEDIA_COLOMBIA -> close();
?>