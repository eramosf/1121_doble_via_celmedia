<?php
	require ('login.head.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<meta name="author" content="celmedia" />
		<title>Celmedia :: Envios Masivos</title>
		<link rel="stylesheet" type="text/css" href="css/style.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="css/calendar-blue2.css" />
		<link rel="shortcut icon" href="favicon.ico" />
		<script type="text/javascript" src="js/prototype.js"></script>
		<script type="text/javascript" src="js/textsizer.js"></script>
		<script type="text/javascript" src="js/rel.js"></script>
		<script type="text/javascript" src="js/aplicaciones.js"></script>
		<script type="text/javascript" src="js/jscalendar/calendar.js"></script>
		<script type="text/javascript" src="js/jscalendar/lang/calendar-es.js"></script>
		<script type="text/javascript" src="js/jscalendar/calendar-setup.js"></script>
	</head>

<body onload="totalmsg();ver_lista_area();ver_envios_prog();ver_lista_grupos_agenda();contador(160);calendario();">
	<div id="wrap">
		<div id="top">
			<h2><a href="#" title="Back to main page">Celmedia</a></h2>
			<div id="menu">
				<?php include ('menu.php');?>
			</div>
			<h3>Tel : 57 1 6421002</h3>
		</div>
		<div id="content">
			<div style="float: right;">Usuario: <?=$reg_nombre?> (<a href="logout.php">Salir</a>)<p id="totalmsg"><p/></div>
			<div id="left">
				<h2>Agendar nuevo envio
					<small style="float:right" onclick="ver_lista_area();ver_lista_grupos_agenda();return false;">
						<a href="#!">Actualizar</a>
					</small>
				</h2>
				<p>
					<table>
					<tr><td class="td2">Fecha Envio</td><td><input type="text" id="fecha_envio"  value="" name="fecha_envio" readonly />&nbsp;<img src="img/calendario.gif" width="17" height="16" border="0" align="absmiddle" title="CALENDARIO" id="launch_fecha" onclick="calendario();return false;" style="cursor:pointer;">&nbsp;&nbsp;
						  <select name="inicio_hora" id="inicio_hora">				
									<option value="08" selected="selected">08</option>				
									<option value="09">09</option>				
									<option value="10">10</option>				
									<option value="11">11</option>				
									<option value="12">12</option>				
									<option value="13">13</option>				
									<option value="14">14</option>				
									<option value="15">15</option>				
									<option value="16">16</option>				
									<option value="17">17</option>				
									<option value="18">18</option>			
									<option value="19">19</option>				
									</select>
									<select  name="inicio_minuto" id="inicio_minuto">
									<option value="00" selected="selected">00</option>		
									<option value="01">01</option>	
									<option value="02">02</option>
									<option value="03">03</option>
									<option value="04">04</option>				
									<option value="05">05</option>				
									<option value="06">06</option>				
									<option value="07">07</option>				
									<option value="08">08</option>				
									<option value="09">09</option>				
									<option value="10">10</option>				
									<option value="11">11</option>				
									<option value="12">12</option>				
									<option value="13">13</option>				
									<option value="14">14</option>				
									<option value="15">15</option>				
									<option value="16">16</option>				
									<option value="17">17</option>				
									<option value="18">18</option>				
									<option value="19">19</option>				
									<option value="20">20</option>				
									<option value="21">21</option>				
									<option value="22">22</option>				
									<option value="23">23</option>				
									<option value="24">24</option>				
									<option value="25">25</option>				
									<option value="26">26</option>			
									<option value="27">27</option>				
									<option value="28">28</option>
									<option value="29">29</option>				
									<option value="30">30</option>				
									<option value="31">31</option>				
									<option value="32">32</option>				
									<option value="33">33</option>				
									<option value="34">34</option>				
									<option value="35">35</option>				
									<option value="36">36</option>				
									<option value="37">37</option>			
									<option value="38">38</option>				
									<option value="39">39</option>				
									<option value="40">40</option>				
									<option value="41">41</option>				
									<option value="42">42</option>				
									<option value="43">43</option>				
									<option value="44">44</option>				
									<option value="45">45</option>				
									<option value="46">46</option>				
									<option value="47">47</option>				
									<option value="48">48</option>				
									<option value="49">49</option>				
									<option value="50">50</option>				
									<option value="51">51</option>				
									<option value="52">52</option>				
									<option value="53">53</option>				
									<option value="54">54</option>				
									<option value="55">55</option>				
									<option value="56">56</option>				
									<option value="57">57</option>				
									<option value="58">58</option>				
									<option value="59">59</option>				
									</select></td></tr>
					<tr><td class="td2">&Aacute;rea</td>
						<td>
							<div id="lista_areas">
							<select>
								<option disabled selected>No tiene &aacute;reas habilitadas.</option>								
							</select>
							</div>
						</td>
					</tr>	
						<td class="td2">Grupo</td>
						<td>
							<div id="lista_grupos">
							<select>
								<option disabled selected>No tiene grupos habilitados.</option>								
							</select>
							</div>
							<input name="idsesion" id="idsesion" value="<?php echo $_SESSION['idsess'.NBOLSA];?>" type="hidden"/>
						</td>
					<tr><td class="td2">Mensaje</td><td><textarea id="mensaje" wrap="VIRTUAL" rows="10" cols="20" onkeyup="contador();"></textarea></td></tr>
						<tr><td class="td2"></td><td><span id="contador" style="margin:4px 0px;padding:3px;border:1px solid#ededed; background-color:#fafafa;color#888">0 contador de caracteres</span>  <br></td></tr>
					<tr><td class="td2" colspan=2><input id="enviar" type="submit" onclick="guardar_envio();"><input type="reset"></td></tr>
					</table>
				</p>
				<br><br>
				<h2>Envios agendados
					<small style="float:right" onclick="ver_envios_prog();return false;">
						<a href="#!">Actualizar</a>
					</small>
				</h2>
				<p>
					<div id="envios_prog">
						<img src="img/ajax-loader.gif" border="0" alt="Cargando...">						
					</div>
				</p>
				
			</div>
			<div id="clear"></div>
		</div>
		<div id="footer">
			<?php include ('foot.php');?>
		</div>
	</div>
</body>
</html>