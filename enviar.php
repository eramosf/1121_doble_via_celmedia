<?php
	require ('login.head.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<meta name="author" content="celmedia" />
		<title>Celmedia :: Envios Masivos</title>
		<link rel="stylesheet" type="text/css" href="css/style.css" media="screen" />
		<link rel="shortcut icon" href="favicon.ico" />
		<script type="text/javascript" src="js/prototype.js"></script>
		<script type="text/javascript" src="js/textsizer.js"></script>
		<script type="text/javascript" src="js/rel.js"></script>
		<script type="text/javascript" src="js/scriptaculous.js?load=effects"></script>
		<script type="text/javascript" src="js/aplicaciones.js"></script>
	</head>
<body onload="totalmsg();ver_lista_area();ver_lista_grupos_envio();ver_lista_areaunoauno();ver_lista_grupos_unoauno();contador(160);">
	<div id="wrap">
		<div id="top">
			<h2><a href="#" title="Back to main page">Celmedia</a></h2>
			<div id="menu">
				<?php include ('menu.php');?>
			</div>
		</div>
		<div id="content">
			<div style="float: right;">Usuario: <?=$reg_nombre?> (<a href="logout.php">Salir</a>)<p id="totalmsg"><p/></div>
			<div id="left">
				<h2>Envio Mensajes
					<small style="float:right" onclick="ver_lista_area();ver_lista_grupos_envio();return false;">
						<a href="#!">Actualizar</a>
					</small>
				</h2>
				<div style="text-align:right">(<a href="#" onclick="cambiar_envio();">Ver</a>)</div>
				<div id="e_normal">
				<p>
					<table>
					<tr><td class="td2">&Aacute;rea</td>
						<td>
							<div id="lista_areas">
							<select>
								<option disabled selected>No tiene &aacute;reas habilitadas.</option>								
							</select>
							</div>
							<input name="idsesion" id="idsesion" value="<?php echo $_SESSION['idsess'.NBOLSA];?>" type="hidden"/>
						</td>
					</tr>	
					<tr>
						<td class="td2">Grupo</td>
						<td>
							<div id="lista_grupos">
							<select>
								<option disabled selected>No tiene grupos habilitados.</option>								
							</select>
							</div>
						</td>
					</tr>
					<tr><td class="td2">Mensaje</td><td><textarea id="mensaje" wrap="VIRTUAL" rows="10" cols="20" onkeyup="contador();"></textarea></td></tr>
					<tr><td class="td2"></td><td><span id="contador" style="margin:4px 0px;padding:3px;border:1px solid#ededed; background-color:#fafafa;color#888">0 contador de caracteres</span>  <br></td></tr>
					<tr><td class="td2" colspan=2><input id="enviar" type="submit" onclick="enviar_mensajes();"><input type="reset" onclick="resetear('enviar');"></td></tr>
					</table>
				</div>	
				</p>
				<br><br>
				<h2>Envio Mensajes Personalizados 1 a 1
					<small style="float:right" onclick="ver_lista_areaunoauno();ver_lista_grupos_unoauno();return false;">
						<a href="#!">Actualizar</a>
					</small>
				</h2>
				<div style="text-align:right">(<a href="#" onclick="cambiar_envio();">Ver</a>)</div>
				<div id="e_unoauno" style="display:none;">
				<p>
					<table>
						<tr><td class="td2">&Aacute;rea</td>
						<td>
							<div id="lista_areasunoauno">
							<select>
								<option disabled selected>No tiene &aacute;reas habilitadas.</option>								
							</select>
							</div>
							<input name="idsesion" id="idsesion" value="<?php echo $_SESSION['idsess'.NBOLSA];?>" type="hidden"/>
						</td>
					</tr>	
					<tr>
						<td class="td2">Grupo</td>
						<td>
							<div id="lista_grupos_unoauno">
							<select>
								<option disabled selected>No tiene grupos habilitados.</option>								
							</select>
							</div>
						</td>
						</tr>
					<tr><td class="td2" colspan=2><input id="btn_enviar_unoauno" type="submit" onclick="enviar_mensajes_unoauno();"></td></tr>
					</table>
				</div>	
				</p>	
				<h2>Resultado de Envio</h2>
				<p>
				<div id="res_lista">
				</div>
				</p>
		
			</div>
			<div id="clear"></div>
		</div>
		<div id="footer">
			<?php include ('foot.php');?>
		</div>
	</div>
</body>
</html>