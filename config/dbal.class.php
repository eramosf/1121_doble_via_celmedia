<?
if (!$DBALINCLUDE) {
	$DBALINCLUDE = 1;
/**
 * Definicion de los textos de error
 */
 define ('DB_CONNECT_ERROR', "Error while connecting to the database");
 define ('DRIVER_NOT_CAPABLE', "The specified driver is not capable of : %s");
 define ('OBJECT_FETCHING', "fetching objects from a result.");
 define ('ARRAY_FETCHING', "fetching array's from a result.");
 define ('ROW_FETCHING', "fetching rows as an indexed array from a result.");
 define ('LAST_INSERT_FETCHING', "fetching the last inserted record.");
 define ('CUSTOM_HANDLER_USED', "You are using a custom error handler so DAL doesn't store the errors anymore");
 define ('ROW_FETCH_FAILED', "No row to fetch as ROW");
 define ('ARRAY_FETCH_FAILED', "No row to fetch as ARRAY");
 define ('OBJECT_FETCH_FAILED', "No row to fetch as OBJECT");

# listado de drivers disponibles
$drivers = array ("mysql");
include "mysql.drv.php";

class dbal {

	var $dbDriver; 		# nombre del driver en uso
	var $selectedDB;	# nombre de la base de datos seleccionada
	var $username;		# username de conexión a la base de datos
	var $password;		# password de conexión a la base de datos
	var $connString;	# string de conexión a la base de datos (hostname)
	var $connected; 	# referencia al objeto driver de la base de datos
	var $lastError;		# texto del ultimo error
	var $lastRowCount;	# ultimo conteo de filas afectadas
	var $errorHandler;	# nombre de la funcion de manejo de errores


/**************************************************************************
Feature Name:	dbal()
Description:	Constructor de la clase
		Llama a la funcion de inicialización
Input:		vacio
Output:		vacio
***************************************************************************
*/

	function dbal()
	{
		$this->initialize();
	}

	
/**************************************************************************
Feature Name:	initialize()
Description:	Inicializa las variables internas de la clase
Input:		Vacio
Output:		vacio
***************************************************************************
*/

	function initialize()
	{
		$this->selectedDB = "none";
		$this->username = "";
		$this->password = "";
		$this->connectionString = "";
		$this->driver = "none";
		$this->connected = 0;
		$this->lastError = "none";
		$this->lastRowCount = 0;
		$this->errorHandler = "\$this->onError";
	}

	
/**************************************************************************
Feature Name:	destroy()
Description:	destructor de la clase.
		debe ser llamado explicitamente al terminar de usar 
		la clase.
Input:		vacio
Output:		vacio
***************************************************************************
*/
	function destroy()
	{
		$this->onDestroy();
	}

/**************************************************************************
Feature Name: 	onDestroy()
Description:	Esta funcion es llamada cuando la clase se destruye
		Cierra la conexion con la base de datos y llama cualquier
		operacion de cierre que requiera la clase driver
Input:		vacio
Output:		vacio
***************************************************************************
*/
	function onDestroy()
	{
		if ($this->connected != 0) {
			$this->database->dbClose();
		}
		$this->database->onDestroy();
	}

/**************************************************************************
Feature Name:	setAll()
Description:	configura todos los parametros e intenta establecer una conexion
Input:		driver,db username, password,hostname,db name
Output:		TRUE si hubo exito en la conexion, FALSE en caso contrario
***************************************************************************
*/
	function setAll($driver,$username,$password,$hostname,$dbname)
	{
    $this->selectDriver($driver);
  	$this->setUsername($username);
  	$this->setPassword("$password");
	  $this->setConnectionString("$hostname");
	  return $this->selectDatabase("$dbname");
	}

/**************************************************************************
Feature Name:	getAll()
Description:	despliega los parametros actuales del objeto
Input:		none
Output:		TRUE 
***************************************************************************
*/
	function getAll()
	{
		$salida= "Driver:".$this->driver.
				"<br>username:". $this->username.
				"<br>password:".$this->password.
				"<br>hostname:".$this->connectionString.
				"<br>database:".$this->selectedDB;
				"<br>";
		return $salida;								
	}


/**************************************************************************
Feature Name:	getUserName()
Description:	Retorna el nombre de la base de datos actualmente seleccionada
Input:		vacio
Output:		(string) nombre de la base de datos
***************************************************************************
*/	
	function getUserName()
	{
		return $this->username;
	}

/**************************************************************************
Feature Name:	getPassword()
Description:	Retorna el nombre de la base de datos actualmente seleccionada
Input:		vacio
Output:		(string) nombre de la base de datos
***************************************************************************
*/	
	function getPassword()
	{
		return $this->password;
	}

/**************************************************************************
Feature Name:	getHostName()
Description:	Retorna el nombre de la base de datos actualmente seleccionada
Input:		vacio
Output:		(string) nombre de la base de datos
***************************************************************************
*/	
	function getHostName()
	{
		return $this->connectionString;
	}


/**************************************************************************
Feature Name:	getDriverName()
Description:	Retorna el nombre del driver seleccionado
Input:		vacio
Output:		vacio
***************************************************************************
*/
	function getDriverName()
	{
		return $this->database->driverName;
	}


/**************************************************************************
Feature Name:	selectDriver()
Description:	Selecciona el driver a utilizar para la conexion
		Los drivers son una clase que hace de interfase con la
		base de datos que manejan. La clase debe llamarse [nombre del driver]Driver
		ej: mysqlDriver y la definicion de la clase debe incluirse en este archivo
		con una directiva include(). Ademas el nombre del driver debe incluirse en 
		la variable global $drivers, al principio de este archivo
Input:		(string) Nombre del driver
Output:		vacio
***************************************************************************
*/
	function selectDriver($drivername)
	{
	$dbDriver = $drivername . "Driver";
	if ($this->driver != "none") {
		$this->database->onDestroy();
		unset($this->database);
	}
		eval("\$this->database = new $dbDriver;");
		$this->driver = $drivername;
	}

/**************************************************************************
Feature Name:	driverExist()
Description:	Verifica que $drivername sea un driver valido, revisando 
		el arreglo de drivers
Input:		(string) nombre del driver
Output:		(int) 1 si el driver existe, 0 si el driver no existe
***************************************************************************
*/
	function driverExist($drivername)
	{
		$drivers = enumDrivers();

		while(list($index, $name) = each($drivers)) {
			if ($drivername == $name) {
				return 1;
			}
		}

		return 0;
	}

/**************************************************************************
Feature Name:	enumDrivers()
Description:	Retorna el arreglo de drivers
Input:		vacio	
Output:		(array) arreglo de drivers 
***************************************************************************
*/
	function enumDrivers()
	{
		global $drivers;
		return $drivers;
	}



/**************************************************************************
Feature Name:	setUsername 
Description:	Modifica el valor del feature username de la clase, que 
		corresponde al nombre de usuario para ejecutar la conexión
		a la base de datos
Input:		(string) username
Output:		vacio
***************************************************************************
*/
	function setUsername($username)
	{
		$this->username = $username;
	}



/**************************************************************************
Feature Name:	setPassword()
Description:	Modifica el valor del feature password de la clase que
		corresponde al password de conexión a la base de datos
Input:		(string) password
Output:		vacio
***************************************************************************
*/
	function setPassword($password)
	{
		$this->password = $password;
	}

	
/**************************************************************************
Feature Name:	setConnectionString()
Description:	Modifica el valor del feature connectionString que corresponde
		al host de la base de datos o a cualquier string necesario
		para hacer la conexión con la base de datos.
		ej: mysql.dominio.cl:8090 
Input:		(string) string de conexión
Output:		vacio
***************************************************************************
*/
	function setConnectionString($connString)
	{
		$this->connectionString = $connString;
	}




/**************************************************************************
Feature Name:	selectDatabase()
Description:	selecciona la base de datos sobre la que se va a trabajar
		Si no se ha abierto una conexión con el servidor, en este
		feature se abre la conexión. 
Input:		(string) nombre de la base de datos
Output:		(bool) Verdadero si fue posible seleccionar la base de datos
		falso en otro caso
***************************************************************************
*/
	function selectDatabase($database)
	{
		global $DB_CONNECT_ERROR;

		if ($this->connected == 0) {
			$this->connected = $this->database->dbConnect($this->connectionString,$this->username,$this->password);
			
			if ($this->connected == 0) {
				$this->errorHandler(DB_CONNECT_ERROR);
				return false;
			}
		}

		if (!$this->database->dbSelect($database)) {
			$this->errorHandler($this->database->dbGetError());
			return false;
		}
		else {
			$this->selectedDB=$database;
			return true;
		}
	}




/**************************************************************************
Feature Name:	getSelectedDB()
Description:	Retorna el nombre de la base de datos actualmente seleccionada
Input:		vacio
Output:		(string) nombre de la base de datos
***************************************************************************
*/	
	function getSelectedDB()
	{
		return $this->selectedDB;
	}



/**************************************************************************
Feature Name:	executeQuery()
Description:	ejecuta un query a la base de datos. Setea ademas el valor 
		del feature lastRowCount , llamando a la funcion correspondiente 
		segun si el query es un select u otro tipo
Input:		(string) query
Output:		(bool) resultado de la ejecucion del query
***************************************************************************
*/
	function executeQuery($query)
	{
		if (!$this->database->dbExecuteQuery($query)) {
			$this->errorHandler($this->database->dbGetError());
			return false;
		}

		if (eregi("^select", $query)) {
			$this->lastRowCount = $this->database->dbRowCount();
		}
		else {
			$this->lastRowCount = $this->database->dbAffectedRows();
		}
		
		return true;
	}

/**************************************************************************
Feature Name:	executeQueryLimit()
Description:	ejecuta un query a la base de datos con limites en el resultado. 
							Setea ademas el valor del feature lastRowCount , 
							llamando a la funcion correspondiente 
							segun si el query es un select u otro tipo
Input:		(string) query, limite inferior, cantidad
Output:		(bool) resultado de la ejecucion del query
***************************************************************************
*/
	function executeQueryLimit($query,$base,$offset)
	{
		if (!$this->database->dbExecuteQueryLimit($query,$base,$offset)) {
			$this->errorHandler($this->database->dbGetError());
			return false;
		}

		if (eregi("^select", $query)) {
			$this->lastRowCount = $this->database->dbRowCount();
		}
		else {
			$this->lastRowCount = $this->database->dbAffectedRows();
		}
		
		return true;
	}
	

/**************************************************************************
Feature Name:	getRowCount()
Description:	Retorna el valor del ultimo conteo de filas afectadas por el
		ultimo query ejecutado
Input:		vacio
Output:		(int) conteo de filas afectadas
***************************************************************************
*/
	function getRowCount()
	{
		return $this->lastRowCount;
	}





/**************************************************************************
Feature Name:	fetchObject()
Description:	retorna una nueva fila del conjunto resultante de un query
		en forma de un objeto en que cada columna de la fila resultante
		aparece como un feature del objeto. Si ya no quedan mas filas
		que retornar, devuelve Null. Si la base de datos, y por lo
		tanto el driver asociado no tienen la capacidad de devolver 
		filas como objetos, devuelve NULL, y setea el error correspondiente
Input:		vacio
Output:		Null si no quedan mas filas, o un objeto con la ultima fila
***************************************************************************
*/
	function fetchObject()
	{
		if (!$this->verifyCapability("return_object")) {
			$this->errorHandler(sprintf(DRIVER_NOT_CAPABLE, OBJECT_FETCHING));
			return NULL;
		}

		$object = $this->database->dbFetchObject(); 

		if ($object == NULL) {
			$this->errorHandler(OBJECT_FETCH_FAILED);
			return NULL;
		}
		return $object;
	}





/**************************************************************************
Feature Name:	fetchArray()
Description:	retorna una nueva fila del conjunto resultante de un query
		en forma de un arreglo asociativo en que cada columna de la fila
		resultante aparece como llave para un elemento del arreglo. Si ya no 
		quedan mas filas que retornar, devuelve Null. Si la base de datos,
		y por lo tanto el driver asociado no tienen la capacidad de devolver 
		filas como arreglos asociativos, devuelve NULL, y setea el error correspondiente
Input:		vacio
Output:		Null si no quedan mas filas, o un arreglo asociativo con la siguiente fila
***************************************************************************
*/
	function fetchArray()
	{
		if (!$this->verifyCapability("return_array")) {
			$this->errorHandler(sprintf(DRIVER_NOT_CAPABLE, ARRAY_FETCHING));
			return NULL;
		}

		$array = $this->database->dbFetchArray();

		if ($array == NULL){
			$this->errorHandler(ARRAY_FETCH_FAILED);
			return NULL;
		}

		return $array;
	}




/**************************************************************************
Feature Name:	fetchRow()
Description:	retorna una nueva fila del conjunto resultante de un query
		en forma de un arreglo secuencial en que cada columna de la fila
		resultante aparece como un elemento del arreglo. Si ya no 
		quedan mas filas que retornar, devuelve Null. Si la base de datos,
		y por lo tanto el driver asociado no tienen la capacidad de devolver 
		filas como arreglos, devuelve NULL, y setea el error correspondiente
Input:		vacio
Output:		Null si no quedan mas filas, o un arreglo con la siguiente fila
***************************************************************************
*/
	function fetchRow()
	{
		if (!$this->verifyCapability("return_row")) {
			$this->errorHandler(sprintf(DRIVER_NOT_CAPABLE, ROW_FETCHING));
			return NULL;
		}

		$row = $this->database->dbFetchRow();

		if ($row == NULL) {
			$this->errorHandler(ROW_FETCH_FAILED);
			return NULL;
		}
		return $row;
	}


/**************************************************************************
Feature Name:	fetchLastInsertId() 
Description:	Regresa el id generado para una columna AutoIncrement por el 
		ultimo insert ejecutado
Input:		vacio
Output:		(int) Ultimo id generado
***************************************************************************
*/
	function fetchLastInsertId()
	{
		if (!$this->verifyCapability("return_last_insert_id")) {
			$this->errorHandler(sprintf(DRIVER_NOT_CAPABLE, LAST_INSERT_FETCHING));
			return NULL;
		}
		
		return $this->database->dbFetchLastID();
	}


/**************************************************************************
Feature Name: 	safeValue()
Description:	recibe una variable y corrige valores que pudieran representar
		un problema de seguridad como ', ", etc.
Input:		(string) variable
Output:		variable corregida
***************************************************************************
*/
	function safeValue($valor)
	{
		return $this->database->dbSafeValue($valor);
	}


/**************************************************************************
Feature Name:	getLastError() 
Description:	Retorna el texto del ultimo error registrado.	
Input:		vacio
Output:		(string) texto del ultimo de error
***************************************************************************
*/
	function getLastError()
	{
		return $this->lastError;
	}



/**************************************************************************
Feature Name:	onError()		
Description:	funcion por defecto de manejo de errores. Es posible cambiar
		esta funcion por otra funcion que maneje los errores. La
		clase llama a la funcion seleccionada pasandole como argumento
		el texto del error
Input:		(string) texto del error		
Output:		vacio
***************************************************************************
*/
	function onError($errorMessage)
	{
		$this->lastError = $errorMessage;
	}





/**************************************************************************
Feature Name:	errorHandler()
Description:	Este feature llama a la funcion de manejo de errores asignada
		pasandole como argumento el texto del error
Input:		(string) texto del error
Output:		vacio
***************************************************************************
*/
	function errorHandler($errorMessage)
	{
		$this->lastError = $errorMessage;
		eval("$this->errorHandler(\$errorMessage);");
	}




/**************************************************************************
Feature Name: 	setErrorHandler()
Description:	Modifica el nombre de la funcion que maneja errores
Input:		(string) nombre de la funcion
Output:		vacio
***************************************************************************
*/
	function setErrorHandler($errorHandler)
	{
	  $this->lastError = CUSTOM_HANDLER_USED;
		$this->errorHandler = $errorHandler;
	}




/**************************************************************************
Feature Name:	verifyCapability()
Description:	varifica si el driver es capaz de ejecutar la funcion que
		se da como entrada
Input:		(string) nombre de la funcion a verificar
Output:		(bool) verdadero si el driver es capaz de la funcionalidad
		solicitada, falso en caso contrario
***************************************************************************
*/
	function verifyCapability($capability)
	{
		$driverCapabilities = $this->database->capabilities();

		while (list($key, $driverCapability) = each($driverCapabilities)) {
			if ($driverCapability == $capability) {
				return true;
			}
		}

		return false;
	}

}
}
?>
