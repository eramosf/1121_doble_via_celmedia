<?
class mysqlDriver	{

#############################################################################
# Driver de conexión a bases de datos MySQL para dbal
#
# 
#############################################################################
	
	var $connection;
	var $resultID;
	var $dbActive;
	var $driverName;
	var $driverCapabilities;

/** 
 * mysqlDriver()	: Constructor de la clase. Llama a la función de inicialización
 * 
 * entrada		: nada
 * retorna      	: nada
 * 
 */
	function mysqlDriver()
	{
		$this->initialize();
	}

/**
 * Inicialización de variables privadas de la clase
 *
 * entrada	: nada
 * retorna	: nada
 * 
 */
	function initialize()
	{
		$this->connection = 0;
		$this->resultID = 0;
		$this->driverName = "mysql";
		$this->driverCapabilities = array ("return_last_insert_id","return_object","return_array","return_row");
	}

/**
 * Acciones a tomar al destruir la clase
 *
 * entrada	: nada
 * retorna	: nada
 * 
 */
	function onDestroy()
	{
	}

/**
 * capabilities()	: Retorna la lista de capacidades del driver
 *
 * entrada	: nada
 * salida	: (array) arreglo de capacidades del driver
 * 
 */


	function capabilities()
	{
		return $this->driverCapabilities;
	}
	
/**
 * dbConnect()	: Abre la conexión a la base de datos
 *
 * entrada	: (string) String de conexión (hostname), (string) username, (string) password
 * salida	: (resource) Handler de la conexión 
 * 
 */
	function dbConnect($connString,$username,$password)
	{

		$this->connection = @mysql_connect($connString, $username , $password);
		return $this->connection;
	}

/**
 * dbSelect()	: Selecciona la base de datos 
 *
 * entrada	: (string) Nombre de la base de datos
 * salida	: (bool) Resultado de la conexión 
 * 
 */
 
	function dbSelect($database)
	{
		$this->dbActive = $database;
		return @mysql_select_db($database, $this->connection);
	}

/**
 * dbClose()	: Cierra la conexión con la base de datos
 *
 * entrada	: nada
 * salida	: (bool) Resultado de la operación
 * 
 */
	function dbClose()
	{
		return @mysql_close($this->connection);
	}

/**
 * dbGetError()	: Obtiene el ultimo error para la conexión actual 
 *
 * entrada	: nada
 * salida	: (string) Texto del error
 */
	function dbGetError()
	{
		if ($this->connection != 0) {
			return @mysql_error($this->connection);
		}
		else {
			return @mysql_error();
		}
	}

/**
 * dbExecuteQuery()	: Ejecuta un query para la conexión
 *
 * entrada		: (string) El query a ejecutar
 * salida		: (bool) resultado del intento de ejecutar el query
 */
	function dbExecuteQuery($query)
	{
		$this->resultID = @mysql_query($query, $this->connection);
		if ($this->resultID != 0) {
			return true;
		}
		else {
			return false;
		}
	}
/**
 * dbExecuteQueryLimit()	: Ejecuta un query para la conexión devolviendo un subconjunto 
 *													de resultados
 *
 * entrada		: (string) El query a ejecutar, (int)base(opcional), (int)offset
 * salida		: (bool) resultado del intento de ejecutar el query
 */
	function dbExecuteQueryLimit()
	{
		$numargs  = func_num_args();
    $arg_list = func_get_args();
    $query  = $arg_list[0];
    $base   = $arg_list[1];
    
    $query = $query." limit $base";
    if ($numargs>2) 
    {
      $offset = $arg_list[2];
      $query.=",$offset ";
    }	
    #print "haciendo:$query<br>";
		$this->resultID = @mysql_query($query, $this->connection);
		if ($this->resultID != 0) {
			return true;
		}
		else {
			return false;
		}
	}

/**
 * dbAffectedRows()	: retorna el numero de filas afectadas por el ultimo query
 *
 * entrada		: nada
 * salida		: (int) Numero de filas afectadas por el query
 */
	function dbAffectedRows()
	{
		return @mysql_affected_rows($this->connection);
	}

/**
 * dbRowCount()		: Retorna el numero de filas resultantes de un select
 *
 * entrada		: nada
 * salida		: int
 */
	function dbRowCount()
	{
		return @mysql_num_rows($this->resultID);
	}

/**
 * dbFetchObject()	: Retorna una fila del resultado en forma de un objeto, cuyos features
 *			  son las columnas de la base de datos
 *
 * entrada		: nada
 * salida		: object
 */


	function dbFetchObject()
	{
		return @mysql_fetch_object($this->resultID);
	}

/**
 * dbFetchArray()	: Retorna una fila del resultado en forma de un arreglo asociativo, 
 *			  las llaves del arreglo son las columnas de la base de datos
 *
 * entrada		: nada
 * salida		: array
 */

	function dbFetchArray()
	{
		return @mysql_fetch_array($this->resultID);
	}

/**
 * dbFetchRow()		: Retorna una fila del resultado en forma de un arreglo indexado 
 *
 * entrada		: nada
 * salida		: array
 */

	function dbFetchRow()
	{
		return @mysql_fetch_row($this->resultID);
	}
	
/**
 * dbFetchLastID()	: Retorna el ID del ultimo insert, si es que la tabla
 *			  afectada tiene una columna indice con autoincrement
 *
 * entrada		: nada 
 * salida  		: int
 */

	function dbFetchLastID() 
	{ 
		return @mysql_insert_id($this->connection); 
	} 
/**
 * dbSafeValue($valor)	: Escapa ', ; y otros caracteres que pudieran
 *			  representar un problema de seguridad al emplearlos
 			  en una variable dentro de un query
 *
 * entrada		: nada 
 * salida  		: int
 */

	function dbSafeValue($valor) 
	{ 
		return addSlashes($valor);
	} 

}
?>
