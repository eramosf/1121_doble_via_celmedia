<?php
	require ('login.head.php');
	require ("config/dbal.class.php");
	$db = new dbal;
	$conectado = $db->setAll($kDbalDriver, $kDatabaseUsername, $kDatabasePassword, $kDatabaseHostname, $kDatabaseName);
	
	$db2 = new dbal;
	$conectado2 = $db2->setAll($kDbalDriver, $kDatabaseUsername, $kDatabasePassword, $kDatabaseHostname, $kDatabaseName);

?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<meta name="author" content="celmedia" />
		<title>Celmedia :: Envios Masivos</title>
		<link rel="stylesheet" type="text/css" href="css/style.css" media="screen" />
		<link rel="shortcut icon" href="favicon.ico" />
		<script type="text/javascript" src="js/prototype.js"></script>
		<script type="text/javascript" src="js/textsizer.js"></script>
		<script type="text/javascript" src="js/rel.js"></script>
		<script type="text/javascript" src="js/aplicaciones.js"></script>

	</head>

<body>
	<div id="wrap">
		<div id="top">
			<h2><a href="#" title="Back to main page">Celmedia</a></h2>
			<div id="menu">
				<?php include ('menu.php'); ?>
			</div>
			<h3>Tel : 57 1 6421002</h3>
		</div>
	</div>
	<div id="wrap_b">
		<div id="content">


			<?php

			$query="select * from ".IDSERVICIO."_areas where id = '1' limit 1";
			$db->executeQuery($query);
			$sms = $db->fetchArray();


			$query2="select sum(total) as 'count' from ".IDSERVICIO."_status where fecha_hora like '".date('Y-m')."%' limit 1";
			$db2->executeQuery($query2);
			$sms2 = $db2->fetchArray();

			?>

			<div style="border:solid 1px #2b542c;border-radius:5px;float:right;width:300px;">

				<div style="padding: 5px;background-color: rgba(101, 228, 25, 0.15);">
					Resumen
				</div>
				<div>
					<div style="padding: 5px;margin: 5px;">Usuario: <?= $reg_nombre ?> (<a href="logout.php">Salir</a>)</div>
				</div>
				<hr>
				<div>
					<div style="padding: 5px;margin: 5px;">Haz enviado: <?= $sms['cantidad_enviado'] ?> sms.</div>
				</div>
				<hr>
				<div>
					<div style="padding: 5px;margin: 5px;">SMS Restantes: <?= (int)$sms['cantidad_total']-(int)$sms['cantidad_enviado'] ?> sms.</div>
				</div>
				<hr>
				<div>
					<div style="padding: 5px;margin: 5px;">Bolsa Contrato: <?= (int)$sms['cantidad_total'] ?> sms.</div>
				</div>
				<hr>
				<div>
					<div style="padding: 5px;margin: 5px;">Contador de mensajes para este mes: <?= $sms2['count'] ?> sms.</div>
				</div>
			</div>



			<div id="leftEstad">
				<h2>Estadisticas de Envio</h2>
				<p>
					<table class="tabla2">
						<tr>
							<th>Usuario</th>
							<th>Asunto / Area</th>
							<th>Grupo</th>
							<th>Fecha</th>
							<th>Sms Enviado</th>
							<th>Total Comcel</th>
							<th>Total Tigo</th>
							<th>Total Movistar</th>
							<th>Total Avantel</th>
							<th>Concatenado</th>
							<th>Comcel</th>
							<th>Tigo</th>
							<th>Movistar</th>
							<th>Avantel</th>
							<th>Comcel Concatenado</th>
							<th>Tigo Concatenado</th>
							<th>Movistar Concatenado</th>
							<th>Avantel Concatenado</th>
							<th>BlackList</th>
							<th>Errores</th>
							<th>Enviado Por</th>
							<th>Informe</th>
						</tr>
					<?php
						$query = "select u.nombre,s.asunto,s.mensaje,g.nombre,date(s.fecha_hora),s.total,s.comcel,s.tigo,s.movistar,s.avantel,g.id,s.concatenados,s.id_usr_envio,s.comcel_conca,s.tigo_conca,s.movistar_conca,s.avantel_conca,s.comcel_total,s.tigo_total,s.movistar_total,s.avantel_total, s.id, s.blacklist_total from ".IDSERVICIO."_status s,".IDSERVICIO."_grupos g,".IDSERVICIO."_usuarios u where s.id_grupo = g.id and g.id_usuario = u.id order by s.fecha_hora desc";
						$db->executeQuery($query);
						while ($stInfo = $db->fetchArray())
						{
							$excel = "<a href=\"informe.php?id=$stInfo[21]\">Ver Informe</a>";
							/*if ($stInfo[4] > '2014-04-07' and ($stInfo[1]=='1' or $stInfo[1]=='2' ))
							{
								$query2="select nombre from ".IDSERVICIO."_areas where id=".$stInfo[1];
								$db2->executeQuery($query2);
								$stInfo2 = $db2->fetchArray();	
							 	$stInfo[1]=$stInfo2[0];
							}*/
							($stInfo[11] != "") ? $contate =$stInfo[11]  : $contate = "N/A";
							
								$query2="select count(*) from ".IDSERVICIO."_numeros_error where id_grupo=".$stInfo[10];
								$db2->executeQuery($query2);
								$stInfo2 = $db2->fetchArray();
								$errores=$stInfo2[0];					
								//$errores = $stInfo[5] - ($stInfo[17]+$stInfo[18]+$stInfo[19]+$stInfo[20]);//errores anterior
													
							if ($stInfo[12] == "0")
							{
								 $usr_envio="Sin Informaci&oacute;n";
							}else
							{
								$query2="select nombre from ".IDSERVICIO."_usuarios where id=".$stInfo[12];
								$db2->executeQuery($query2);
								$stInfo2 = $db2->fetchArray();
								$usr_envio=$stInfo2[0];
							}
							
							
							echo "<tr><td>".$stInfo[0]."</td><td>" . wordwrap($stInfo[1], 20, "<br>", 1) . "</td><td>" . wordwrap($stInfo[3], 20, "<br>", 1) . "</td><td>".$stInfo[4]."</td><td>".$stInfo[5]."</td><td>".$stInfo[17]."</td><td>".$stInfo[18]."</td><td>".$stInfo[19]."</td><td>".$stInfo[20]."</td><td>".$contate."</td><td>".$stInfo[6]."</td><td>".$stInfo[7]."</td><td>".$stInfo[8]."</td><td>".$stInfo[9]."</td><td>".$stInfo[13]."</td><td>".$stInfo[14]."</td><td>".$stInfo[15]."</td><td>".$stInfo[16]."</td><td>".$stInfo[22]."</td><td>".$errores."</td><td>".$usr_envio."</td><td>$excel</td></tr>";
						  }	
					?>
					</table>
				</p>
			</div>
			<div id="clear"></div>
		</div>
		<div id="footer">
			<?php include ('foot.php'); ?>
		</div>
	</div>
</body>
</html>