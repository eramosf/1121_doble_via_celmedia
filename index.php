<?php
if (isset($_GET["ref"]))
{
	$ref = htmlentities($_GET["ref"]);
}
if (isset($_GET["err"]))
{
	$err = htmlentities($_GET["err"]);
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Bienvenido al Sistema de Mensajeria</title>
<link href="css/index.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="login">
	<h1>Login</h1>
	<div class="lgo">
	<?php
	if (isset($err))
	{
	?>
		<div class="error">Usuario o Contrase&ntilde;a incorrecta</div>
	<?php
	}
	?>
	
		<form method="post" action="login_paso.php" name="login">
			<label>Username:</label><input class="in" type="text" name="txt_login" maxlength="20"  size="20" autocomplete="off"/>
			<label>Password:</label><input class="in" type="password" name="pwd_clave" maxlength="20" size="20" autocomplete="off"/>
			<?php
			if (isset($ref))
			{
			?>
			<input type="hidden" name="ref" value="<?php echo $ref; ?>">
			<?
			}
			?>
			<br><br>
			<input type="submit" class="bot" name="boton" value="Login &raquo;" />	
	</form>
	</div>
	
</div>
</body>
</html>