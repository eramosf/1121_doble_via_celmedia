<?php
	include ("login.head.php");
	require ("config/dbal.class.php");
	require ('class.phpmailer.php');
	require ("/var/www/html/celmedia_colombia/Global/portabilidad.php");
	set_time_limit(0);
	ini_set("max_input_time", "-1");
	ini_set("memory_limit", "2048M");
	$db = new dbal;
	$conectado = $db->setAll($kDbalDriver, $kDatabaseUsername, $kDatabasePassword, $kDatabaseHostname, $kDatabaseName);	
	$db2 = new dbal;
	$conectado2 = $db2->setAll($kDbalDriver, $kDatabaseUsername, $kDatabasePassword, $kDatabaseHostname, $kDatabaseName);
	$blacklist_total=0;
	$largo_sms_concatenado=153;
	$appId = $_GET['idaplicacion']; 
	$id_sesion = $_GET['idsesion'];
	if ($id_sesion != $_SESSION['idsess'.NBOLSA])
	{	
		exit();
	}
	function calculaCuotaPermitida($db)
	{
		$query = "select cantidad_total,cantidad_enviado,limite_alerta,estado_alerta,id from ".IDSERVICIO."_areas  where estado = 'activo'";
		$db->executeQuery($query);
		$total=$limite_alerta=$estado_alerta=$id=0;
		while ($stInfo = $db->fetchRow())
		{
				$total=$stInfo[0]-$stInfo[1];
				$cantidad_enviado=$stInfo[1];
				$limite_alerta=$stInfo[2];
				$estado_alerta=$stInfo[3];
				$id=$stInfo[4];
		}
		if(($cantidad_enviado > $limite_alerta) and ($estado_alerta=='N'))
		{	
			Alerta_Limite();
			$query="update ".IDSERVICIO."_areas set estado_alerta='S' where id=".$id;
			$db->executeQuery($query);
		}
		
		return $total;
	}
	function calculaTotalMensajes($id_sel,$mensaje,$db,$largo_sms_concatenado)
	{
		$cont=0;
		$query = "select nombre,variable2,variable3 from ".IDSERVICIO."_numeros  where id_grupo = $id_sel and estado = 'activo' order by id asc";
		$db->executeQuery($query);
		while ($stInfo = $db->fetchRow())
		{
				if ($stInfo[0]!= null)
				{
					$mensaje_tmp = str_replace("_NAME_", $stInfo[0], $mensaje);
					$mensaje_tmp = str_replace("_NAME2_", $stInfo[1], $mensaje_tmp);
					$mensaje_tmp = str_replace("_NAME3_", $stInfo[2], $mensaje_tmp);
				}
				else
				{
					$mensaje_tmp = $mensaje;
				}
				$n_sms_conca=$resto_conca=0;
				$n_sms_conca=floor(strlen($mensaje_tmp)/$largo_sms_concatenado);
				$resto_conca=strlen($mensaje_tmp)%$largo_sms_concatenado;
				if ($resto_conca > 0)
				{
					$n_sms_conca++;
				}
				if(strlen($mensaje_tmp)<=160)
				{
					$cont++;
				}
				else
				{
					$cont=$cont+$n_sms_conca;
				}
		}
		return $cont;
	}
	function calculaTotalMensajesUnoaUno($id_sel,$db,$largo_sms_concatenado)
	{
		$cont=0;
		$query = "select mensaje from ".IDSERVICIO."_numeros_unoauno  where id_grupo = $id_sel and estado = 'activo' order by id asc";
		$db->executeQuery($query);
		while ($stInfo = $db->fetchRow())
		{
				$mensaje_tmp = $stInfo[0];
				$n_sms_conca=$resto_conca=0;
				$n_sms_conca=floor(strlen($mensaje_tmp)/$largo_sms_concatenado);
				$resto_conca=strlen($mensaje_tmp)%$largo_sms_concatenado;
				if ($resto_conca > 0)
				{
					$n_sms_conca++;
				}
				if ($mensaje_tmp != '')
				{
					if(strlen($mensaje_tmp)<=160)
					{
						$cont++;
					}
					else
					{
						$cont=$cont+$n_sms_conca;
					} 
				}
		}
		return $cont;
	}
	
	function generaMail($id_area,$usuario,$fecha,$mensaje,$totalmensaje,$id_sel)
	{
		global $db;
		$query="select nombre from ".IDSERVICIO."_grupos where id=$id_sel";
		$db->executeQuery($query);
		$stInfo = $db->fetchRow();
		$ngrupo=$stInfo[0];
		
		$query2="select nombre from ".IDSERVICIO."_areas where id=".$id_area;
		$db->executeQuery($query2);
		$stInfo = $db->fetchArray();	
		$nombre_area=$stInfo[0];

		$mensaje = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Untitled Document</title>
		<style type="text/css">
		<!--
		
		.Estilo1, p{
			background-color:#FFFFFF;
			font-family: Arial, Helvetica, sans-serif;
			font-size: 14px;
			color:#000000
		}
		.Estilo2 {
			font-weight:bold;
		}
		.Estilo3 {
			background-color:#FFFFFF;
			font-family: Arial, Helvetica, sans-serif;
			font-size: 10px;
			color:#000000
		}
		-->
		</style>
		</head>
		</head>
		<body  >
		<p>Estimados,<br><br><br>El usuario : <span class="Estilo2">'.$usuario.'</span> Realizo el siguiente envio. <br></p>
		<table width="450" class="Estilo1" border="0" cellpadding="4" cellspacing="0">
		  <tr >  
		  	<tr><th width="120" class="Estilo2" scope="col" align="left">Area o Asunto</th><td align="left">'.$nombre_area.'</td></tr>    
		    <tr><th width="120" class="Estilo2" scope="col" align="left">Grupo </th><td align="left">'.$ngrupo.'</td></tr>
		    <tr><th width="115" class="Estilo2" scope="col" align="left">Fecha Envio </th> <td align="left">'.$fecha.'</td></tr>
		    <tr><th width="115" class="Estilo2" scope="col" align="left">Mensaje</th><td align="left">'.$mensaje.'</td></tr>
		    <tr><th width="100" class="Estilo2" scope="col" align="left">Total Mensajes </th><td align="left">'.$totalmensaje.'</td></tr>		  
		  </table>
		  	  <p>
		  <span class="Estilo2">Validar a la brevedad de acuerdo al procedimiento establecido.<br>COMUNIQUESE INMEDIATAMENTE AL NO ESTAR DE ACUERDO CON LA PRESENTE COMUNICACION</span><br><br>
		  	<span class="Estilo2">Pdt:El procedimiento establecido en este momento es comunicarse directamente con el Usuario que ejecuto la accion, para su cambio, actualizacion y/o modificacion. Si no se logra algun cambio o dicho usuario presenta algun<br>
		  	 inconveniente para realizar cualquiera de los anteriores items mencionados; comunicarse inmediatamente a la linea de Soporte de Celmedia.</span>
		  </p>
		  </html></body>';
		
			$m_host = "mail.misp.cl";
			$m_user = "procesos@misp.cl";
			$m_pass = "proce2532";
			$m_mail = "procesos@misp.cl";
			$m_from = "Celmedia";			
			$m_subject = "[NOTIFICACION] Envio ".NBOLSA;			
			$mail = new PHPMailer();
			$mail->IsSMTP(); // enviar via SMTP
			$mail->Host = $m_host; // Servidor SMTP
			$mail->SMTPAuth = true; // Activar Autenticacion
			$mail->Username = $m_user; // usuario SMTP
			$mail->Password = $m_pass; // password SMTP
			$mail->From = $m_mail; // Desde
			$mail->FromName = $m_from; //Nombre desde
			$mail->WordWrap = 72;
			$mail->IsHTML(true);
			$mail->Subject = $m_subject;
			$query2="select mail,nombre from ".IDSERVICIO."_notificaciones_mail";
			$db->executeQuery($query2);
			while ($stInfo = $db->fetchArray())
			{
				$mail->AddAddress($stInfo[0],$stInfo[1]);
			}			
			$mail->Body = $mensaje;      
			if(!$mail->Send())
			{	sleep(20);
				$mail->Body = $mensaje;      
				if(!$mail->Send())
				{
					echo "Message was not sent1.\n";
					echo 'Mailer error1: ' . $mail->ErrorInfo . "\n";
				}
			}
	}
	
	function Alerta_Limite()
	{
		global $db;
		
		$mensaje = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Untitled Document</title>
		<style type="text/css">
		<!--
		
		.Estilo1, p{
			background-color:#FFFFFF;
			font-family: Arial, Helvetica, sans-serif;
			font-size: 14px;
			color:#000000
		}
		.Estilo2 {
			font-weight:bold;
		}
		.Estilo3 {
			background-color:#FFFFFF;
			font-family: Arial, Helvetica, sans-serif;
			font-size: 10px;
			color:#000000
		}
		-->
		</style>
		</head>
		</head>
		<body  >
		<table width="450" class="Estilo1" border="0" cellpadding="4" cellspacing="0">
		  <tr ><th width="120" class="Estilo2" scope="col" align="left">Alerta de consumo </th>
				<td align="left">
				Se&ntilde;ores el consumo del servicio contratado de bolsa de mensajes por cantidad de 50.000 ha llegado a 40.000 por favor tenga en cuenta esta alerta para no afectar su campa&ntilde;a. 
				</td>
		  </tr>    
		  
		  </table>
		  	  <p>
		  <span class="Estilo2">Validar a la brevedad de acuerdo al procedimiento establecido.<br>COMUNIQUESE INMEDIATAMENTE AL NO ESTAR DE ACUERDO CON LA PRESENTE COMUNICACION</span><br><br>
		  	<span class="Estilo2">Pdt:El procedimiento establecido en este momento es comunicarse directamente con el Usuario que ejecuto la accion, para su cambio, actualizacion y/o modificacion. Si no se logra algun cambio o dicho usuario presenta algun<br>
		  	 inconveniente para realizar cualquiera de los anteriores items mencionados; comunicarse inmediatamente a la linea de Soporte de Celmedia.</span>
		  </p>
		  </html></body>';
		
			$m_host = "mail.misp.cl";
			$m_user = "procesos@misp.cl";
			$m_pass = "proce2532";
			$m_mail = "procesos@misp.cl";
			$m_from = "Celmedia";			
			$m_subject = "[NOTIFICACION] ".NBOLSA." alerta de consumo";			
			$mail = new PHPMailer();
			$mail->IsSMTP(); // enviar via SMTP
			$mail->Host = $m_host; // Servidor SMTP
			$mail->SMTPAuth = true; // Activar Autenticacion
			$mail->Username = $m_user; // usuario SMTP
			$mail->Password = $m_pass; // password SMTP
			$mail->From = $m_mail; // Desde
			$mail->FromName = $m_from; //Nombre desde
			$mail->WordWrap = 72;
			$mail->IsHTML(true);
			$mail->Subject = $m_subject;
			$query2="select mail,nombre from ".IDSERVICIO."_notificaciones_mail";
			$db->executeQuery($query2);
			while ($stInfo = $db->fetchArray())
			{
				$mail->AddAddress($stInfo[0],$stInfo[1]);
			}			
			$mail->Body = $mensaje;      
			if(!$mail->Send())
			{	sleep(20);
				$mail->Body = $mensaje;      
				if(!$mail->Send())
				{
					echo "Message was not sent1.\n";
					echo 'Mailer error1: ' . $mail->ErrorInfo . "\n";
				}
				return;
			}
			return;
	}
		
	function generaMailCancela($id_area,$usuario,$fecha,$mensaje,$totalmensaje,$id_sel)
	{
		global $db;
		$query="select nombre from ".IDSERVICIO."_grupos where id=$id_sel";
		$db->executeQuery($query);
		$stInfo = $db->fetchRow();
		$ngrupo=$stInfo[0];
		
		$query2="select nombre from ".IDSERVICIO."_areas where id=".$id_area;
		$db->executeQuery($query2);
		$stInfo = $db->fetchArray();	
		$nombre_area=$stInfo[0];
		
		
		$mensaje = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Untitled Document</title>
		<style type="text/css">
		<!--
		
		.Estilo1, p{
			background-color:#FFFFFF;
			font-family: Arial, Helvetica, sans-serif;
			font-size: 14px;
			color:#000000
		}
		.Estilo2 {
			font-weight:bold;
		}
		.Estilo3 {
			background-color:#FFFFFF;
			font-family: Arial, Helvetica, sans-serif;
			font-size: 10px;
			color:#000000
		}
		-->
		</style>
		</head>
		</head>
		<body  >
		<p>Estimados,<br><br><br>El usuario : <span class="Estilo2">'.$usuario.'</span> Realizo la CANCELACION del siguiente envio. <br></p>
		<table width="450" class="Estilo1" border="0" cellpadding="4" cellspacing="0">
		  <tr >
		  	<tr><th width="120" class="Estilo2" scope="col" align="left">Area o Asunto</th><td align="left">'.$nombre_area.'</td></tr>
		    <tr><th width="120" class="Estilo2" scope="col" align="left">Grupo </th><td align="left">'.$ngrupo.'</td></tr>
		    <tr><th width="115" class="Estilo2" scope="col" align="left">Fecha Envio </th> <td align="left">'.$fecha.'</td></tr>
		    <tr><th width="115" class="Estilo2" scope="col" align="left">Mensaje</th><td align="left">'.$mensaje.'</td></tr>
		    <tr><th width="100" class="Estilo2" scope="col" align="left">Total Mensajes </th><td align="left">'.$totalmensaje.'</td></tr>		  
		  </table>
		  	  <p>
		  <span class="Estilo2">Validar a la brevedad de acuerdo al procedimiento establecido.<br>COMUNIQUESE INMEDIATAMENTE AL NO ESTAR DE ACUERDO CON LA PRESENTE COMUNICACION</span><br><br>
		  	<span class="Estilo2">Pdt:El procedimiento establecido en este momento es comunicarse directamente con el Usuario que ejecuto la accion, para su cambio, actualizacion y/o modificacion. Si no se logra algun cambio o dicho usuario presenta algun<br>
		  	 inconveniente para realizar cualquiera de los anteriores items mencionados; comunicarse inmediatamente a la linea de Soporte de Celmedia.</span>
		  </p>
			</html></body>';
		
			$m_host = "mail.misp.cl";
			$m_user = "procesos@misp.cl";
			$m_pass = "proce2532";
			$m_mail = "procesos@misp.cl";
			$m_from = "Celmedia";			
			$m_subject = "[CANCELACION] Envio ".NBOLSA;			
			$mail = new PHPMailer();
			$mail->IsSMTP(); // enviar via SMTP
			$mail->Host = $m_host; // Servidor SMTP
			$mail->SMTPAuth = true; // Activar Autenticacion
			$mail->Username = $m_user; // usuario SMTP
			$mail->Password = $m_pass; // password SMTP
			$mail->From = $m_mail; // Desde
			$mail->FromName = $m_from; //Nombre desde
			$mail->WordWrap = 72;
			$mail->IsHTML(true);
			$mail->Subject = $m_subject;
			$mail->Body = $mensaje;      
			$query2="select mail,nombre  from ".IDSERVICIO."_notificaciones_mail";
			$db->executeQuery($query2);
			while ($stInfo = $db->fetchArray())
			{
				$mail->AddAddress($stInfo[0],$stInfo[1]);
			}			   
			if(!$mail->Send())
			{	sleep(20);
				$mail->Body = $mensaje;      
				if(!$mail->Send())
				{
					echo "Message was not sent1.\n";
					echo 'Mailer error1: ' . $mail->ErrorInfo . "\n";
				}
			}
	}
	
	switch ($appId)
	{
		case "totalmsg":
						//se elimina doble via 06-10-2016 y al total se le suma 8464 SMS
						/*$query="select count(*) as total from 1113_momt where sentidocobro='O' AND operador <>'mispColombia';";
						$db->executeQuery($query);
						$stInfo1 = $db->fetchRow();
						$totalmo=$stInfo1[0];*/
						$query = "select sum(tigo_total+movistar_total+avantel_total+comcel_total)as total
								   from ".IDSERVICIO."_status";
						$db->executeQuery($query);
						$stInfo2 = $db->fetchRow();
						$totalmt=$stInfo2[0];
						if($totalmt=='')$totalmt=0;
						//$retorno="TOTAL SMS MT:".$totalmt."</br>TOTAL SMS MO:".$totalmo;
						$retorno="TOTAL SMS MT:".($totalmt+8464)."</br>";
						echo $retorno;
		break;
		case "ver_lista_grupos_p":
			$query = "select id,nombre,tipo,comentario from ".IDSERVICIO."_grupos where estado = 'activo' and id_usuario = '$reg_id' and tipo='personalizado'";
				$db->executeQuery($query);
			echo "<select size=\"1\" id=\"lista_grupo_p\" name=\"lista_grupo_p\">";
			while ($stInfo = $db->fetchArray())
			{
				echo "<option value=\"$stInfo[0]\">$stInfo[1]</value>";
			}
			if(!$db->getRowCount()>0){
				echo '<option disabled selected>No tiene grupos habilitados.</option>';
			}
			echo "</select>";
			break;
	
		case "grupos":
			$nombre = $_GET['g_nombre'];
			$tipo = $_GET['g_tipo'];
			$desc = $_GET['g_desc'];
			$query = "insert into ".IDSERVICIO."_grupos values ('','$nombre','$tipo','$desc','$reg_id','activo')";
			if ($db->executeQuery($query))
			{
				echo "ok";
			}
			break;
	
		case "eliminar_grupos":
			$g_id = $_GET['g_id'];
			$query = "delete from ".IDSERVICIO."_grupos where id = '$g_id' limit 1";
			if ($db->executeQuery($query))
			{
				echo "ok";
			}
			break;
		
		case "eliminar_envio_prog":
			$e_id = $_GET['e_id'];
			$datetime = date("Y-m-d H:i:s");
			$query = "update ".IDSERVICIO."_envios_pendientes set estado='cancelado' where id = '$e_id' limit 1";
			if ($db->executeQuery($query))
			{
				
				$query="select mensaje,id_grupo,fecha_hora_envio,asunto from ".IDSERVICIO."_envios_pendientes where id = '$e_id' limit 1";
				$db->executeQuery($query);
				$stInfo = $db->fetchArray();				
				if ($stInfo[0]=='unoauno')
				{
					$nMensajes=calculaTotalMensajesUnoaUno($stInfo[1],$db,$largo_sms_concatenado);
					$query = "select mensaje from ".IDSERVICIO."_numeros_unoauno as n where n.id_grupo = $stInfo[1] and estado = 'activo' limit 1";
					$db2->executeQuery($query);
					$stInfo2 = $db2->fetchRow();
					$mensaje=$stInfo2[0];				
				}
				else
				{
					$nMensajes=calculaTotalMensajes($stInfo[1],$stInfo[0],$db,$largo_sms_concatenado);
					$mensaje=$stInfo[0];	
				}
				$queryArea="update ".IDSERVICIO."_areas set cantidad_enviado=(cantidad_enviado - $nMensajes) where id=$stInfo[3] limit 1";
				$db->executeQuery($queryArea);
				generaMailCancela($stInfo[3],$reg_nombre,$stInfo[2],$mensaje,$nMensajes,$stInfo[1]);						
				echo "ok";
			}
			break;
		
		case "ver_grupos":
			$query = "select id,nombre,tipo,comentario from ".IDSERVICIO."_grupos where estado = 'activo' and (id_usuario = '$reg_id' or tipo = 'publico' or tipo = 'unoauno') order by id desc";
			$db->executeQuery($query);
			echo "<table class=\"tabla\"><tr><th>Nombre</th><th>Tipo</th><th>M&oacute;viles</th><th>Desc.</th><th>Borrar</th></tr>";
			while ($stInfo = $db->fetchArray())
			{
				$query = "select count(*) from ".IDSERVICIO."_numeros where id_grupo = '$stInfo[0]'";
				if ($stInfo[2] == 'unoauno')
				{
					 $query = "select count(*) from ".IDSERVICIO."_numeros_unoauno where id_grupo = '$stInfo[0]'"; 
				}
				$db2->executeQuery($query);
				$total = $db2->fetchArray();
				if ($stInfo[2] == 'privado')
				{
					$tipo_td = "<img src=\"img/lock.gif\" border=\"0\" alt=\"Privado\">";
				}
				elseif($stInfo[2] == 'publico')
				{
					$tipo_td = "<img src=\"img/unlock.gif\" border=\"0\" alt=\"Publico\" title=\"Publico\">";
				}
				elseif($stInfo[2] == 'personalizado')
				{
					$tipo_td = "<img src=\"img/personal_public.png\" border=\"0\" alt=\"personalizado\" title=\"personalizado\"> ";
				}
				elseif($stInfo[2] == 'unoauno')
				{
				   $tipo_td = "<img src=\"img/unoauno.png\" border=\"0\" alt=\"Uno a Uno\" title=\"Uno a Uno\"> ";
				}
				echo "<tr><td>" . wordwrap($stInfo[1], 20, "<br>", 1) . "</td><td>$tipo_td</td><td>$total[0]</td><td>" . wordwrap($stInfo[3], 20, "	<br>", 1) . "</td><td><a href=\"#\" onclick=\"eliminar_grupo('$stInfo[0]');return false\">Borrar</a></td></tr>";
			}
			echo "</table>";
			break;
			
		case "ver_lista_grupos_envio":
			$query = "select id,nombre,tipo,comentario from ".IDSERVICIO."_grupos where estado = 'activo' and (id_usuario = '$reg_id' or tipo = 'publico' or tipo='personalizado') and tipo!='unoauno'";
			$db->executeQuery($query);
			echo "<select onchange=\"contador(this.options[this.selectedIndex].title);\" size=\"1\" id=\"lista_grupo\" name=\"lista_grupo\">";
			while ($stInfo = $db->fetchArray())
			{
				if($stInfo[2]=='personalizado')
				{
				 echo "<option value=\"$stInfo[0]\" title=\"160\">$stInfo[1]</value>";
				}
				else
				{
				 echo "<option value=\"$stInfo[0]\" title=\"160\";>$stInfo[1]</value>";
				}			
			}
			if(!$db->getRowCount()>0){
				echo '<option disabled selected>No tiene grupos habilitados.</option>';
			}
			echo "</select>";
			break;
		
		case "ver_lista_grupos_agenda":
			$query = "select id,nombre,tipo,comentario from ".IDSERVICIO."_grupos where estado = 'activo' and (id_usuario = '$reg_id' or tipo = 'publico' or tipo='personalizado' or tipo='unoauno') ";
			$db->executeQuery($query);
			echo "<select onchange=\"contador(this.options[this.selectedIndex].title);confirmaMensaje();\" size=\"1\" id=\"lista_grupo\" name=\"lista_grupo\">";
			while ($stInfo = $db->fetchArray())
			{
				echo "<option value=\"$stInfo[0]\" title=\"160\" >$stInfo[1]</value>";				
			}
			if(!$db->getRowCount()>0){
				echo '<option disabled selected>No tiene grupos habilitados.</option>';
			}
			echo "</select>";
			break;
		
		case "ver_lista_grupos_envio":
			$query = "select id,nombre,tipo,comentario from ".IDSERVICIO."_grupos where estado = 'activo' and (id_usuario = '$reg_id' or tipo = 'publico' or tipo='personalizado')";
			$db->executeQuery($query);
			//echo $query;
			echo "<select onchange=\"contador(this.options[this.selectedIndex].title);\" size=\"1\" id=\"lista_grupo\" name=\"lista_grupo\">";
			while ($stInfo = $db->fetchArray())
			{
				if($stInfo[2]=='personalizado')
				{
					echo "<option value=\"$stInfo[0]\" title=\"160\">$stInfo[1]</value>";
				}
				else
				{
					echo "<option value=\"$stInfo[0]\" title=\"160\";>$stInfo[1]</value>";
				}
				
			}
			if(!$db->getRowCount()>0){
				echo '<option disabled selected>No tiene grupos habilitados.</option>';
			}
			echo "</select>";
			break;
			
		case "ver_lista_grupos_unoauno":
			$query = "select id,nombre,tipo,comentario from ".IDSERVICIO."_grupos where estado = 'activo' and  tipo = 'unoauno'";
			$db->executeQuery($query);
			echo "<select  size=\"1\" id=\"lista_grupouno\" name=\"lista_grupouno\">";
			while ($stInfo = $db->fetchArray())
			{
				 echo "<option value=\"$stInfo[0]\" >$stInfo[1]</value>";						
			}
			if(!$db->getRowCount()>0){
				echo '<option disabled selected>No tiene grupos habilitados.</option>';
			}
			echo "</select>";
			break;			
			
		case "ver_envios_prog":
			$datetime = date("Y-m-d H:i:s");

			$query = "select e.id,a.nombre,e.mensaje,g.nombre, e.fecha_hora_envio,u.nombre from ".IDSERVICIO."_envios_pendientes e, ".IDSERVICIO."_grupos g, ".IDSERVICIO."_usuarios u, ".IDSERVICIO."_areas a where e.id_grupo=g.id and u.id=g.id_usuario and e.asunto=a.id and e.estado = 'pendiente' order by fecha_hora_envio asc";
			$db->executeQuery($query);
			echo "<table class=\"tabla\"><tr><th>Agendado Por</th><th>Area</th><th>Mensaje</th><th>Grupo</th><th>Fecha Agendada</th><th>Cancelar</th></tr>";
			$count_envios_pendientes=$db->getRowCount();
			if ($count_envios_pendientes == 0 )
			{
				echo "<tr><td colspan='6'>Por el momento No existen env&iacute;os pendientes</td></tr>";
			}
			while ($stInfo = $db->fetchArray())
			{
				echo "<tr><td>" . wordwrap($stInfo[5], 20, "<br>", 1) . "</td><td>". wordwrap($stInfo[1], 20, "<br>", 1) ."</td><td>". wordwrap($stInfo[2], 20, "<br>", 1) ."</td><td>" . wordwrap($stInfo[3], 20, "	<br>", 1) . "</td><td>" . wordwrap($stInfo[4], 20, "	<br>", 1) . "</td><td><a href=\"#\" onclick=\"eliminar_envio_prog('$stInfo[0]');return false\">Cancelar</a></td></tr>";
			}
			echo "</table>";
			break;

		case "ver_lista_grupos":
			$query = "select id,nombre,tipo,comentario from ".IDSERVICIO."_grupos where estado = 'activo' and (id_usuario = '$reg_id' or tipo = 'publico') and tipo not in ('personalizado','unoauno')";
			$db->executeQuery($query);
			echo "<select size=\"1\" id=\"lista_grupo\" name=\"lista_grupo\">";
			while ($stInfo = $db->fetchArray())
			{
				echo "<option value=\"$stInfo[0]\">$stInfo[1]</value>";
			}
			if(!$db->getRowCount()>0){
				echo '<option disabled selected>No tiene grupos habilitados.</option>';
			}
			echo "</select>";
			break;
		case "ver_lista_grupos_m":
			$query = "select id,nombre,tipo,comentario from ".IDSERVICIO."_grupos where estado = 'activo' and (id_usuario = '$reg_id' or tipo = 'publico') and tipo not in ('personalizado','unoauno')";
			$db->executeQuery($query);
			echo "<select size=\"1\" id=\"lista_grupo_m\" name=\"lista_grupo_m\">";
			while ($stInfo = $db->fetchArray())
			{
				echo "<option value=\"$stInfo[0]\">$stInfo[1]</value>";
			}
			if(!$db->getRowCount()>0){
				echo '<option disabled selected>No tiene grupos habilitados.</option>';
			}
			echo "</select>";
			break;	
		
		case "ver_lista_grupos_uno":
			$query = "select id,nombre,tipo,comentario from ".IDSERVICIO."_grupos where estado = 'activo' and id_usuario = '$reg_id' and tipo='unoauno'";
			$db->executeQuery($query);
			echo "<select size=\"1\" id=\"lista_grupo_uno\" name=\"lista_grupo_uno\">";
			while ($stInfo = $db->fetchArray())
			{
				echo "<option value=\"$stInfo[0]\">$stInfo[1]</value>";
			}
			if(!$db->getRowCount()>0){
				echo '<option disabled selected>No tiene grupos habilitados.</option>';
			}
			echo "</select>";
			break;	
		
		case "enviar_mensajes":
			$id_area = $_GET['areaid'];
			$id_sel = $_GET['gid'];
			$mensaje = $_GET['mensaje'];
			$datetime = date("Y-m-d H:i:s");
			$totalmensaje=calculaTotalMensajes($id_sel,$mensaje,$db,$largo_sms_concatenado);
			$totalpermitido=calculaCuotaPermitida($db);
			if($totalpermitido < $totalmensaje)
			{
				echo "<div class='lerr'>La cuota del area fue superada</b></div>";
				return;
			}
			generaMail($id_area,$reg_nombre,$datetime,$mensaje,$totalmensaje,$id_sel);
			$query = "insert into ".IDSERVICIO."_status values ('','$id_area','$mensaje',now(),'$id_sel','0','0','0','0','0','$reg_id','','0','0','0','0','0','0','0','0','0')";
			
			$db2->executeQuery($query);
			$idEnvio=$db2->fetchLastInsertId();
			$totalBlackList = 0;
			
			$query = "select numero,operador,nombre,variable2,variable3 from ".IDSERVICIO."_numeros as n where n.id_grupo = $id_sel and estado = 'activo'";
			$db->executeQuery($query);
			$cont = $n_comcel = $n_tigo = $n_movi = $n_avan= $n_comcel_conca = $n_tigo_conca = $n_movi_conca = $n_movi_avan=$n_comcel_total = $n_tigo_total = $n_movi_total = $n_avan_total=0;
			while ($stInfo = $db->fetchRow())
			{
				$blacklist = 'N';
				$numero = $stInfo[0];
				if ($stInfo[2]!= null)
				{
					$mensaje_tmp = str_replace("_NAME_", $stInfo[2], $mensaje);
					$mensaje_tmp = str_replace("_NAME2_", $stInfo[3], $mensaje_tmp);
					$mensaje_tmp = str_replace("_NAME3_", $stInfo[4], $mensaje_tmp);
				}
				else
				{
					$mensaje_tmp = $mensaje;
				}
				
				$n_sms_conca=$resto_conca=0;
				$n_sms_conca=floor(strlen($mensaje_tmp)/$largo_sms_concatenado);
				$resto_conca=strlen($mensaje_tmp)%$largo_sms_concatenado;
				if ($resto_conca > 0)
				{
					$n_sms_conca++;
				}
				$operador = GetPortabilidad($numero);
				//switch (trim($stInfo[1]))
				$NC_DEFINIDO = "85390";
				$PING_DEFINIDO = "85390";	
				switch(strtolower($operador))			
				{
					case "comcel";
						$n_comcel++;				
						if(strlen($mensaje_tmp)<=160)
						{
							$operador = "Comcel";
							$n_comcel_total++;
						}
						else
						{
							$NC_DEFINIDO = "85385";
							$PING_DEFINIDO = "85385";
							$operador = "celcsms-comcel";
							$operadorLog = "Comcel";
							$n_comcel_conca=$n_comcel_conca+$n_sms_conca;
							$n_comcel_total=$n_comcel_total+$n_sms_conca;
						}
						
						break;
					case "tigo";
						$n_tigo++;
						if(strlen($mensaje_tmp)<=160)
						{
							$operador = "Colombia_movil";
							$n_tigo_total++;
						}
						else
						{
							$operador = "celcsms-tigo";
							$operadorLog = "Colombia_movil";
							$n_tigo_conca=$n_tigo_conca+$n_sms_conca;
							$n_tigo_total=$n_tigo_total+$n_sms_conca;
						}
						break;
					case "movistar";
						$n_movi++;
						if(strlen($mensaje_tmp)<=160)
						{
							$operador = "Movistar";
							$n_movi_total++;
						}
						else
						{
							$operador = "celcsms-movistar";
							$operadorLog = "Movistar";
							$n_movi_conca=$n_movi_conca+$n_sms_conca;
							$n_movi_total=$n_movi_total+$n_sms_conca;
						}
						break;
					case "avantel";
						$n_avan++;
						if(strlen($mensaje_tmp)<=160)
						{
							$operador = "Avantel";
							$n_avan_total++;
						}
						else
						{
							//$operador = "celcsms-avantel";
							$operador = "celcsms-tigo";
							$operadorLog = "Avantel";
							$n_avan_conca=$n_avan_conca+$n_sms_conca;
							$n_avan_total=$n_avan_total+$n_sms_conca;
						}
						break;
				}
				
							
				$movilList = 'N';
				$queryList = "select movil from ".IDSERVICIO."_blacklist where movil = '".$numero."';";
				$db2->executeQuery($queryList);
				$totalDB = $db2 -> getRowCount();
				if($totalDB > 0)
				{
					$blacklist = 'S';
					$blacklist_total++;
				}
					
				if($blacklist == 'N')
				{	
					if($operador=="celcsms-movistar" || $operador=="celcsms-tigo" || $operador=="celcsms-comcel" || $operador=="celcsms-avantel")
					{
						$concatenado="S";
						//echo "lynx -dump \"http://127.0.0.1:13013/cgi-bin/sendsms?username=KcelCOL&password=kCEL68798col&smsc={$operador}&text=".urlencode($mensaje)."&from=50050&to={$numero }\"" ."<br />";
						####ENVIO####
						$texto_enviar=urlencode($mensaje_tmp);
						$url_enviosms = "http://127.0.0.1:13013/cgi-bin/sendsms?username=KcelCOL&password=kCEL68798col&smsc={$operador}&text={$texto_enviar}&from=".$NC_DEFINIDO."&to={$numero}";
						//echo $exec=shell_exec("lynx -dump \"http://127.0.0.1:13013/cgi-bin/sendsms?username=KcelCOL&password=kCEL68798col&smsc=celcsms-movistar&text={$texto_enviar}&from=50050&to={$numero}\"");
						#############
											
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_HEADER, 0);
						curl_setopt($ch, CURLOPT_NOBODY, 0);
						curl_setopt($ch, CURLOPT_URL, $url_enviosms);
						curl_setopt($ch, CURLOPT_POST, 0);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
						if ($resultado = curl_exec($ch))
						{
							$query = "insert into ".IDSERVICIO."_log values ('','".$NC_DEFINIDO."','$id_sel',now(),'573".substr($numero, -9)."','$operadorLog','$mensaje_tmp','activo','$idEnvio','web')";
							$db2->executeQuery($query);
							
							
							$query = "insert into ".IDSERVICIO."_enviados (fecha_hora_enviado,fecha_hora_procesado,origen,destino,mensaje,respuesta,emisor,estado)";
							$query.= " values (now(),now(),'".$NC_DEFINIDO."','573".substr($numero, -9)."','".NBOLSA."','".$mensaje_tmp."','".$operadorLog."','activo') ";
							$db2->executeQuery($query);
							$QueryInMOMT = "INSERT INTO ".IDSERVICIO."_momt (idservicio,idarea,operador,numerocorto,idservicioopcion,fechahorarecibido,movil,mensaje,fechahoraresponse,response,sentidocobro,valido)
                            VALUES ('".IDSERVICIO."','".$id_area."','".$operadorLog."','".$NC_DEFINIDO."','0',now(),'573".substr($numero, -9)."','".$mensaje_tmp."',now(),'message accepted for process.','T','activo');";
							$db2->executeQuery($QueryInMOMT);						
							$SQL = "INSERT INTO cel_momt (idservicio, operador, numerocorto, idservicioopcion, fechahorarecibido, movil, mensaje, fechahoraresponse, fechahorarequest, response, SentidoCobro, Valido) ".
									   " VALUES (".IDSERVICIO.", '$operadorLog', ".$NC_DEFINIDO.", 0, now(), '573".substr($numero, -9)."', '$mensaje_tmp', now(), now(), 'message accepted for process.', 'T', 'S');";
							$db2->executeQuery($SQL);
							
		                
						}
						else
						{
							$query = "insert into ".IDSERVICIO."_log values ('','".$NC_DEFINIDO."','$id_sel',now(),'$numero','$operadorLog','$mensaje_tmp','inactivo','$idEnvio','web')";
							$db2->executeQuery($query);
						}
							$cont=$cont+$n_sms_conca;
					}
					else
					{ 
						$concatenado="N";
						//echo $mensaje_tmp;
					
						$texto_enviar = urlencode($mensaje_tmp);
						$url_enviosms = "http://localhost/mispColombia/client/sendSMS.php?from=";
						$url_enviosms .= $numero . "&to=" . $PING_DEFINIDO . "&message=" . $texto_enviar;
						$url_enviosms .= "&now=true&operator=".$operador."&messageId=null";
						//echo $url_enviosms . "<br>";
						$timeout = 15;
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_HEADER, 0);
						curl_setopt($ch, CURLOPT_NOBODY, 0);
						curl_setopt($ch, CURLOPT_URL, $url_enviosms);
						curl_setopt($ch, CURLOPT_POST, 0);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
						if ($resultado = curl_exec($ch))
						{
							$query = "insert into ".IDSERVICIO."_log values ('','".$NC_DEFINIDO."','$id_sel',now(),'573".substr($numero, -9)."','$operador','$mensaje_tmp','activo','$idEnvio','web')";
							$db2->executeQuery($query);										
							$query = "insert into ".IDSERVICIO."_enviados (fecha_hora_enviado,fecha_hora_procesado,origen,destino,mensaje,respuesta,emisor,estado)";
							$query.= " values (now(),now(),'".$NC_DEFINIDO."','573".substr($numero, -9)."','".NBOLSA."','".$mensaje_tmp."','".$operador."','activo') ";
							$db2->executeQuery($query);
							$QueryInMOMT = "INSERT INTO ".IDSERVICIO."_momt (idservicio,idarea,operador,numerocorto,idservicioopcion,fechahorarecibido,movil,mensaje,fechahoraresponse,response,sentidocobro,valido)
                            VALUES ('".IDSERVICIO."','".$id_area."','".$operador."','".$NC_DEFINIDO."','0',now(),'573".substr($numero, -9)."','".$mensaje_tmp."',now(),'message accepted for process.','T','activo');";
							$db2->executeQuery($QueryInMOMT);		
							$SQL = "INSERT INTO cel_momt (idservicio, operador, numerocorto, idservicioopcion, fechahorarecibido, movil, mensaje, fechahoraresponse, fechahorarequest, response, SentidoCobro, Valido) 
									    VALUES (".IDSERVICIO.", '$operador', ".$NC_DEFINIDO.", 0, now(), '573".substr($numero, -9)."', '$mensaje_tmp', now(), now(), 'message accepted for process.', 'T', 'S');";
							$db2->executeQuery($SQL);
						}
						else
						{
							$query = "insert into ".IDSERVICIO."_log values ('','".$NC_DEFINIDO."','$id_sel',now(),'573".substr($numero, -9)."','$operador','$mensaje_tmp','inactivo','$idEnvio','web')";
							$db2->executeQuery($query);
						}
						$cont++;
					}
				}
				else
				{
					$totalBlackList++;
				}		
			}
			$query="update ".IDSERVICIO."_areas set cantidad_enviado=(cantidad_enviado+$cont) where id=$id_area";
			$db2->executeQuery($query);
							
			$query="update ".IDSERVICIO."_status set total='$cont', comcel='$n_comcel', tigo='$n_tigo', movistar='$n_movi', avantel='$n_avan', concatenados='$concatenado', comcel_conca='$n_comcel_conca', tigo_conca='$n_tigo_conca', movistar_conca='$n_movi_conca', avantel_conca='$n_avan_conca', comcel_total='$n_comcel_total', tigo_total='$n_tigo_total', movistar_total='$n_movi_total', avantel_total='$n_avan_total', mensaje='$mensaje_tmp',blacklist_total ='$totalBlackList' where id='$idEnvio' limit 1";
			$db2->executeQuery($query);
			
			echo "<div class=lval>Se enviaron <b>$cont</b> sms, con el siguiente mensaje:<b>$mensaje</b></div>";
			echo "<div class='lerr'>Total lista negra <b>$blacklist_total</b></div>";
			break;
			
			
			case "enviar_mensajes_unoauno":
			$id_area = $_GET['areaid'];
			$id_sel = $_GET['gid'];
			$datetime = date("Y-m-d H:i:s");
			
			$totalmensaje=calculaTotalMensajesUnoaUno($id_sel,$db,$largo_sms_concatenado);
			$totalpermitido=calculaCuotaPermitida($db);
			if($totalpermitido < $totalmensaje)
			{
				echo "<div class='lerr'>La cuota del area fue superada</b></div>";
				return;
			}
			$query = "select mensaje from ".IDSERVICIO."_numeros_unoauno as n where n.id_grupo = $id_sel and estado = 'activo' limit 1";
			$db->executeQuery($query);
			$stInfo = $db->fetchRow();
			generaMail($id_area,$reg_nombre,$datetime,$stInfo[0],$totalmensaje,$id_sel);
			$totalBlackList = 0;
			
			$query = "insert into ".IDSERVICIO."_status values ('','$id_area','mensaje uno a uno',now(),'$id_sel','0','0','0','0','0','$reg_id','','0','0','0','0','0','0','0','0','0')";
			$db2->executeQuery($query);
			$idEnvio=$db2->fetchLastInsertId();
			$query = "select numero,operador,mensaje from ".IDSERVICIO."_numeros_unoauno as n where n.id_grupo = $id_sel and estado = 'activo'";
			$db->executeQuery($query);
			$cont = $n_comcel = $n_tigo = $n_movi= $n_avan = $n_comcel_conca = $n_tigo_conca = $n_movi_conca = $n_avan_conca=$n_comcel_total = $n_tigo_total = $n_movi_total= $n_avan_total =0;
			while ($stInfo = $db->fetchRow())
			{
				$blacklist = 'N';
				$numero = $stInfo[0];
				$mensaje_tmp = $stInfo[2];
				$n_sms_conca=$resto_conca=0;
				$n_sms_conca=floor(strlen($mensaje_tmp)/$largo_sms_concatenado);
				$resto_conca=strlen($mensaje_tmp)%$largo_sms_concatenado;
				if ($resto_conca > 0)
				{
					$n_sms_conca++;
				}
				if ($mensaje_tmp != '')
				{

						$operador = GetPortabilidad($numero);
						//switch (trim($stInfo[1]))
						$NC_DEFINIDO = "85390";
						$PING_DEFINIDO = "85390";					
						switch(strtolower($operador))			
						{
							case "comcel";
								$n_comcel++;				
								if(strlen($mensaje_tmp)<=160)
								{
									$operador = "Comcel";
									$n_comcel_total++;
								}
								else
								{
									$NC_DEFINIDO = "85385";
									$PING_DEFINIDO = "85385";
									$operador = "celcsms-comcel";
									$operadorBolsa = "Comcel";
									$n_comcel_conca=$n_comcel_conca+$n_sms_conca;
									$n_comcel_total=$n_comcel_total+$n_sms_conca;
								}						
								break;
							case "tigo";
								$n_tigo++;
								if(strlen($mensaje_tmp)<=160)
								{
									$operador = "Colombia_movil";
									$n_tigo_total++;
								}
								else
								{
									$operador = "celcsms-tigo";
									$operadorBolsa = "Colombia_movil";
									$n_tigo_conca=$n_tigo_conca+$n_sms_conca;
									$n_tigo_total=$n_tigo_total+$n_sms_conca;
								}
								break;
							case "movistar";
								$n_movi++;
								if(strlen($mensaje_tmp)<=160)
								{
									$operador = "Movistar";
									$n_movi_total++;
								}
								else
								{
									$operador = "celcsms-movistar";
									$operadorBolsa = "Movistar";
									$n_movi_conca=$n_movi_conca+$n_sms_conca;
									$n_movi_total=$n_movi_total+$n_sms_conca;
								}
								break;
							case "avantel";
								$n_avan++;
								if(strlen($mensaje_tmp)<=160)
								{
									$operador = "Avantel";
									$n_avan_total++;
								}
								else
								{
									//$operador = "celcsms-avantel";
									$operador = "celcsms-tigo";
									$operadorBolsa = "Avantel";
									$n_avan_conca=$n_avan_conca+$n_sms_conca;
									$n_avan_total=$n_avan_total+$n_sms_conca;
								}
								break;
						}
					$movilList = 'N';
					$queryList = "select movil from ".IDSERVICIO."_blacklist where movil = '".$numero."';";
					$db2->executeQuery($queryList);
					$totalDB = $db2 -> getRowCount();
					if($totalDB > 0)
					{
						$blacklist = 'S';
						$blacklist_total++;
					}
					
					if($blacklist == 'N')
					{		
													
						if($operador=="celcsms-movistar" || $operador=="celcsms-tigo" || $operador=="celcsms-comcel" || $operador=="celcsms-avantel")
						{
							$concatenado="S";
							 ####ENVIO####
							$texto_enviar=urlencode($mensaje_tmp);
							$url_enviosms = "http://127.0.0.1:13013/cgi-bin/sendsms?username=KcelCOL&password=kCEL68798col&smsc={$operador}&text={$texto_enviar}&from=".$NC_DEFINIDO."&to={$numero}";
							#############										
							$ch = curl_init();
							curl_setopt($ch, CURLOPT_HEADER, 0);
							curl_setopt($ch, CURLOPT_NOBODY, 0);
							curl_setopt($ch, CURLOPT_URL, $url_enviosms);
							curl_setopt($ch, CURLOPT_POST, 0);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
							if ($resultado = curl_exec($ch))
							{
								$query = "insert into ".IDSERVICIO."_log values ('','".$NC_DEFINIDO."','$id_sel',now(),'573".substr($numero, -9)."','$operadorBolsa','$mensaje_tmp','activo','$idEnvio','web')";
								$db2->executeQuery($query);
								$query = "insert into log_enviados (fecha_hora_enviado,fecha_hora_procesado,origen,destino,mensaje,respuesta,emisor,estado)";
								$query.= " values (now(),now(),'".$NC_DEFINIDO."','573".substr($numero, -9)."','".NBOLSA."','".$mensaje_tmp."','".$operadorBolsa."','activo') ";
								$db2->executeQuery($query);
								$QueryInMOMT = "INSERT INTO ".IDSERVICIO."_momt (idservicio,idarea,operador,numerocorto,idservicioopcion,fechahorarecibido,movil,mensaje,fechahoraresponse,response,sentidocobro,valido)
								VALUES ('".IDSERVICIO."','".$id_area."','".$operadorBolsa."','".$NC_DEFINIDO."','0',now(),'573".substr($numero, -9)."','".$mensaje_tmp."',now(),'message accepted for process.','T','activo');";
								$db2->executeQuery($QueryInMOMT);
								$SQL = "INSERT INTO cel_momt (idservicio, operador, numerocorto, idservicioopcion, fechahorarecibido, movil, mensaje, fechahoraresponse, fechahorarequest, response, SentidoCobro, Valido) 
									    VALUES (".IDSERVICIO.", '$operadorBolsa', '".$NC_DEFINIDO."', 0, now(), '573".substr($numero, -9)."', '$mensaje_tmp', now(), now(), 'message accepted for process.', 'T', 'S');";
								$db2->executeQuery($SQL);								
							}
							else
							{
								$query = "insert into ".IDSERVICIO."_log values ('','".$NC_DEFINIDO."','$id_sel',now(),'573".substr($numero, -9)."','$operadorBolsa','$mensaje_tmp','inactivo','$idEnvio','web')";
								$db2->executeQuery($query);
							}
							$cont=$cont+$n_sms_conca;					
						}
						else
						{
							$concatenado="N";
							//echo $mensaje_tmp;
							$texto_enviar = urlencode($mensaje_tmp);
							$url_enviosms = "http://localhost/mispColombia/client/sendSMS.php?from=";
							$url_enviosms .= $numero . "&to=" . $PING_DEFINIDO . "&message=" . $texto_enviar;
							$url_enviosms .= "&now=true&operator=".$operador."&messageId=null";
							
							$timeout = 15;
							$ch = curl_init();
							curl_setopt($ch, CURLOPT_HEADER, 0);
							curl_setopt($ch, CURLOPT_NOBODY, 0);
							curl_setopt($ch, CURLOPT_URL, $url_enviosms);
							curl_setopt($ch, CURLOPT_POST, 0);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
							if ($resultado = curl_exec($ch))
							{
								$query = "insert into ".IDSERVICIO."_log values ('','".$NC_DEFINIDO."','$id_sel',now(),'573".substr($numero, -9)."','$operador','$mensaje_tmp','activo','$idEnvio','web')";
								$db2->executeQuery($query);
								$query = "insert into log_enviados (fecha_hora_enviado,fecha_hora_procesado,origen,destino,mensaje,respuesta,emisor,estado)";
								$query.= " values (now(),now(),'".$NC_DEFINIDO."','573".substr($numero, -9)."','".NBOLSA."','".$mensaje_tmp."','".$operador."','activo') ";
								$db2->executeQuery($query);
								$QueryInMOMT = "INSERT INTO ".IDSERVICIO."_momt (idservicio,idarea,operador,numerocorto,idservicioopcion,fechahorarecibido,movil,mensaje,fechahoraresponse,response,sentidocobro,valido)
											    VALUES ('".IDSERVICIO."','".$id_area."','".$operador."','".$NC_DEFINIDO."','0',now(),'573".substr($numero, -9)."','".$mensaje_tmp."',now(),'message accepted for process.','T','activo');";
								$db2->executeQuery($QueryInMOMT);
								$SQL = "INSERT INTO cel_momt (idservicio, operador, numerocorto, idservicioopcion, fechahorarecibido, movil, mensaje, fechahoraresponse, fechahorarequest, response, SentidoCobro, Valido) 
									    VALUES (".IDSERVICIO.", '$operador', '".$NC_DEFINIDO."', 0, now(), '573".substr($numero, -9)."', '$mensaje_tmp', now(), now(), 'message accepted for process.', 'T', 'S');";
								$db2->executeQuery($SQL);	
								
							}
							else
							{
								$query = "insert into ".IDSERVICIO."_log values ('','".$NC_DEFINIDO."','$id_sel',now(),'573".substr($numero, -9)."','$operador','$mensaje_tmp','inactivo','$idEnvio','web')";
								$db2->executeQuery($query);
							}
							$cont++;
						}
					}
					else
					{
						$totalBlackList++;
					}	
						

				}
			}
			$query="update ".IDSERVICIO."_areas set cantidad_enviado=(cantidad_enviado+".$cont.") where id=$id_area";
			$db2->executeQuery($query);
			$query="update ".IDSERVICIO."_status set total='$cont', comcel='$n_comcel', tigo='$n_tigo', movistar='$n_movi', avantel='$n_avan', concatenados='$concatenado', comcel_conca='$n_comcel_conca', tigo_conca='$n_tigo_conca', movistar_conca='$n_movi_conca', avantel_conca='$n_avan_conca', comcel_total='$n_comcel_total', tigo_total='$n_tigo_total', movistar_total='$n_movi_total', avantel_total='$n_avan_total', mensaje='$mensaje_tmp',blacklist_total ='$totalBlackList' where id='$idEnvio' limit 1";
			$db2->executeQuery($query);

			echo "<div class=lval>Se enviaron <b>$cont</b> sms, con el siguiente mensaje:<b>$mensaje_tmp</b></div>";
			echo "<div class='lerr'>Total lista negra <b>$blacklist_total</b></div>";
			break;
		case "buscamovil":
			$movil = $_GET['movil'];
			$movil='573'.substr($movil, -9);
			$fecha_envio = $_GET['fecha_envio'];
			$fecha_termino = $_GET['fecha_termino'];
			if (ctype_digit($movil))
			{
				
				$SQL="select fecha,shortcode,sms_destino,mensaje from ".IDSERVICIO."_log where date(fecha) between '$fecha_envio' and '$fecha_termino' 
							and sms_destino='$movil';";
							
				$resSQL=$db->executeQuery($SQL);
				$nregistros=$db->getRowCount();
				if (!$resSQL)
				{
						echo "Ha ocurrido un error. Intente nuevamente por favor.";
						exit();
				}	
				echo "<table class=\"tabla\"><tr><th>Fecha Hora</th><th>Numero Corto</th><th>Celular</th><th>Mensaje</th></tr>";				
				if ($nregistros > 0)
				{
					while ($stInfo = $db->fetchArray())
					{
						if (strlen($stInfo[1])>0) $numcorto=$stInfo[1]; 
						else $numcorto="s/i"; 
						echo "<tr><td>$stInfo[0]</td><td>$numcorto</td><td>$stInfo[2]</td><td>" . wordwrap($stInfo[3], 40, "	<br>", 1) . "</td></tr>";
					}
				}
				else
				{ 
					echo "<tr><td colspan='4'>No existen registros para el m&oacute;vil <b>".$movil."</b></td></tr>";     
				}		
			}
			echo "</table>";
			break;
		case "guardar_envio":
			$areaid = $_GET['areaid'];
			$id_grupo = $_GET['gid'];
			$mensaje = $_GET['mensaje'];
			$fecha = $_GET['fecha'];
			$inicio_hor = $_GET['inicio_hor'];
			$inicio_min = $_GET['inicio_min'];
			$fechahora_envio=$fecha." ".$inicio_hor.":".$inicio_min.":00";
			$datetime = date("Y-m-d H:i:s");
			
			$query="select fecha_hora_envio from ".IDSERVICIO."_envios_pendientes where fecha_hora_envio= '$fechahora_envio' and estado = 'pendiente' order by fecha_hora_envio desc limit 1";
			$db->executeQuery($query);
			$stInfo5 = $db->fetchRow();
			$fechahoraenvio=$stInfo5[0];
			if ($fechahora_envio != $fechahoraenvio)
			{
				$query="select tipo from ".IDSERVICIO."_grupos where id='$id_grupo'";
				$db->executeQuery($query);
				$stInfo = $db->fetchRow();
				$tipoGrupo=$stInfo[0];
				if ($tipoGrupo=='unoauno')
				{
					$mensajeEnvio='unoauno';
					$totalmensaje=calculaTotalMensajesUnoaUno($id_grupo,$db,$largo_sms_concatenado);
					$totalpermitido=calculaCuotaPermitida($db);
					if($totalpermitido < $totalmensaje)
					{
						echo "La cuota del area fue superada";
						return;
					}
					$query = "select mensaje from ".IDSERVICIO."_numeros_unoauno  where id_grupo = $id_grupo and estado = 'activo' limit 1";
					$db->executeQuery($query);
					$stInfo = $db->fetchRow();	
					$mensajeMail=$stInfo[0];	
				}
				else
				{
					$mensajeEnvio=$mensaje;
					$mensajeMail=$mensaje;
					$totalmensaje=calculaTotalMensajes($id_grupo,$mensaje,$db,$largo_sms_concatenado);
					$totalpermitido=calculaCuotaPermitida($db);
					if($totalpermitido < $totalmensaje)
					{
						echo "La cuota del area fue superada";
						return;
					}
				}
				generaMail($areaid,$reg_nombre,$fechahora_envio,$mensajeMail,$totalmensaje,$id_grupo);
				$query="insert into ".IDSERVICIO."_envios_pendientes set asunto='$areaid', mensaje='$mensajeEnvio', id_grupo='$id_grupo', estado='pendiente', id_usr_envio='$reg_id', fecha_hora_envio='$fechahora_envio', fecha_hora_creado='$datetime'";
				if ($db->executeQuery($query))
				{
					$queryArea="update ".IDSERVICIO."_areas set cantidad_enviado=(cantidad_enviado+$totalmensaje) where id=$areaid limit 1";
					$db->executeQuery($queryArea);
					echo "ok";
				}
			}
			else
			{
				echo "Hora envio ya utilizado, favor volver a agendar.";
			}
			break;
		case "agregar_numeros":
			$id_sel = $_GET['id_lista'];
			$numeros = $_GET['numeros'];
			$prefijo = substr($numeros, -10, 3);
			$numeros = substr($numeros, -10);
			$comcel = array(310, 311, 312, 313, 314, 320, 321,322, 323, 324, 325,326);
			$movistar = array(315, 316, 317, 318, 319);
			$tigo = array(300, 301, 302, 303, 304,305, 306, 307);
			$avantel = array(350, 351, 352, 353, 354,355);
			$operador = "";
			if (in_array($prefijo, $comcel))
			{
				$operador = "comcel";
			} elseif (in_array($prefijo, $movistar))
			{
				$operador = "movistar";
			} elseif (in_array($prefijo, $tigo))
			{
				$operador = "tigo";
			}elseif (in_array($prefijo, $avantel))
			{
				$operador = "avantel";
			}
			if ($operador != "")
			{
				$query = "insert into ".IDSERVICIO."_numeros values ('','$id_sel','$numeros','$operador','activo','','','')";
				if ($db->executeQuery($query))
				{
					echo "<div class='lval'>El M&oacute;vil <b>$numeros</b> fue agregado correctamente a la lista.</div>";
				}
				else
				{
					$query = "insert into ".IDSERVICIO."_numeros_error values ('','$id_sel','$numeros','".$operador."-suscrito')";
					$db->executeQuery($query);
					echo "<div class='linv'>El M&oacute;vil <b>$numeros</b> ya se encuentra suscrito a la lista.</div>";
				}
			}
			else
			{
				if ($numeros!="")
				{
					$query = "insert into ".IDSERVICIO."_numeros_error values ('','$id_sel','$numeros','no valido')";
					$db->executeQuery($query);
				}
				echo "<div class='lerr'>El M&oacute;vil <b>$numeros</b> no tiene Operador v&aacute;lido.</div>";
			}
			break;
		case "agregar_numeros_negra":
			$id_sel = $_GET['id_lista'];
			$numeros = $_GET['numeros'];
			$user = $_GET['user'];
			$prefijo = substr($numeros, -10, 3);
			$numeros = substr($numeros, -10);
			$comcel = array(310, 311, 312, 313, 314, 320, 321,322, 323, 324, 325,326);
			$movistar = array(315, 316, 317, 318, 319);
			$tigo = array(300, 301, 302, 303, 304,305, 306, 307);
			$avantel = array(350, 351, 352, 353, 354,355);
			$operador = "";
			if (in_array($prefijo, $comcel))
			{
				$operador = "comcel";
			} elseif (in_array($prefijo, $movistar))
			{
				$operador = "movistar";
			} elseif (in_array($prefijo, $tigo))
			{
				$operador = "tigo";
			}elseif (in_array($prefijo, $avantel))
			{
				$operador = "avantel";
			}
			if ($operador != "")
			{
				$query = "insert into ".IDSERVICIO."_blacklist(movil,operador,fechahorainscrito,user_inscribe) values ('$numeros','$operador',now(),'".$user."');";
				if ($db->executeQuery($query))
				{
					echo "<div class='lval'>El M&oacute;vil <b>$numeros</b> fue agregado correctamente a la lista.</div>";
				}
				else
				{
					/*$query = "insert into linio_numeros_error values ('','$id_sel','$numeros','".$operador."-suscrito')";
					$db->executeQuery($query);*/
					echo "<div class='linv'>El M&oacute;vil <b>$numeros</b> ya se encuentra suscrito a la lista.</div>";
				}
			}
			else
			{
				/*if ($numeros!="")
				{
					$query = "insert into linio_numeros_error values ('','$id_sel','$numeros','no valido')";
					$db->executeQuery($query);
				}*/
				echo "<div class='lerr'>El M&oacute;vil <b>$numeros</b> no tiene Operador v&aacute;lido.</div>";
			}
			break;		
		case 'confirmaMensaje':
			$id = $_GET['gid'];
			$query="select tipo from ".IDSERVICIO."_grupos where id='$id'";
			$db->executeQuery($query);
			$stInfo = $db->fetchRow();
			if ($stInfo[0]=='unoauno')
			{
				echo "err";
			}
			else
			{
				echo "ok";
			}
			break;
	  case "ver_lista_area":
			$query = "select id,nombre,cantidad_enviado from ".IDSERVICIO."_areas where estado = 'activo' order by id";
			$db->executeQuery($query);
			echo "<select  size=\"1\" id=\"lista_area\" name=\"lista_area\">";
			while ($stInfo = $db->fetchArray())
			{
				 echo "<option value=\"$stInfo[0]\" >$stInfo[1] </value>";				
			}
			if(!$db->getRowCount()>0){
				echo '<option disabled selected>No tiene areas habilitadas.</option>';
			}
			echo "</select>";
			break;
		 case "ver_lista_areaunoauno":
			$query = "select id,nombre,cantidad_enviado from ".IDSERVICIO."_areas where estado = 'activo' order by id";
			$db->executeQuery($query);
			echo "<select  size=\"1\" id=\"lista_areaunoauno\" name=\"lista_areaunoauno\">";
			while ($stInfo = $db->fetchArray())
			{
				 echo "<option value=\"$stInfo[0]\" >$stInfo[1]</value>";				
			}
			echo "</select>";
			break;
		case "saber_tipo_grupo":
			$id_grupo = $_GET['grupoid'];
			$query="select tipo from ".IDSERVICIO."_grupos where id='$id_grupo' ";
			$db->executeQuery($query);
			$stInfo = $db->fetchRow();
			echo $stInfo[0];
			break;
		case "grupo_total":
			$id_grupo = $_GET['e_id'];
			$query="select count(*) from ".IDSERVICIO."_numeros where estado='activo' and id_grupo='$id_grupo' ";
			$db->executeQuery($query);
			$stInfo = $db->fetchRow();
			echo $stInfo[0];
			break;
		case "grupo_total_uno":
			$id_grupo = $_GET['e_id'];
			$query="select count(*), mensaje from ".IDSERVICIO."_numeros_unoauno where estado='activo' and id_grupo='$id_grupo' ";
			$db->executeQuery($query);
			$stInfo = $db->fetchRow();
			echo $stInfo[0]."|".$stInfo[1];
			break;
		case "buscamo":
			$fecha_envio = $_GET['fecha_envio'];
			$fecha_termino = $_GET['fecha_termino'];
			$SQL="select fechahorarecibido,operador,right(movil,10),mensaje from ".IDSERVICIO."_momt where date(fechahorarecibido) between '$fecha_envio' and '$fecha_termino' 
							 and sentidocobro='O' and operador != 'mispColombia'";				
							/*query produccion..*/
			$resSQL=$db->executeQuery($SQL);
			$nregistros=$db->getRowCount();
			if (!$resSQL)
			{
					echo "Ha ocurrido un error. Intente nuevamente por favor.";
					exit();
			}	
			$dato=$fecha_envio."|".$fecha_termino;
				
			$excel = "Descargar Reporte <a href=\"descargarmo.php?id=$dato\"><img src='img/descargar.png' style='width:70 px; height:30px;'/></a>";
			if ($nregistros > 0)
			{
				echo $excel;
			}				
			echo "<br><br><table class=\"tabla2\"><tr><th></th><th>Fecha Hora</th><th>Movil</th><th>Operador</th><th>Mensaje</th></tr>";		
			if ($nregistros > 0)
			{
				$i=0;
				while ($stInfo = $db->fetchArray())
				{
					$i++;
					echo "<tr><td>$i</td><td>$stInfo[0]</td><td>$stInfo[2]</td><td>$stInfo[1]</td><td>" . wordwrap($stInfo[3], 90, "	<br>", 1) . "</td></tr>";
				}
			}
			else
			{
				echo "<tr><td colspan='5'>No existen registros para la fechas consultadas.</b></td></tr>";   
			}
			break;
		default:
			echo "error_fatal";
	}

?>