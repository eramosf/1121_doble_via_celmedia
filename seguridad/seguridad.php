<?php
$nuevoGET = array();
$nuevoPOST = array();
$nuevoREQUEST = array();
foreach ($_GET as $indice => $valor){
	$nuevoGET[$indice]=htmlspecialchars(addslashes(strip_tags($valor)), ENT_QUOTES, 'UTF-8');
}
foreach ($_POST as $indice => $valor){
	$nuevoPOST[$indice]=htmlspecialchars(addslashes(strip_tags($valor)), ENT_QUOTES, 'UTF-8');
}
foreach ($_REQUEST as $indice => $valor){
	$nuevoREQUEST[$indice]=htmlspecialchars(addslashes(strip_tags($valor)), ENT_QUOTES, 'UTF-8');
}
unset ($_GET);
unset ($_POST);
unset ($_REQUEST);
$_GET=$nuevoGET; 
$_POST=$nuevoPOST; 
$_REQUEST=$nuevoREQUEST;

?>