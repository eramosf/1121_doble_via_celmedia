<?php
	//error_reporting(E_ALL);
	require ('login.head.php');
	require ("config/dbal.class.php");

	set_time_limit(0);
	ini_set("max_input_time", "-1");
	ini_set("memory_limit", "2048M");
	
	$id_sesion = $_POST['idsesion'];
	if ($id_sesion != $_SESSION['idsess'.NBOLSA])
	{
		exit();
	}
	
	
	$db = new dbal;
	$conectado = $db->setAll($kDbalDriver, $kDatabaseUsername, $kDatabasePassword, $kDatabaseHostname, $kDatabaseName);
	$db2 = new dbal;
	$conectado2 = $db2->setAll($kDbalDriver, $kDatabaseUsername, $kDatabasePassword, $kDatabaseHostname, $kDatabaseName);

	$id_sel = $_POST['lista_grupo_p'];
	$nombre_archivo = $HTTP_POST_FILES['filexp']['name'];
	$tipo_archivo = $HTTP_POST_FILES['filexp']['type'];
	$tamano_archivo = $HTTP_POST_FILES['filexp']['size'];
	$datetime = date("YmdHis", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
	$nombre_directorio = "files/";
	$msg = "";
	$nombre_archivo = "numeros_" . $datetime . ".txt";
	$result = "false";
	if (move_uploaded_file($_FILES['filexp']['tmp_name'], $nombre_directorio . $nombre_archivo))
	{
		$archivo = $nombre_directorio . $nombre_archivo;

		$dataFile = fopen($archivo, "r");

		if ($dataFile)
		{
			while (!feof($dataFile))
			{
				$buffer = explode(";", fgets($dataFile, 4096));
                                $numero=trim($buffer[0]);
                                $nombre=trim($buffer[1]);
                                $var2=trim($buffer[2]);
                                $var3=trim($buffer[3]);
                                if(is_numeric($numero))
                                {
                                    $numeros = trim($buffer[0]);
                                    $prefijo = substr($numeros, -10, 3);
                                    $numeros = substr($numeros, -10);
																		$comcel = array(310, 311, 312, 313, 314, 320, 321,322, 323, 324, 325,326);
																		$movistar = array(315, 316, 317, 318, 319);
																		$tigo = array(300, 301, 302, 303, 304,305, 306, 307);
            												$avantel = array(350, 351, 352, 353, 354,355);
                                    $operador = "";
                                    if (in_array($prefijo, $comcel))
                                    {
                                            $operador = "comcel";
                                    } elseif (in_array($prefijo, $movistar))
                                    {
                                            $operador = "movistar";
                                    } elseif (in_array($prefijo, $tigo))
                                    {
                                            $operador = "tigo";
                                    }elseif (in_array($prefijo, $avantel))
																		{
																			$operador = "avantel";
																		}
                                }
                                elseif(is_string($nombre))
                                {
                                    $nombre = $nombre;
                                }
				if ($operador != "" && $nombre!="")
				{
					if(strlen($numeros) == 12 or strlen($numeros) == 10 and ctype_digit($numeros))
					{
						$query = "insert into ".IDSERVICIO."_numeros values ('','$id_sel','$numeros','$operador','activo','{$nombre}','{$var2}','{$var3}')";
						if ($db->executeQuery($query))
						{
							$msg .= "<div class='lval'>[$id_sel] El M&oacute;vil <b>$numeros</b> fue agregado correctamente a la lista con el nombre <b>{$nombre}</b>.</div>";
						}
						else
						{
							$query = "insert into ".IDSERVICIO."_numeros_error(id_grupo,numero,operador,fecha) values ('$id_sel','$numeros','".$operador."-suscrito',now())";
							$db->executeQuery($query);
							$msg .= "<div class='linv'>El M&oacute;vil <b>$numeros</b> ya se encuentra suscrito a la lista con el nombre <b>{$nombre}</b>.</div>";
						}
					}ELSE{
						
						$query = "insert into ".IDSERVICIO."_numeros_error(id_grupo,numero,operador,fecha) values ('$id_sel','$numeros','no valido',now())";
						$db->executeQuery($query);
						$msg .= "<div class='lerr'>El M&oacute;vil <b>$numeros</b> no v&aacute;lido.</div>";
							
					}

				}
				else
				{
					$query = "insert into ".IDSERVICIO."_numeros_error  (id_grupo,numero,operador,fecha)values('$id_sel','$numeros','no valido',now())";
					$db->executeQuery($query);
					$msg .= "<div class='lerr'>El M&oacute;vil <b>$numeros</b> no tiene Operador O nombre v&aacute;lido  nombre <b>{$nombre}</b>.</div>";
				}

			}
			fclose($dataFile);
		}
		else
		{
			$msg = "Ha ocurrido un error al subir el fichero.";
		}

	}
	else
	{
		$msg = "Ha ocurrido un error al subir el fichero. No pudo guardarse.";
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<meta name="author" content="celmedia" />
		<title>Celmedia :: Envios Masivos</title>
		<link rel="stylesheet" type="text/css" href="css/style.css" media="screen" />
		<link rel="shortcut icon" href="favicon.ico" />
		<script type="text/javascript" src="js/prototype.js"></script>
		<script type="text/javascript" src="js/textsizer.js"></script>
		<script type="text/javascript" src="js/rel.js"></script>
		<script type="text/javascript" src="js/aplicaciones.js"></script>
	</head>

<body>
	<div id="wrap">
		<div id="top">
			<h2><a href="#" title="Back to main page">Celmedia</a></h2>
			<div id="menu">
				<?php include ('menu.php'); ?>
			</div>
		</div>
		<div id="content">
			<div style="float: right;">Usuario: <?= $reg_nombre ?> (<a href="logout.php">Salir</a>)</div>
			<div id="left">
				<h2>Resumen</h2>
				<p>
					<?php
                                                                             utf8_encode($nombre);
	echo $msg;
?>
				</p>
			</div>
			<div id="clear"></div>
		</div>
		<div id="footer">
			<?php include ('foot.php'); ?>
		</div>
	</div>
</body>
</html>