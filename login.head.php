<?php
	// Comienzo Include Login
	include ("seguridad/seguridad.php");
	session_start();
	include ("config/config.inc.php");
	if (!session_is_registered("admin_autorizado"))
	{
		session_unset();
		session_destroy();
		header("Location:index.php?msg=login&err=1&ref=$ref");
		exit();
	}
	if (($_SESSION['rid_usuario']=="") and (!$_SESSION['idsess'.NBOLSA]))
	{	
		session_unset();
		session_destroy();
		header("Location: index.php");
		exit();
	}
	/** VARIABLES DE SESION **/
	$reg_id = $_SESSION['rid_usuario'];
	$reg_nombre = $_SESSION['rnombre'];
	$reg_user = $_SESSION['rusuario'];
	$reg_nivel = $_SESSION['rnivel']; 
?>